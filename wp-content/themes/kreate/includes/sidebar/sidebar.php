<?php

if(!function_exists('kreate_elated_register_sidebars')) {
    /**
     * Function that registers theme's sidebars
     */
    function kreate_elated_register_sidebars() {

        register_sidebar(array(
            'name' => 'Sidebar',
            'id' => 'sidebar',
            'description' => 'Default Sidebar',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<div class="sidebar-widget-title-holder"><h4>',
            'after_title' => '</h4><div class="eltd-separator-holder clearfix eltd-separator-full-width eltd-dotted-multiple eltd-sep-dark"><div class="eltd-separator" style="height: 11px; margin-top: 25px; margin-bottom: 30px;"></div></div></div>'
        ));

    }

    add_action('widgets_init', 'kreate_elated_register_sidebars');
}

if(!function_exists('kreate_elated_add_support_custom_sidebar')) {
    /**
     * Function that adds theme support for custom sidebars. It also creates KreateSidebar object
     */
    function kreate_elated_add_support_custom_sidebar() {
        add_theme_support('KreateSidebar');
        if (get_theme_support('KreateSidebar')) new KreateSidebar();
    }

    add_action('after_setup_theme', 'kreate_elated_add_support_custom_sidebar');
}
