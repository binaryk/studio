<?php

if(!function_exists('kreate_elated_is_responsive_on')) {
    /**
     * Checks whether responsive mode is enabled in theme options
     * @return bool
     */
    function kreate_elated_is_responsive_on() {
        return kreate_elated_options()->getOptionValue('responsiveness') !== 'no';
    }
}