<?php

if(!function_exists('kreate_elated_boxed_class')) {
    /**
     * Function that adds classes on body for boxed layout
     */
    function kreate_elated_boxed_class($classes) {

        //is boxed layout turned on?
        if(kreate_elated_options()->getOptionValue('boxed') == 'yes' && kreate_elated_get_meta_field_intersect('header_type') !== 'header-vertical') {
            $classes[] = 'eltd-boxed';
        }

        return $classes;
    }

    add_filter('body_class', 'kreate_elated_boxed_class');
}

if(!function_exists('kreate_elated_theme_version_class')) {
    /**
     * Function that adds classes on body for version of theme
     */
    function kreate_elated_theme_version_class($classes) {
        $current_theme = wp_get_theme();

        //is child theme activated?
        if($current_theme->parent()) {
            //add child theme version
            $classes[] = strtolower($current_theme->get('Name')).'-child-ver-'.$current_theme->get('Version');

            //get parent theme
            $current_theme = $current_theme->parent();
        }

        if($current_theme->exists() && $current_theme->get('Version') != '') {
            $classes[] = strtolower($current_theme->get('Name')).'-ver-'.$current_theme->get('Version');
        }

        return $classes;
    }

    add_filter('body_class', 'kreate_elated_theme_version_class');
}

if(!function_exists('kreate_elated_smooth_scroll_class')) {
    /**
     * Function that adds classes on body for smooth scroll
     */
    function kreate_elated_smooth_scroll_class($classes) {

        //is smooth scroll enabled enabled?
        if(kreate_elated_options()->getOptionValue('smooth_scroll') == 'yes') {
            $classes[] = 'eltd-smooth-scroll';
        } else {
            $classes[] = '';
        }

        return $classes;
    }

    add_filter('body_class', 'kreate_elated_smooth_scroll_class');
}

if(!function_exists('kreate_elated_smooth_page_transitions_class')) {
    /**
     * Function that adds classes on body for smooth page transitions
     */
    function kreate_elated_smooth_page_transitions_class($classes) {

        if(kreate_elated_options()->getOptionValue('smooth_page_transitions') == 'yes') {
            $classes[] = 'eltd-smooth-page-transitions';
        } else {
            $classes[] = '';
        }

        return $classes;
    }

    add_filter('body_class', 'kreate_elated_smooth_page_transitions_class');
}

if(!function_exists('kreate_elated_content_initial_width_body_class')) {
    /**
     * Function that adds transparent content class to body.
     *
     * @param $classes array of body classes
     *
     * @return array with transparent content body class added
     */
    function kreate_elated_content_initial_width_body_class($classes) {

        if(kreate_elated_options()->getOptionValue('initial_content_width')) {
            $classes[] = 'eltd-'.kreate_elated_options()->getOptionValue('initial_content_width');
        }else{
            $classes[] = 'eltd-grid-1300';
        }

        return $classes;
    }

    add_filter('body_class', 'kreate_elated_content_initial_width_body_class');
}

if(!function_exists('kreate_elated_set_blog_body_class')) {
    /**
     * Function that adds blog class to body if blog template, shortcodes or widgets are used on site.
     *
     * @param $classes array of body classes
     *
     * @return array with blog body class added
     */
    function kreate_elated_set_blog_body_class($classes) {

        if(kreate_elated_load_blog_assets()) {
            $classes[] = 'eltd-blog-installed';
        }

        return $classes;
    }

    add_filter('body_class', 'kreate_elated_set_blog_body_class');
}


if(!function_exists('kreate_elated_set_portfolio_single_info_follow_body_class')) {
    /**
     * Function that adds follow portfolio info class to body if sticky sidebar is enabled on portfolio single small images or small slider
     *
     * @param $classes array of body classes
     *
     * @return array with follow portfolio info class body class added
     */

    function kreate_elated_set_portfolio_single_info_follow_body_class($classes) {

        if(is_singular('portfolio-item')){
            if(kreate_elated_options()->getOptionValue('portfolio_single_sticky_sidebar') == 'yes'){
                $classes[] = 'eltd-follow-portfolio-info';
            }
        }


        return $classes;
    }

    add_filter('body_class', 'kreate_elated_set_portfolio_single_info_follow_body_class');
}

if(!function_exists('kreate_elated_set_fixed_background_class')) {
    /**
     * Function that adds fixed image class to body
     *
     * @param $classes array of body classes
     *
     * @return array with fixed image class body class added
     */

    function kreate_elated_set_fixed_background_class($classes) {
        $id = kreate_elated_get_page_id();

        if(kreate_elated_get_meta_field_intersect('fixed_background_image',$id) !== '' && kreate_elated_options()->getOptionValue('boxed') == 'no'){
            $classes[] = 'eltd-fixed-background';
        }

        return $classes;
    }

    add_filter('body_class', 'kreate_elated_set_fixed_background_class');
}