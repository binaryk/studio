<?php
if(!function_exists('kreate_elated_design_styles')) {
    /**
     * Generates general custom styles
     */
    function kreate_elated_design_styles() {

        $preload_background_styles = array();

        if(kreate_elated_options()->getOptionValue('preload_pattern_image') !== ""){
            $preload_background_styles['background-image'] = 'url('.kreate_elated_options()->getOptionValue('preload_pattern_image').') !important';
        }else{
            $preload_background_styles['background-image'] = 'url('.esc_url(ELATED_ASSETS_ROOT."/img/preload_pattern.png").') !important';
        }

        echo kreate_elated_dynamic_css('.eltd-preload-background', $preload_background_styles);

		if (kreate_elated_options()->getOptionValue('google_fonts')){
			$font_family = kreate_elated_options()->getOptionValue('google_fonts');
			if(kreate_elated_is_font_option_valid($font_family)) {
				echo kreate_elated_dynamic_css('body', array('font-family' => kreate_elated_get_font_option_val($font_family)));
			}
		}

        if(kreate_elated_options()->getOptionValue('first_color') !== "") {
            $color_selector = array(
                'h1 a:hover',
                'h2 a:hover',
                'h3 a:hover',
                'h4 a:hover',
                'h5 a:hover',
                'h6 a:hover',
                'h5',
                'a',
                'p a',
                '.eltd-author-description .eltd-author',
                '.eltd-single-tags-holder .eltd-tags a:hover',
                '.eltd-single-links-pages .eltd-single-links-pages-inner > a:hover',
                '.eltd-single-links-pages .eltd-single-links-pages-inner > span',
                '.eltd-comment-holder .eltd-comment-text .replay:hover',
                '.eltd-comment-holder .eltd-comment-text .comment-reply-link:hover',
                '.eltd-comment-holder .eltd-comment-text .comment-edit-link:hover',
                '.eltd-comment-holder .eltd-comment-text .eltd-comment-arrow:hover',
                '#submit_comment:hover',
                '.post-password-form input[type="submit"]:hover',
                'input.wpcf7-form-control.wpcf7-submit:hover',
                '.eltd-pagination li a:hover',
                '.eltd-pagination li.active span',
                '.eltd-print-page:hover',
                '.eltd-drop-down .wide .second .inner > ul > li > a:hover',
                '.eltd-drop-down .wide .second .inner ul li.sub .flexslider ul li a:hover',
                '.eltd-drop-down .wide .second ul li .flexslider ul li a:hover',
                '.eltd-drop-down .wide .second .inner ul li.sub .flexslider.widget_flexslider .menu_recent_post_text a:hover',
                '.eltd-mobile-header .eltd-mobile-nav a:hover, .eltd-mobile-header .eltd-mobile-nav h4:hover',
                'footer .eltd-footer-bottom-holder .widget.widget_nav_menu li a:hover',
                '.eltd-title .eltd-title-holder .eltd-breadcrumbs span.eltd-current',
                '.eltd-portfolio-single-holder .eltd-toolbar-holder .eltd-like:not(.liked):hover',
                '.eltd-portfolio-single-holder .eltd-portfolio-info-holder .eltd-portfolio-info-item.eltd-portfolio-categories p',
                '.eltd-portfolio-single-holder .eltd-portfolio-single-nav .eltd-portfolio-back-btn:hover span',
                '.eltd-counter-holder .eltd-counter',
                '.eltd-ordered-list ol > li:before',
                '.eltd-icon-list-item .eltd-icon-list-icon-holder-inner i',
                '.eltd-icon-list-item .eltd-icon-list-icon-holder-inner .font_elegant',
                '.eltd-price-table .eltd-table-btm .eltd-table-icon',
                '.eltd-price-table .eltd-price-table-inner .eltd-table-prices .eltd-value',
                '.eltd-tabs .eltd-tabs-nav li.ui-state-active a',
                '.eltd-tabs .eltd-tabs-nav li.ui-state-hover a',
                '.eltd-accordion-holder .eltd-title-holder.ui-state-active',
                '.eltd-accordion-holder .eltd-title-holder.ui-state-hover',
                '.eltd-blog-list-holder .eltd-item-info-section',
                '.eltd-blog-list-holder .eltd-item-info-section > div a',
                '.eltd-blog-list-holder .eltd-item-info-section > div:before',
                '.eltd-blog-list-holder .eltd-item-info-section span',
                '.eltd-blog-slider-item .eltd-item-info-section',
                '.eltd-blog-slider-item .eltd-item-info-section > div a',
                '.eltd-blog-slider-item .eltd-item-info-section > div:before',
                '.eltd-blog-slider-item .eltd-item-info-section span',
                '.eltd-btn.eltd-btn-outline',
                '.eltd-dropcaps',
                '.eltd-portfolio-filter-holder .eltd-portfolio-filter-holder-inner ul li.active span',
                '.eltd-portfolio-filter-holder .eltd-portfolio-filter-holder-inner ul li.current span',
                '.eltd-portfolio-list-holder article .eltd-item-icons-holder a',
                '.eltd-portfolio-list-holder-outer.eltd-ptf-standard article .eltd-item-icons-holder a:hover',
                '.eltd-portfolio-list-holder-outer.eltd-ptf-standard article .eltd-item-text-holder .eltd-ptf-category-holder span',
                '.eltd-portfolio-list-holder-outer.eltd-ptf-gallery article .eltd-item-text-holder .eltd-item-title > a',
                '.eltd-portfolio-list-holder-outer.eltd-ptf-gallery article .eltd-item-text-holder .eltd-ptf-category-holder span',
                '.eltd-portfolio-list-holder-outer.eltd-ptf-pinterest article .eltd-item-text-holder .eltd-item-title > a',
                '.eltd-portfolio-list-holder-outer.eltd-ptf-pinterest article .eltd-item-text-holder .eltd-ptf-category-holder span',
                '.eltd-portfolio-list-holder-outer.eltd-ptf-masonry article .eltd-item-text-holder .eltd-item-title > a',
                '.eltd-portfolio-list-holder-outer.eltd-ptf-masonry article .eltd-item-text-holder .eltd-ptf-category-holder span',
                '.eltd-social-share-holder.eltd-dropdown .eltd-social-share-dropdown-opener:hover',
                '.eltd-process-outer .eltd-process-slider .eltd-process-slider-content-area .eltd-process-slider-item .eltd-process-slide-item-title-holder .eltd-process-subtitle',
                '.widget.widget_rss ul li:hover > a',
                '.widget.widget_rss ul li:hover:before',
                '.widget.widget_text ul li:hover > a',
                '.widget.widget_text ul li:hover:before',
                '.widget.widget_pages ul li:hover > a',
                '.widget.widget_pages ul li:hover:before',
                '.widget.widget_meta ul li:hover > a',
                '.widget.widget_meta ul li:hover:before',
                '.widget.widget_archive ul li:hover > a',
                '.widget.widget_archive ul li:hover:before',
                '.widget.widget_nav_menu ul li:hover > a',
                '.widget.widget_nav_menu ul li:hover:before',
                '.widget.widget_recent_entries ul li:hover > a',
                '.widget.widget_recent_entries ul li:hover:before',
                '.widget.widget_recent_comments ul li:hover > a',
                '.widget.widget_recent_comments ul li:hover:before',
                '.widget.widget_product_categories ul li:hover > a',
                '.widget.widget_product_categories ul li:hover:before',
                '.widget.widget_categories ul li:hover > a',
                '.widget.widget_categories ul li:hover:before',
                '.widget.widget_tag_cloud a:hover',
                '.widget #lang_sel *:hover > a',
                '.widget #lang_sel_list *:hover > a',
                '.widget #lang_sel ul ul *:hover > a',
                '.widget #lang_sel_list ul ul *:hover > a',
                '.widget #lang_sel a:hover',
                '.widget #lang_sel_list a:hover'
            );

            $color_important_selector = array(
            	'.eltd-btn.eltd-btn-solid:not(.eltd-btn-custom-hover-color):hover'
            );

            $background_color_selector = array(
            	'.eltd-blog-holder article .eltd-post-mark',
            	'.eltd-st-loader .pulse',
            	'.eltd-st-loader .double_pulse .double-bounce1',
            	'.eltd-st-loader .double_pulse .double-bounce2',
            	'.eltd-st-loader .cube',
            	'.eltd-st-loader .rotating_cubes .cube1',
            	'.eltd-st-loader .rotating_cubes .cube2',
            	'.eltd-st-loader .stripes > div',
            	'.eltd-st-loader .wave > div',
            	'.eltd-st-loader .two_rotating_circles .dot1',
            	'.eltd-st-loader .two_rotating_circles .dot2',
            	'.eltd-st-loader .five_rotating_circles .container1 > div',
            	'.eltd-st-loader .five_rotating_circles .container2 > div',
            	'.eltd-st-loader .five_rotating_circles .container3 > div',
            	'.eltd-st-loader .atom .ball-1:before',
            	'.eltd-st-loader .atom .ball-2:before',
            	'.eltd-st-loader .atom .ball-3:before',
            	'.eltd-st-loader .atom .ball-4:before',
            	'.eltd-st-loader .clock .ball:before',
            	'.eltd-st-loader .mitosis .ball',
            	'.eltd-st-loader .lines .line1',
            	'.eltd-st-loader .lines .line2',
            	'.eltd-st-loader .lines .line3',
            	'.eltd-st-loader .lines .line4',
            	'.eltd-st-loader .fussion .ball',
            	'.eltd-st-loader .fussion .ball-1',
            	'.eltd-st-loader .fussion .ball-2',
            	'.eltd-st-loader .fussion .ball-3',
            	'.eltd-st-loader .fussion .ball-4',
            	'.eltd-st-loader .wave_circles .ball',
            	'.eltd-st-loader .pulse_circles .ball',
            	'.eltd-main-menu > ul > li > a:hover span.bottom_line',
            	'.eltd-drop-down .narrow .second .inner ul li.tracker',
                'nav.eltd-fullscreen-menu ul li a:hover span.bottom_line',
                '.eltd-icon-shortcode.circle',
                '.eltd-icon-shortcode.square',
                '.eltd-progress-bar .eltd-progress-content-outer .eltd-progress-content',
                '.eltd-blog-list-holder.eltd-boxes .eltd-post-mark',
                '.eltd-btn.eltd-btn-solid',
                '.eltd-dropcaps.eltd-square',
                '.eltd-dropcaps.eltd-circle',
                '.eltd-portfolio-list-holder-outer.eltd-ptf-standard article .eltd-item-icons-holder',
                '.eltd-process-outer .eltd-process-slider .eltd-process-slider-title-area .eltd-process-slider-next-nav span'
            );

            $background_color_important_selector = array(
            	'.eltd-btn.eltd-btn-outline:not(.eltd-btn-custom-hover-bg):hover'
            );

            $border_color_selector = array(
            	'.eltd-st-loader .pulse_circles .ball',
            	'.wpcf7-form-control.wpcf7-text:focus',
            	'.wpcf7-form-control.wpcf7-number:focus',
            	'.wpcf7-form-control.wpcf7-date:focus',
            	'.wpcf7-form-control.wpcf7-textarea:focus',
            	'.wpcf7-form-control.wpcf7-select:focus',
            	'.wpcf7-form-control.wpcf7-quiz:focus',
            	'#respond textarea:focus',
            	'#respond input[type="text"]:focus',
            	'.post-password-form input[type="password"]:focus',
            	'.eltd-drop-down .second',
            	'.widget_search input[type="text"]:focus',
            	'.eltd-btn.eltd-btn-solid',
            	'.eltd-btn.eltd-btn-outline',
            	'.eltd-portfolio-list-holder article .eltd-item-icons-holder a',
            	'.widget.widget_rss select:focus',
            	'.widget.widget_rss select option:focus',
            	'.widget.widget_text select:focus',
            	'.widget.widget_text select option:focus',
            	'.widget.widget_pages select:focus',
            	'.widget.widget_pages select option:focus',
            	'.widget.widget_meta select:focus',
            	'.widget.widget_meta select option:focus',
            	'.widget.widget_archive select:focus',
            	'.widget.widget_archive select option:focus',
            	'.widget.widget_nav_menu select:focus',
            	'.widget.widget_nav_menu select option:focus',
            	'.widget.widget_recent_entries select:focus',
            	'.widget.widget_recent_entries select option:focus',
            	'.widget.widget_recent_comments select:focus',
            	'.widget.widget_recent_comments select option:focus',
            	'.widget.widget_product_categories select:focus',
            	'.widget.widget_product_categories select option:focus',
            	'.widget.widget_categories select:focus',
            	'.widget.widget_categories select option:focus',
            	'.widget #lang_sel ul ul'
            );

            $border_color_important_selector = array(
            	'.eltd-btn.eltd-btn-solid:not(.eltd-btn-custom-border-hover):hover',
            	'.eltd-btn.eltd-btn-outline:not(.eltd-btn-custom-border-hover):hover'
        	);
			if(kreate_elated_is_woocommerce_installed()){
				$color_selector = array_merge($color_selector,array(
					'.woocommerce-pagination .page-numbers li span.current',
					'.woocommerce-pagination .page-numbers li a:hover',
					'.woocommerce-pagination .page-numbers li span:hover',
					'.woocommerce-pagination .page-numbers li span.current:hover',
					'.eltd-single-product-summary input[type="submit"]:hover',
					'.eltd-woocommerce-page .woocommerce-product-rating .woocommerce-review-link:hover',
					'.eltd-woocommerce-page .star-rating',
					'.eltd-woocommerce-page input[type="submit"].button',
					'.eltd-shopping-cart-dropdown ul li a:hover',
					'.eltd-shopping-cart-dropdown .eltd-item-info-holder .eltd-item-left:hover',
					'.eltd-shopping-cart-dropdown span.eltd-total span',
					'.eltd-shopping-cart-dropdown .eltd-empty-cart .eltd-cart-icon-empty',
					'.eltd-shopping-cart-dropdown span.eltd-quantity',
					'.woocommerce.widget_price_filter .price_slider_amount button:hover',
					'.eltd-woocommerce-page .select2-container .select2-choice .select2-arrow b:after',
					'.woocommerce-pagination .page-numbers li span.current',
					'.woocommerce-pagination .page-numbers li a:hover',
					'.woocommerce-pagination .page-numbers li span:hover',
					'.woocommerce-pagination .page-numbers li span.current:hover',
					'.eltd-single-product-summary input[type="submit"]:hover',
					'.eltd-woocommerce-page .woocommerce-product-rating .woocommerce-review-link:hover',
					'.eltd-woocommerce-page .star-rating',
					'.eltd-woocommerce-page input[type="submit"].button',
					'.eltd-shopping-cart-dropdown ul li a:hover',
					'.eltd-shopping-cart-dropdown .eltd-item-info-holder .eltd-item-left:hover',
					'.eltd-shopping-cart-dropdown span.eltd-total span',
					'.eltd-shopping-cart-dropdown .eltd-empty-cart .eltd-cart-icon-empty',
					'.eltd-shopping-cart-dropdown span.eltd-quantity',
					'.woocommerce.widget_price_filter .price_slider_amount button:hover',
					'.eltd-woocommerce-page .select2-container .select2-choice .select2-arrow b:after'
				));

				$color_important_selector = array_merge($color_important_selector,array(
					'.eltd-woocommerce-page .single_add_to_cart_button.eltd-btn:hover',
					'.eltd-woocommerce-page .single_add_to_cart_button.eltd-btn:hover'
				));

				$background_color_selector = array_merge($background_color_selector,array(
					'.woocommerce .product .eltd-onsale',
					'.woocommerce .product .eltd-out-of-stock',
					'.eltd-woocommerce-page .product .eltd-onsale',
					'.eltd-woocommerce-page .product .eltd-out-of-stock',
					'.eltd-woocommerce-page input[type="submit"].button:hover',
					'.woocommerce .product .eltd-onsale',
					'.woocommerce .product .eltd-out-of-stock',
					'.eltd-woocommerce-page .product .eltd-onsale',
					'.eltd-woocommerce-page .product .eltd-out-of-stock',
					'.eltd-woocommerce-page input[type="submit"].button:hover'
				));

				$border_color_selector = array_merge($border_color_selector,array(
					'.eltd-woocommerce-page .coupon input[type="text"]:focus',
					'.eltd-woocommerce-page input[type="text"]:focus:not(.eltd-search-field)',
					'.eltd-woocommerce-page input[type="email"]:focus',
					'.eltd-woocommerce-page input[type="tel"]:focus',
					'.eltd-woocommerce-page input[type="password"]:focus',
					'.eltd-woocommerce-page textarea:focus',
					'.eltd-woocommerce-page input[type="submit"].button',
					'.eltd-shopping-cart-dropdown',
					'.eltd-woocommerce-page .coupon input[type="text"]:focus',
					'.eltd-woocommerce-page input[type="text"]:focus:not(.eltd-search-field)',
					'.eltd-woocommerce-page input[type="email"]:focus',
					'.eltd-woocommerce-page input[type="tel"]:focus',
					'.eltd-woocommerce-page input[type="password"]:focus',
					'.eltd-woocommerce-page textarea:focus',
					'.eltd-woocommerce-page input[type="submit"].button',
					'.eltd-shopping-cart-dropdown'
				));
			}

            echo kreate_elated_dynamic_css($color_selector, array('color' => kreate_elated_options()->getOptionValue('first_color')));
            echo kreate_elated_dynamic_css($color_important_selector, array('color' => kreate_elated_options()->getOptionValue('first_color').'!important'));
            echo kreate_elated_dynamic_css('::selection', array('background' => kreate_elated_options()->getOptionValue('first_color')));
            echo kreate_elated_dynamic_css('::-moz-selection', array('background' => kreate_elated_options()->getOptionValue('first_color')));
            echo kreate_elated_dynamic_css($background_color_selector, array('background-color' => kreate_elated_options()->getOptionValue('first_color')));
            echo kreate_elated_dynamic_css($background_color_important_selector, array('background-color' => kreate_elated_options()->getOptionValue('first_color').'!important'));
            echo kreate_elated_dynamic_css($border_color_selector, array('border-color' => kreate_elated_options()->getOptionValue('first_color')));
            echo kreate_elated_dynamic_css($border_color_important_selector, array('border-color' => kreate_elated_options()->getOptionValue('first_color').'!important'));
        }

		if (kreate_elated_options()->getOptionValue('page_background_color')) {
			$background_color_selector = array(
				'.eltd-wrapper-inner',
				'.eltd-content',
				'.eltd-container',
				'.single-post .eltd-content-inner > .eltd-container',
                '.single-portfolio-item .eltd-content-inner > .eltd-container',
				'.eltd-tabs.eltd-vertical .eltd-tabs-nav li.ui-state-active a'
			);
			echo kreate_elated_dynamic_css($background_color_selector, array('background-color' => kreate_elated_options()->getOptionValue('page_background_color')));
		}

		if (kreate_elated_options()->getOptionValue('selection_color')) {
			echo kreate_elated_dynamic_css('::selection', array('background' => kreate_elated_options()->getOptionValue('selection_color')));
			echo kreate_elated_dynamic_css('::-moz-selection', array('background' => kreate_elated_options()->getOptionValue('selection_color')));
		}

		$boxed_background_style = array();
		if (kreate_elated_options()->getOptionValue('page_background_color_in_box')) {
			$boxed_background_style['background-color'] = kreate_elated_options()->getOptionValue('page_background_color_in_box');
		}

		if (kreate_elated_options()->getOptionValue('boxed_background_image')) {
			$boxed_background_style['background-image'] = 'url('.esc_url(kreate_elated_options()->getOptionValue('boxed_background_image')).')';
			$boxed_background_style['background-position'] = 'center 0px';
			$boxed_background_style['background-repeat'] = 'no-repeat';
		}

		if (kreate_elated_options()->getOptionValue('boxed_pattern_background_image')) {
			$boxed_background_style['background-image'] = 'url('.esc_url(kreate_elated_options()->getOptionValue('boxed_pattern_background_image')).')';
			$boxed_background_style['background-position'] = '0px 0px';
			$boxed_background_style['background-repeat'] = 'repeat';
		}

		if (kreate_elated_options()->getOptionValue('boxed_background_image_attachment')) {
			$boxed_background_style['background-attachment'] = (kreate_elated_options()->getOptionValue('boxed_background_image_attachment'));
		}

		echo kreate_elated_dynamic_css('.eltd-boxed .eltd-wrapper', $boxed_background_style);


		$content_top_padding_grid = kreate_elated_options()->getOptionValue('content_top_padding_grid');
		if ($content_top_padding_grid !== ''){
			echo kreate_elated_dynamic_css('.eltd-content-inner > .eltd-container', array('padding-top' => kreate_elated_filter_px($content_top_padding_grid).'px'));
		}

		$content_top_padding_full_width = kreate_elated_options()->getOptionValue('content_top_padding_full_width');
		if ($content_top_padding_full_width !== ''){
			echo kreate_elated_dynamic_css('.eltd-content-inner > .eltd-full-width', array('padding-top' => kreate_elated_filter_px($content_top_padding_full_width).'px'));
		}
    }

    add_action('kreate_elated_style_dynamic', 'kreate_elated_design_styles');
}

if (!function_exists('kreate_elated_h1_styles')) {

    function kreate_elated_h1_styles() {

        $h1_styles = array();

        if(kreate_elated_options()->getOptionValue('h1_color') !== '') {
            $h1_styles['color'] = kreate_elated_options()->getOptionValue('h1_color');
        }
        if(kreate_elated_options()->getOptionValue('h1_google_fonts') !== '-1') {
            $h1_styles['font-family'] = kreate_elated_get_formatted_font_family(kreate_elated_options()->getOptionValue('h1_google_fonts'));
        }
        if(kreate_elated_options()->getOptionValue('h1_fontsize') !== '') {
            $h1_styles['font-size'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h1_fontsize')).'px';
        }
        if(kreate_elated_options()->getOptionValue('h1_lineheight') !== '') {
            $h1_styles['line-height'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h1_lineheight')).'px';
        }
        if(kreate_elated_options()->getOptionValue('h1_texttransform') !== '') {
            $h1_styles['text-transform'] = kreate_elated_options()->getOptionValue('h1_texttransform');
        }
        if(kreate_elated_options()->getOptionValue('h1_fontstyle') !== '') {
            $h1_styles['font-style'] = kreate_elated_options()->getOptionValue('h1_fontstyle');
        }
        if(kreate_elated_options()->getOptionValue('h1_fontweight') !== '') {
            $h1_styles['font-weight'] = kreate_elated_options()->getOptionValue('h1_fontweight');
        }
        if(kreate_elated_options()->getOptionValue('h1_letterspacing') !== '') {
            $h1_styles['letter-spacing'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h1_letterspacing')).'px';
        }

        $h1_selector = array(
            'h1'
        );

        if (!empty($h1_styles)) {
            echo kreate_elated_dynamic_css($h1_selector, $h1_styles);
        }
    }

    add_action('kreate_elated_style_dynamic', 'kreate_elated_h1_styles');
}

if (!function_exists('kreate_elated_h2_styles')) {

    function kreate_elated_h2_styles() {

        $h2_styles = array();

        if(kreate_elated_options()->getOptionValue('h2_color') !== '') {
            $h2_styles['color'] = kreate_elated_options()->getOptionValue('h2_color');
        }
        if(kreate_elated_options()->getOptionValue('h2_google_fonts') !== '-1') {
            $h2_styles['font-family'] = kreate_elated_get_formatted_font_family(kreate_elated_options()->getOptionValue('h2_google_fonts'));
        }
        if(kreate_elated_options()->getOptionValue('h2_fontsize') !== '') {
            $h2_styles['font-size'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h2_fontsize')).'px';
        }
        if(kreate_elated_options()->getOptionValue('h2_lineheight') !== '') {
            $h2_styles['line-height'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h2_lineheight')).'px';
        }
        if(kreate_elated_options()->getOptionValue('h2_texttransform') !== '') {
            $h2_styles['text-transform'] = kreate_elated_options()->getOptionValue('h2_texttransform');
        }
        if(kreate_elated_options()->getOptionValue('h2_fontstyle') !== '') {
            $h2_styles['font-style'] = kreate_elated_options()->getOptionValue('h2_fontstyle');
        }
        if(kreate_elated_options()->getOptionValue('h2_fontweight') !== '') {
            $h2_styles['font-weight'] = kreate_elated_options()->getOptionValue('h2_fontweight');
        }
        if(kreate_elated_options()->getOptionValue('h2_letterspacing') !== '') {
            $h2_styles['letter-spacing'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h2_letterspacing')).'px';
        }

        $h2_selector = array(
            'h2'
        );

        if (!empty($h2_styles)) {
            echo kreate_elated_dynamic_css($h2_selector, $h2_styles);
        }
    }

    add_action('kreate_elated_style_dynamic', 'kreate_elated_h2_styles');
}

if (!function_exists('kreate_elated_h3_styles')) {

    function kreate_elated_h3_styles() {

        $h3_styles = array();

        if(kreate_elated_options()->getOptionValue('h3_color') !== '') {
            $h3_styles['color'] = kreate_elated_options()->getOptionValue('h3_color');
        }
        if(kreate_elated_options()->getOptionValue('h3_google_fonts') !== '-1') {
            $h3_styles['font-family'] = kreate_elated_get_formatted_font_family(kreate_elated_options()->getOptionValue('h3_google_fonts'));
        }
        if(kreate_elated_options()->getOptionValue('h3_fontsize') !== '') {
            $h3_styles['font-size'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h3_fontsize')).'px';
        }
        if(kreate_elated_options()->getOptionValue('h3_lineheight') !== '') {
            $h3_styles['line-height'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h3_lineheight')).'px';
        }
        if(kreate_elated_options()->getOptionValue('h3_texttransform') !== '') {
            $h3_styles['text-transform'] = kreate_elated_options()->getOptionValue('h3_texttransform');
        }
        if(kreate_elated_options()->getOptionValue('h3_fontstyle') !== '') {
            $h3_styles['font-style'] = kreate_elated_options()->getOptionValue('h3_fontstyle');
        }
        if(kreate_elated_options()->getOptionValue('h3_fontweight') !== '') {
            $h3_styles['font-weight'] = kreate_elated_options()->getOptionValue('h3_fontweight');
        }
        if(kreate_elated_options()->getOptionValue('h3_letterspacing') !== '') {
            $h3_styles['letter-spacing'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h3_letterspacing')).'px';
        }

        $h3_selector = array(
            'h3'
        );

        if (!empty($h3_styles)) {
            echo kreate_elated_dynamic_css($h3_selector, $h3_styles);
        }
    }

    add_action('kreate_elated_style_dynamic', 'kreate_elated_h3_styles');
}

if (!function_exists('kreate_elated_h4_styles')) {

    function kreate_elated_h4_styles() {

        $h4_styles = array();

        if(kreate_elated_options()->getOptionValue('h4_color') !== '') {
            $h4_styles['color'] = kreate_elated_options()->getOptionValue('h4_color');
        }
        if(kreate_elated_options()->getOptionValue('h4_google_fonts') !== '-1') {
            $h4_styles['font-family'] = kreate_elated_get_formatted_font_family(kreate_elated_options()->getOptionValue('h4_google_fonts'));
        }
        if(kreate_elated_options()->getOptionValue('h4_fontsize') !== '') {
            $h4_styles['font-size'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h4_fontsize')).'px';
        }
        if(kreate_elated_options()->getOptionValue('h4_lineheight') !== '') {
            $h4_styles['line-height'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h4_lineheight')).'px';
        }
        if(kreate_elated_options()->getOptionValue('h4_texttransform') !== '') {
            $h4_styles['text-transform'] = kreate_elated_options()->getOptionValue('h4_texttransform');
        }
        if(kreate_elated_options()->getOptionValue('h4_fontstyle') !== '') {
            $h4_styles['font-style'] = kreate_elated_options()->getOptionValue('h4_fontstyle');
        }
        if(kreate_elated_options()->getOptionValue('h4_fontweight') !== '') {
            $h4_styles['font-weight'] = kreate_elated_options()->getOptionValue('h4_fontweight');
        }
        if(kreate_elated_options()->getOptionValue('h4_letterspacing') !== '') {
            $h4_styles['letter-spacing'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h4_letterspacing')).'px';
        }

        $h4_selector = array(
            'h4'
        );

        if (!empty($h4_styles)) {
            echo kreate_elated_dynamic_css($h4_selector, $h4_styles);
        }
    }

    add_action('kreate_elated_style_dynamic', 'kreate_elated_h4_styles');
}

if (!function_exists('kreate_elated_h5_styles')) {

    function kreate_elated_h5_styles() {

        $h5_styles = array();

        if(kreate_elated_options()->getOptionValue('h5_color') !== '') {
            $h5_styles['color'] = kreate_elated_options()->getOptionValue('h5_color');
        }
        if(kreate_elated_options()->getOptionValue('h5_google_fonts') !== '-1') {
            $h5_styles['font-family'] = kreate_elated_get_formatted_font_family(kreate_elated_options()->getOptionValue('h5_google_fonts'));
        }
        if(kreate_elated_options()->getOptionValue('h5_fontsize') !== '') {
            $h5_styles['font-size'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h5_fontsize')).'px';
        }
        if(kreate_elated_options()->getOptionValue('h5_lineheight') !== '') {
            $h5_styles['line-height'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h5_lineheight')).'px';
        }
        if(kreate_elated_options()->getOptionValue('h5_texttransform') !== '') {
            $h5_styles['text-transform'] = kreate_elated_options()->getOptionValue('h5_texttransform');
        }
        if(kreate_elated_options()->getOptionValue('h5_fontstyle') !== '') {
            $h5_styles['font-style'] = kreate_elated_options()->getOptionValue('h5_fontstyle');
        }
        if(kreate_elated_options()->getOptionValue('h5_fontweight') !== '') {
            $h5_styles['font-weight'] = kreate_elated_options()->getOptionValue('h5_fontweight');
        }
        if(kreate_elated_options()->getOptionValue('h5_letterspacing') !== '') {
            $h5_styles['letter-spacing'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h5_letterspacing')).'px';
        }

        $h5_selector = array(
            'h5'
        );

        if (!empty($h5_styles)) {
            echo kreate_elated_dynamic_css($h5_selector, $h5_styles);
        }
    }

    add_action('kreate_elated_style_dynamic', 'kreate_elated_h5_styles');
}

if (!function_exists('kreate_elated_h6_styles')) {

    function kreate_elated_h6_styles() {

        $h6_styles = array();

        if(kreate_elated_options()->getOptionValue('h6_color') !== '') {
            $h6_styles['color'] = kreate_elated_options()->getOptionValue('h6_color');
        }
        if(kreate_elated_options()->getOptionValue('h6_google_fonts') !== '-1') {
            $h6_styles['font-family'] = kreate_elated_get_formatted_font_family(kreate_elated_options()->getOptionValue('h6_google_fonts'));
        }
        if(kreate_elated_options()->getOptionValue('h6_fontsize') !== '') {
            $h6_styles['font-size'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h6_fontsize')).'px';
        }
        if(kreate_elated_options()->getOptionValue('h6_lineheight') !== '') {
            $h6_styles['line-height'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h6_lineheight')).'px';
        }
        if(kreate_elated_options()->getOptionValue('h6_texttransform') !== '') {
            $h6_styles['text-transform'] = kreate_elated_options()->getOptionValue('h6_texttransform');
        }
        if(kreate_elated_options()->getOptionValue('h6_fontstyle') !== '') {
            $h6_styles['font-style'] = kreate_elated_options()->getOptionValue('h6_fontstyle');
        }
        if(kreate_elated_options()->getOptionValue('h6_fontweight') !== '') {
            $h6_styles['font-weight'] = kreate_elated_options()->getOptionValue('h6_fontweight');
        }
        if(kreate_elated_options()->getOptionValue('h6_letterspacing') !== '') {
            $h6_styles['letter-spacing'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h6_letterspacing')).'px';
        }

        $h6_selector = array(
            'h6'
        );

        if (!empty($h6_styles)) {
            echo kreate_elated_dynamic_css($h6_selector, $h6_styles);
        }
    }

    add_action('kreate_elated_style_dynamic', 'kreate_elated_h6_styles');
}

if (!function_exists('kreate_elated_text_styles')) {

    function kreate_elated_text_styles() {

        $text_styles = array();

        if(kreate_elated_options()->getOptionValue('text_color') !== '') {
            $text_styles['color'] = kreate_elated_options()->getOptionValue('text_color');
        }
        if(kreate_elated_options()->getOptionValue('text_google_fonts') !== '-1') {
            $text_styles['font-family'] = kreate_elated_get_formatted_font_family(kreate_elated_options()->getOptionValue('text_google_fonts'));
            echo kreate_elated_dynamic_css(array(
            	'.eltd-blog-holder article span.eltd-quote-author',
            	'.eltd-single-links-pages .eltd-single-links-pages-inner > a',
            	'.eltd-single-links-pages .eltd-single-links-pages-inner > span',
            	'.eltd-comment-holder .eltd-comment-text .eltd-comment-date',
            	'.eltd-pagination li a',
            	'.eltd-pagination li.active span',
            	'.eltd-counter-holder .eltd-counter',
            	'.eltd-progress-bar.eltd-progress-on-side .eltd-progress-number-wrapper .eltd-progress-number',
            	'.eltd-price-table .eltd-price-table-inner .eltd-table-prices .eltd-price',
            	'.eltd-blog-list-holder.eltd-image-in-box .eltd-item-info-section',
            	'blockquote div.eltd-blockquote-text',
            	'.eltd-process-outer .eltd-process-slider .eltd-process-slider-content-area .eltd-process-slider-item .eltd-process-slide-item-title-holder .eltd-process-slide-item-number',
            	'.widget.widget_text'
        	),array('font-family' => $text_styles['font-family']));

			if (kreate_elated_is_woocommerce_installed()){
				echo kreate_elated_dynamic_css(array(
					'.woocommerce .product .price',
					'.eltd-woocommerce-page .product .price',
					'.woocommerce .product .eltd-onsale',
					'.woocommerce .product .eltd-out-of-stock',
					'.eltd-woocommerce-page .product .eltd-onsale',
					'.eltd-woocommerce-page .product .eltd-out-of-stock',
					'.woocommerce-pagination .page-numbers li > a',
					'.woocommerce-pagination .page-numbers li > span',
					'.eltd-woocommerce-page .woocommerce-product-rating .woocommerce-review-link',
					'.eltd-woocommerce-page .eltd-quantity-buttons .eltd-quantity-input',
					'.eltd-shopping-cart-outer .eltd-shopping-cart-header .eltd-cart-no',
					'.eltd-shopping-cart-dropdown .eltd-item-info-holder .eltd-item-left .amount',
					'.eltd-shopping-cart-dropdown .eltd-cart-bottom .eltd-subtotal-holder .eltd-total-amount',
					'.woocommerce.widget_price_filter .price_slider_amount .price_label',
					'.widget .product_list_widget li .amount'
				),array('font-family' => $text_styles['font-family']));
			}
        }
        if(kreate_elated_options()->getOptionValue('text_fontsize') !== '') {
            $text_styles['font-size'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('text_fontsize')).'px';
        }
        if(kreate_elated_options()->getOptionValue('text_lineheight') !== '') {
            $text_styles['line-height'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('text_lineheight')).'px';
        }
        if(kreate_elated_options()->getOptionValue('text_texttransform') !== '') {
            $text_styles['text-transform'] = kreate_elated_options()->getOptionValue('text_texttransform');
        }
        if(kreate_elated_options()->getOptionValue('text_fontstyle') !== '') {
            $text_styles['font-style'] = kreate_elated_options()->getOptionValue('text_fontstyle');
        }
        if(kreate_elated_options()->getOptionValue('text_fontweight') !== '') {
            $text_styles['font-weight'] = kreate_elated_options()->getOptionValue('text_fontweight');
        }
        if(kreate_elated_options()->getOptionValue('text_letterspacing') !== '') {
            $text_styles['letter-spacing'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('text_letterspacing')).'px';
        }

        $text_selector = array(
            'p'
        );

        if (!empty($text_styles)) {
            echo kreate_elated_dynamic_css($text_selector, $text_styles);
        }
    }

    add_action('kreate_elated_style_dynamic', 'kreate_elated_text_styles');
}

if (!function_exists('kreate_elated_link_styles')) {

    function kreate_elated_link_styles() {

        $link_styles = array();

        if(kreate_elated_options()->getOptionValue('link_color') !== '') {
            $link_styles['color'] = kreate_elated_options()->getOptionValue('link_color');
        }
        if(kreate_elated_options()->getOptionValue('link_fontstyle') !== '') {
            $link_styles['font-style'] = kreate_elated_options()->getOptionValue('link_fontstyle');
        }
        if(kreate_elated_options()->getOptionValue('link_fontweight') !== '') {
            $link_styles['font-weight'] = kreate_elated_options()->getOptionValue('link_fontweight');
        }
        if(kreate_elated_options()->getOptionValue('link_fontdecoration') !== '') {
            $link_styles['text-decoration'] = kreate_elated_options()->getOptionValue('link_fontdecoration');
        }

        $link_selector = array(
            'a',
            'p a'
        );

        if (!empty($link_styles)) {
            echo kreate_elated_dynamic_css($link_selector, $link_styles);
        }
    }

    add_action('kreate_elated_style_dynamic', 'kreate_elated_link_styles');
}

if (!function_exists('kreate_elated_link_hover_styles')) {

    function kreate_elated_link_hover_styles() {

        $link_hover_styles = array();

        if(kreate_elated_options()->getOptionValue('link_hovercolor') !== '') {
            $link_hover_styles['color'] = kreate_elated_options()->getOptionValue('link_hovercolor');
        }
        if(kreate_elated_options()->getOptionValue('link_hover_fontdecoration') !== '') {
            $link_hover_styles['text-decoration'] = kreate_elated_options()->getOptionValue('link_hover_fontdecoration');
        }

        $link_hover_selector = array(
            'a:hover',
            'p a:hover'
        );

        if (!empty($link_hover_styles)) {
            echo kreate_elated_dynamic_css($link_hover_selector, $link_hover_styles);
        }

        $link_heading_hover_styles = array();

        if(kreate_elated_options()->getOptionValue('link_hovercolor') !== '') {
            $link_heading_hover_styles['color'] = kreate_elated_options()->getOptionValue('link_hovercolor');
        }

        $link_heading_hover_selector = array(
            'h1 a:hover',
            'h2 a:hover',
            'h3 a:hover',
            'h4 a:hover',
            'h5 a:hover',
            'h6 a:hover'
        );

        if (!empty($link_heading_hover_styles)) {
            echo kreate_elated_dynamic_css($link_heading_hover_selector, $link_heading_hover_styles);
        }
    }

    add_action('kreate_elated_style_dynamic', 'kreate_elated_link_hover_styles');
}

if (!function_exists('kreate_elated_sidebar_styles')) {

	function kreate_elated_sidebar_styles() {
		$sidebar_styles = array();

		$background_color = kreate_elated_options()->getOptionValue('sidebar_background_color');
		if ($background_color !== ''){
			$sidebar_styles['background-color'] = $background_color;
		}

		$padding_top = kreate_elated_options()->getOptionValue('sidebar_padding_top');
		if ($padding_top !== ''){
			$sidebar_styles['padding-top'] = kreate_elated_filter_px($padding_top).'px';
		}

		$padding_right = kreate_elated_options()->getOptionValue('sidebar_padding_right');
		if ($padding_right !== ''){
			$sidebar_styles['padding-right'] = kreate_elated_filter_px($padding_right).'px';
		}

		$padding_bottom = kreate_elated_options()->getOptionValue('sidebar_padding_bottom');
		if ($padding_bottom !== ''){
			$sidebar_styles['padding-bottom'] = kreate_elated_filter_px($padding_bottom).'px';
		}

		$padding_left = kreate_elated_options()->getOptionValue('sidebar_padding_left');
		if ($padding_left !== ''){
			$sidebar_styles['padding-left'] = kreate_elated_filter_px($padding_left).'px';
		}

		if(is_array($sidebar_styles) && count($sidebar_styles)){
			echo kreate_elated_dynamic_css('.eltd-sidebar',$sidebar_styles);
		}

	}

	add_action('kreate_elated_style_dynamic', 'kreate_elated_sidebar_styles');
}