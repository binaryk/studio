<?php

if(!function_exists('kreate_elated_design_responsive_styles')) {
    /**
     * Generates general responsive custom styles
     */
    function kreate_elated_design_responsive_styles() {

        $parallax_style = array();
        if (kreate_elated_options()->getOptionValue('parallax_min_height') !== '') {
            $parallax_style['height'] = 'auto !important';
            $parallax_min_height = kreate_elated_options()->getOptionValue('parallax_min_height');
            $parallax_style['min-height'] = kreate_elated_filter_px($parallax_min_height) . 'px';
        }

		echo kreate_elated_dynamic_css('.eltd-section.eltd-parallax-section-holder', $parallax_style);
    }

    add_action('kreate_elated_style_dynamic_responsive_480', 'kreate_elated_design_responsive_styles');
    add_action('kreate_elated_style_dynamic_responsive_480_768', 'kreate_elated_design_responsive_styles');
}

if (!function_exists('kreate_elated_h1_responsive_styles')) {

    function kreate_elated_h1_responsive_styles() {

        $h1_styles = array();

        if(kreate_elated_options()->getOptionValue('h1_responsive_fontsize') !== '') {
            $h1_styles['font-size'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h1_responsive_fontsize')).'px';
        }
        if(kreate_elated_options()->getOptionValue('h1_responsive_lineheight') !== '') {
            $h1_styles['line-height'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h1_responsive_lineheight')).'px';
        }

        $h1_selector = array(
            'h1'
        );

        if (!empty($h1_styles)) {
            echo kreate_elated_dynamic_css($h1_selector, $h1_styles);
        }
    }

    add_action('kreate_elated_style_dynamic_responsive_480_768', 'kreate_elated_h1_responsive_styles');
}

if (!function_exists('kreate_elated_h2_responsive_styles')) {

    function kreate_elated_h2_responsive_styles() {

        $h2_styles = array();

        if(kreate_elated_options()->getOptionValue('h2_responsive_fontsize') !== '') {
            $h2_styles['font-size'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h2_responsive_fontsize')).'px';
        }
        if(kreate_elated_options()->getOptionValue('h2_responsive_lineheight') !== '') {
            $h2_styles['line-height'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h2_responsive_lineheight')).'px';
        }

        $h2_selector = array(
            'h2'
        );

        if (!empty($h2_styles)) {
            echo kreate_elated_dynamic_css($h2_selector, $h2_styles);
        }
    }

    add_action('kreate_elated_style_dynamic_responsive_480_768', 'kreate_elated_h2_responsive_styles');
}

if (!function_exists('kreate_elated_h3_responsive_styles')) {

    function kreate_elated_h3_responsive_styles() {

        $h3_styles = array();

        if(kreate_elated_options()->getOptionValue('h3_responsive_fontsize') !== '') {
            $h3_styles['font-size'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h3_responsive_fontsize')).'px';
        }
        if(kreate_elated_options()->getOptionValue('h3_responsive_lineheight') !== '') {
            $h3_styles['line-height'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h3_responsive_lineheight')).'px';
        }

        $h3_selector = array(
            'h3'
        );

        if (!empty($h3_styles)) {
            echo kreate_elated_dynamic_css($h3_selector, $h3_styles);
        }
    }

    add_action('kreate_elated_style_dynamic_responsive_480_768', 'kreate_elated_h3_responsive_styles');
}

if (!function_exists('kreate_elated_h4_responsive_styles')) {

    function kreate_elated_h4_responsive_styles() {

        $h4_styles = array();

        if(kreate_elated_options()->getOptionValue('h4_responsive_fontsize') !== '') {
            $h4_styles['font-size'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h4_responsive_fontsize')).'px';
        }
        if(kreate_elated_options()->getOptionValue('h4_responsive_lineheight') !== '') {
            $h4_styles['line-height'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h4_responsive_lineheight')).'px';
        }

        $h4_selector = array(
            'h4'
        );

        if (!empty($h4_styles)) {
            echo kreate_elated_dynamic_css($h4_selector, $h4_styles);
        }
    }

    add_action('kreate_elated_style_dynamic_responsive_480_768', 'kreate_elated_h4_responsive_styles');
}

if (!function_exists('kreate_elated_h5_responsive_styles')) {

    function kreate_elated_h5_responsive_styles() {

        $h5_styles = array();

        if(kreate_elated_options()->getOptionValue('h5_responsive_fontsize') !== '') {
            $h5_styles['font-size'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h5_responsive_fontsize')).'px';
        }
        if(kreate_elated_options()->getOptionValue('h5_responsive_lineheight') !== '') {
            $h5_styles['line-height'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h5_responsive_lineheight')).'px';
        }

        $h5_selector = array(
            'h5'
        );

        if (!empty($h5_styles)) {
            echo kreate_elated_dynamic_css($h5_selector, $h5_styles);
        }
    }

    add_action('kreate_elated_style_dynamic_responsive_480_768', 'kreate_elated_h5_responsive_styles');
}

if (!function_exists('kreate_elated_h6_responsive_styles')) {

    function kreate_elated_h6_responsive_styles() {

        $h6_styles = array();

        if(kreate_elated_options()->getOptionValue('h6_responsive_fontsize') !== '') {
            $h6_styles['font-size'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h6_responsive_fontsize')).'px';
        }
        if(kreate_elated_options()->getOptionValue('h6_responsive_lineheight') !== '') {
            $h6_styles['line-height'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h6_responsive_lineheight')).'px';
        }

        $h6_selector = array(
            'h6'
        );

        if (!empty($h6_styles)) {
            echo kreate_elated_dynamic_css($h6_selector, $h6_styles);
        }
    }

    add_action('kreate_elated_style_dynamic_responsive_480_768', 'kreate_elated_h6_responsive_styles');
}

if (!function_exists('kreate_elated_text_responsive_styles')) {

    function kreate_elated_text_responsive_styles() {

        $text_styles = array();

        if(kreate_elated_options()->getOptionValue('text_fontsize_res1') !== '') {
            $text_styles['font-size'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('text_fontsize_res1')).'px';
        }
        if(kreate_elated_options()->getOptionValue('text_lineheight_res1') !== '') {
            $text_styles['line-height'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('text_lineheight_res1')).'px';
        }

        $text_selector = array(
            'body',
            'p'
        );

        if (!empty($text_styles)) {
            echo kreate_elated_dynamic_css($text_selector, $text_styles);
        }
    }

    add_action('kreate_elated_style_dynamic_responsive_480_768', 'kreate_elated_text_responsive_styles');
}

if (!function_exists('kreate_elated_h1_responsive_styles2')) {

    function kreate_elated_h1_responsive_styles2() {

        $h1_styles = array();

        if(kreate_elated_options()->getOptionValue('h1_responsive_fontsize2') !== '') {
            $h1_styles['font-size'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h1_responsive_fontsize2')).'px';
        }
        if(kreate_elated_options()->getOptionValue('h1_responsive_lineheight2') !== '') {
            $h1_styles['line-height'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h1_responsive_lineheight2')).'px';
        }

        $h1_selector = array(
            'h1'
        );

        if (!empty($h1_styles)) {
            echo kreate_elated_dynamic_css($h1_selector, $h1_styles);
        }
    }

    add_action('kreate_elated_style_dynamic_responsive_480', 'kreate_elated_h1_responsive_styles2');
}

if (!function_exists('kreate_elated_h2_responsive_styles2')) {

    function kreate_elated_h2_responsive_styles2() {

        $h2_styles = array();

        if(kreate_elated_options()->getOptionValue('h2_responsive_fontsize2') !== '') {
            $h2_styles['font-size'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h2_responsive_fontsize2')).'px';
        }
        if(kreate_elated_options()->getOptionValue('h2_responsive_lineheight2') !== '') {
            $h2_styles['line-height'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h2_responsive_lineheight2')).'px';
        }

        $h2_selector = array(
            'h2'
        );

        if (!empty($h2_styles)) {
            echo kreate_elated_dynamic_css($h2_selector, $h2_styles);
        }
    }

    add_action('kreate_elated_style_dynamic_responsive_480', 'kreate_elated_h2_responsive_styles2');
}

if (!function_exists('kreate_elated_h3_responsive_styles2')) {

    function kreate_elated_h3_responsive_styles2() {

        $h3_styles = array();

        if(kreate_elated_options()->getOptionValue('h3_responsive_fontsize2') !== '') {
            $h3_styles['font-size'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h3_responsive_fontsize2')).'px';
        }
        if(kreate_elated_options()->getOptionValue('h3_responsive_lineheight2') !== '') {
            $h3_styles['line-height'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h3_responsive_lineheight2')).'px';
        }

        $h3_selector = array(
            'h3'
        );

        if (!empty($h3_styles)) {
            echo kreate_elated_dynamic_css($h3_selector, $h3_styles);
        }
    }

    add_action('kreate_elated_style_dynamic_responsive_480', 'kreate_elated_h3_responsive_styles2');
}

if (!function_exists('kreate_elated_h4_responsive_styles2')) {

    function kreate_elated_h4_responsive_styles2() {

        $h4_styles = array();

        if(kreate_elated_options()->getOptionValue('h4_responsive_fontsize2') !== '') {
            $h4_styles['font-size'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h4_responsive_fontsize2')).'px';
        }
        if(kreate_elated_options()->getOptionValue('h4_responsive_lineheight2') !== '') {
            $h4_styles['line-height'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h4_responsive_lineheight2')).'px';
        }

        $h4_selector = array(
            'h4'
        );

        if (!empty($h4_styles)) {
            echo kreate_elated_dynamic_css($h4_selector, $h4_styles);
        }
    }

    add_action('kreate_elated_style_dynamic_responsive_480', 'kreate_elated_h4_responsive_styles2');
}

if (!function_exists('kreate_elated_h5_responsive_styles2')) {

    function kreate_elated_h5_responsive_styles2() {

        $h5_styles = array();

        if(kreate_elated_options()->getOptionValue('h5_responsive_fontsize2') !== '') {
            $h5_styles['font-size'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h5_responsive_fontsize2')).'px';
        }
        if(kreate_elated_options()->getOptionValue('h5_responsive_lineheight2') !== '') {
            $h5_styles['line-height'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h5_responsive_lineheight2')).'px';
        }

        $h5_selector = array(
            'h5'
        );

        if (!empty($h5_styles)) {
            echo kreate_elated_dynamic_css($h5_selector, $h5_styles);
        }
    }

    add_action('kreate_elated_style_dynamic_responsive_480', 'kreate_elated_h5_responsive_styles2');
}

if (!function_exists('kreate_elated_h6_responsive_styles2')) {

    function kreate_elated_h6_responsive_styles2() {

        $h6_styles = array();

        if(kreate_elated_options()->getOptionValue('h6_responsive_fontsize2') !== '') {
            $h6_styles['font-size'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h6_responsive_fontsize2')).'px';
        }
        if(kreate_elated_options()->getOptionValue('h6_responsive_lineheight2') !== '') {
            $h6_styles['line-height'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('h6_responsive_lineheight2')).'px';
        }

        $h6_selector = array(
            'h6'
        );

        if (!empty($h6_styles)) {
            echo kreate_elated_dynamic_css($h6_selector, $h6_styles);
        }
    }

    add_action('kreate_elated_style_dynamic_responsive_480', 'kreate_elated_h6_responsive_styles2');
}

if (!function_exists('kreate_elated_text_responsive_styles2')) {

    function kreate_elated_text_responsive_styles2() {

        $text_styles = array();

        if(kreate_elated_options()->getOptionValue('text_fontsize_res2') !== '') {
            $text_styles['font-size'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('text_fontsize_res2')).'px';
        }
        if(kreate_elated_options()->getOptionValue('text_lineheight_res2') !== '') {
            $text_styles['line-height'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('text_lineheight_res2')).'px';
        }

        $text_selector = array(
            'body',
            'p'
        );

        if (!empty($text_styles)) {
            echo kreate_elated_dynamic_css($text_selector, $text_styles);
        }
    }

    add_action('kreate_elated_style_dynamic_responsive_480', 'kreate_elated_text_responsive_styles2');
}


if (!function_exists('kreate_elated_content_padding_responsive')) {

    function kreate_elated_content_padding_responsive() {

        $padding = array();

        if(kreate_elated_options()->getOptionValue('content_top_padding_responsive') !== '') {
            $padding['padding-top'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('content_top_padding_responsive')).'px';
        }

        $content_selector = array(
            'body .eltd-wrapper .eltd-content .eltd-content-inner > .eltd-container',
            'body .eltd-wrapper .eltd-content .eltd-content-inner > .eltd-full-width'
        );

        if (!empty($padding)) {
            echo kreate_elated_dynamic_css($content_selector, $padding);
        }
    }

    add_action('kreate_elated_style_dynamic_responsive_600', 'kreate_elated_content_padding_responsive');
}


if(!function_exists('kreate_elated_title_size')){
	/*
	 * Returns title size
	 */

	function kreate_elated_title_size(){
		$title_font_size =  intval(kreate_elated_options()->getOptionValue('page_title_fontsize'));
		$title_h1_font_size = intval(kreate_elated_options()->getOptionValue('h1_fontsize'));

		if ($title_font_size == ''){
			$title_font_size = $title_h1_font_size;
		}

		return $title_font_size;
	}
}

if(!function_exists('kreate_elated_title_responsive_styles_768')){
	/*
	 * Generates title responsive styles
	 */

	function kreate_elated_title_responsive_styles_768(){
		$title_styles = array();

		$title_font_size = kreate_elated_title_size();

		if ($title_font_size !== ''){
			if ($title_font_size > 60){
				$title_styles['font-size'] = ($title_font_size * 0.8).'px';
			}
			elseif($title_font_size > 40){
				$title_styles['font-size'] = ($title_font_size * 0.75).'px';
			}
		}

		if (count($title_styles)){
			echo kreate_elated_dynamic_css(array(
				'.eltd-title .eltd-title-holder h1'
				), $title_styles);
		}
	}

	add_action('kreate_elated_style_dynamic_responsive_480_768','kreate_elated_title_responsive_styles_768');
}

if(!function_exists('kreate_elated_title_responsive_styles_600')){
	/*
	 * Generates title responsive styles
	 */

	function kreate_elated_title_responsive_styles_600(){
		$title_styles = array();

		$title_font_size = kreate_elated_title_size();

		if ($title_font_size !== ''){
			if ($title_font_size > 60){
				$title_styles['font-size'] = ($title_font_size * 0.85).'px';
			}
			elseif($title_font_size > 40){
				$title_styles['font-size'] = ($title_font_size * 0.65).'px';
			}
		}

		if (count($title_styles)){
			echo kreate_elated_dynamic_css(array(
				'.eltd-title .eltd-title-holder h1'
				), $title_styles);
		}
	}

	add_action('kreate_elated_style_dynamic_responsive_600','kreate_elated_title_responsive_styles_600');
}


if(!function_exists('kreate_elated_title_responsive_styles_480')){
	/*
	 * Generates title responsive styles
	 */

	function kreate_elated_title_responsive_styles_480(){
		$title_styles = array();

		$title_font_size = kreate_elated_title_size();

		if ($title_font_size !== ''){
			if ($title_font_size > 60){
				$title_styles['font-size'] = ($title_font_size * 0.6).'px';
			}
			elseif($title_font_size > 40){
				$title_styles['font-size'] = ($title_font_size * 0.4).'px';
			}
		}

		if (count($title_styles)){
			echo kreate_elated_dynamic_css(array(
				'.eltd-title .eltd-title-holder h1'
				), $title_styles);
		}
	}

	add_action('kreate_elated_style_dynamic_responsive_480','kreate_elated_title_responsive_styles_480');
}