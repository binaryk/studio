(function($) {
    'use strict';

    var shortcodes = {};

    eltd.modules.shortcodes = shortcodes;

    shortcodes.eltdInitCounter = eltdInitCounter;
    shortcodes.eltdInitProgressBars = eltdInitProgressBars;
    shortcodes.eltdInitCountdown = eltdInitCountdown;
    shortcodes.eltdInitTestimonials = eltdInitTestimonials;
    shortcodes.eltdInitCarousels = eltdInitCarousels;
    shortcodes.eltdInitPieChart = eltdInitPieChart;
    shortcodes.eltdInitTabs = eltdInitTabs;
    shortcodes.eltdInitTabIcons = eltdInitTabIcons;
    shortcodes.eltdCustomFontResize = eltdCustomFontResize;
    shortcodes.eltdInitImageGallery = eltdInitImageGallery;
    shortcodes.eltdInitAccordions = eltdInitAccordions;
    shortcodes.eltdShowGoogleMap = eltdShowGoogleMap;
    shortcodes.eltdInitPortfolioListMasonry = eltdInitPortfolioListMasonry;
    shortcodes.eltdInitPortfolioListPinterest = eltdInitPortfolioListPinterest;
    shortcodes.eltdInitPortfolio = eltdInitPortfolio;
    shortcodes.eltdInitPortfolioMasonryFilter = eltdInitPortfolioMasonryFilter;
    shortcodes.eltdInitPortfolioLoadMore = eltdInitPortfolioLoadMore;
    shortcodes.eltdProcessSlider = eltdProcessSlider;

    shortcodes.eltdOnDocumentReady = eltdOnDocumentReady;
    shortcodes.eltdOnWindowLoad = eltdOnWindowLoad;
    shortcodes.eltdOnWindowResize = eltdOnWindowResize;
    shortcodes.eltdOnWindowScroll = eltdOnWindowScroll;

    $(document).ready(eltdOnDocumentReady);
    $(window).load(eltdOnWindowLoad);
    $(window).resize(eltdOnWindowResize);
    $(window).scroll(eltdOnWindowScroll);

    /* 
        All functions to be called on $(document).ready() should be in this function
    */
    function eltdOnDocumentReady() {
        eltdInitCounter();
        eltdInitProgressBars();
        eltdInitCountdown();
        eltdIcon().init();
        eltdInitTestimonials();
        eltdInitCarousels();
        eltdInitPieChart();
        eltdInitTabs();
        eltdInitTabIcons();
        eltdButton().init();
        eltdCustomFontResize();
        eltdInitImageGallery();
        eltdInitAccordions();
        eltdShowGoogleMap();
        eltdInitPortfolioListMasonry();
        eltdInitPortfolioListPinterest();
        eltdInitPortfolio();
        eltdInitPortfolioMasonryFilter();
        eltdInitPortfolioLoadMore();
        eltdInitPortfolioSingleArrows();
        eltdSocialIconWidget().init();
    }

    /* 
        All functions to be called on $(window).load() should be in this function
    */
    function eltdOnWindowLoad() {
        eltdButton().initArrowType();
        eltdProcessSlider();
        setTimeout(function(){
            eltdProcessScroll();
        },1000); //beacuse of the safari mac and width calcs problems

        eltd.modules.common.eltdInitParallax();
    }

    /* 
        All functions to be called on $(window).resize() should be in this function
    */
    function eltdOnWindowResize() {
        eltdCustomFontResize();
        eltdInitPortfolioListMasonry();
        eltdInitPortfolioListPinterest();
    }

    /* 
        All functions to be called on $(window).scroll() should be in this function
    */
    function eltdOnWindowScroll() {
        
    }

    /**
     * Counter Shortcode
     */
    function eltdInitCounter() {

        var counters = $('.eltd-counter');


        if (counters.length) {
            counters.each(function() {
                var counter = $(this);
                counter.appear(function() {
                    counter.parent().addClass('eltd-counter-holder-show');

                    //Counter zero type
                    if (counter.hasClass('zero')) {
                        var max = parseFloat(counter.text());
                        counter.countTo({
                            from: 0,
                            to: max,
                            speed: 1500,
                            refreshInterval: 100
                        });
                    } else {
                        counter.absoluteCounter({
                            speed: 2000,
                            fadeInDelay: 1000
                        });
                    }

                },{accX: 0, accY: eltdGlobalVars.vars.eltdElementAppearAmount});
            });
        }

    }
    
    /*
    **	Horizontal progress bars shortcode
    */
    function eltdInitProgressBars(){
        
        var progressBar = $('.eltd-progress-bar');
        
        if(progressBar.length){
            
            progressBar.each(function() {
                
                var thisBar = $(this);
                
                thisBar.appear(function() {
                    eltdInitToCounterProgressBar(thisBar);
                    if(thisBar.find('.eltd-floating.eltd-floating-inside') !== 0){
                        var floatingInsideMargin = thisBar.find('.eltd-progress-content').height();
                        floatingInsideMargin += parseFloat(thisBar.find('.eltd-progress-title-holder').css('padding-bottom'));
                        floatingInsideMargin += parseFloat(thisBar.find('.eltd-progress-title-holder').css('margin-bottom'));
                        thisBar.find('.eltd-floating-inside').css('margin-bottom',-(floatingInsideMargin)+'px');
                    }
                    var percentage = thisBar.find('.eltd-progress-content').data('percentage'),
                        progressContent = thisBar.find('.eltd-progress-content'),
                        progressNumber = thisBar.find('.eltd-progress-number');

                    progressContent.css('width', '0%');
                    progressContent.animate({'width': percentage+'%'}, 1500);
                    progressNumber.css('left', '0%');
                    if (!thisBar.hasClass('eltd-progress-on-side')){
                        progressNumber.animate({'left': percentage+'%'}, 1500);
                    }

                });
            });
        }
    }

    /*
    **	Counter for horizontal progress bars percent from zero to defined percent
    */
    function eltdInitToCounterProgressBar(progressBar){
        var percentage = parseFloat(progressBar.find('.eltd-progress-content').data('percentage'));
        var percent = progressBar.find('.eltd-progress-number .eltd-percent');
        if(percent.length) {
            percent.each(function() {
                var thisPercent = $(this);
                thisPercent.parents('.eltd-progress-number-wrapper').css('opacity', '1');
                thisPercent.countTo({
                    from: 0,
                    to: percentage,
                    speed: 1500,
                    refreshInterval: 50
                });
            });
        }
    }

    /**
     * Countdown Shortcode
     */
    function eltdInitCountdown() {

        var countdowns = $('.eltd-countdown'),
            year,
            month,
            day,
            hour,
            minute,
            timezone,
            monthLabel,
            dayLabel,
            hourLabel,
            minuteLabel,
            secondLabel;

        if (countdowns.length) {

            countdowns.each(function(){

                //Find countdown elements by id-s
                var countdownId = $(this).attr('id'),
                    countdown = $('#'+countdownId),
                    digitFontSize,
                    labelFontSize;

                //Get data for countdown
                year = countdown.data('year');
                month = countdown.data('month');
                day = countdown.data('day');
                hour = countdown.data('hour');
                minute = countdown.data('minute');
                timezone = countdown.data('timezone');
                monthLabel = countdown.data('month-label');
                dayLabel = countdown.data('day-label');
                hourLabel = countdown.data('hour-label');
                minuteLabel = countdown.data('minute-label');
                secondLabel = countdown.data('second-label');
                digitFontSize = countdown.data('digit-size');
                labelFontSize = countdown.data('label-size');


                //Initialize countdown
                countdown.countdown({
                    until: new Date(year, month - 1, day, hour, minute, 44),
                    labels: ['Years', monthLabel, 'Weeks', dayLabel, hourLabel, minuteLabel, secondLabel],
                    format: 'ODHMS',
                    timezone: timezone,
                    padZeroes: true,
                    onTick: setCountdownStyle
                });

                function setCountdownStyle() {
                    countdown.find('.countdown-amount').css({
                        'font-size' : digitFontSize+'px',
                        'line-height' : digitFontSize+'px'
                    });
                    countdown.find('.countdown-period').css({
                        'font-size' : labelFontSize+'px'
                    });
                }

            });

        }

    }

    /**
     * Object that represents icon shortcode
     * @returns {{init: Function}} function that initializes icon's functionality
     */
    var eltdIcon = eltd.modules.shortcodes.eltdIcon = function() {
        //get all icons on page
        var icons = $('.eltd-icon-shortcode');

        /**
         * Function that triggers icon animation and icon animation delay
         */
        var iconAnimation = function(icon) {
            if(icon.hasClass('eltd-icon-animation')) {
                icon.appear(function() {
                    icon.parent('.eltd-icon-animation-holder').addClass('eltd-icon-animation-show');
                }, {accX: 0, accY: eltdGlobalVars.vars.eltdElementAppearAmount});
            }
        };

        /**
         * Function that triggers icon hover color functionality
         */
        var iconHoverColor = function(icon) {
            if(typeof icon.data('hover-color') !== 'undefined') {
                var changeIconColor = function(event) {
                    event.data.icon.css('color', event.data.color);
                };

                var iconElement = icon.find('.eltd-icon-element');
                var hoverColor = icon.data('hover-color');
                var originalColor = iconElement.css('color');

                if(hoverColor !== '') {
                    icon.on('mouseenter', {icon: iconElement, color: hoverColor}, changeIconColor);
                    icon.on('mouseleave', {icon: iconElement, color: originalColor}, changeIconColor);
                }
            }
        };

        /**
         * Function that triggers icon holder background color hover functionality
         */
        var iconHolderBackgroundHover = function(icon) {
            if(typeof icon.data('hover-background-color') !== 'undefined') {
                var changeIconBgColor = function(event) {
                    event.data.icon.css('background-color', event.data.color);
                };

                var hoverBackgroundColor = icon.data('hover-background-color');
                var originalBackgroundColor = icon.css('background-color');

                if(hoverBackgroundColor !== '') {
                    icon.on('mouseenter', {icon: icon, color: hoverBackgroundColor}, changeIconBgColor);
                    icon.on('mouseleave', {icon: icon, color: originalBackgroundColor}, changeIconBgColor);
                }
            }
        };

        /**
         * Function that initializes icon holder border hover functionality
         */
        var iconHolderBorderHover = function(icon) {
            if(typeof icon.data('hover-border-color') !== 'undefined') {
                var changeIconBorder = function(event) {
                    event.data.icon.css('border-color', event.data.color);
                };

                var hoverBorderColor = icon.data('hover-border-color');
                var originalBorderColor = icon.css('border-color');

                if(hoverBorderColor !== '') {
                    icon.on('mouseenter', {icon: icon, color: hoverBorderColor}, changeIconBorder);
                    icon.on('mouseleave', {icon: icon, color: originalBorderColor}, changeIconBorder);
                }
            }
        };

        return {
            init: function() {
                if(icons.length) {
                    icons.each(function() {
                        iconAnimation($(this));
                        iconHoverColor($(this));
                        iconHolderBackgroundHover($(this));
                        iconHolderBorderHover($(this));
                    });

                }
            }
        };
    };

    /**
     * Object that represents social icon widget
     * @returns {{init: Function}} function that initializes icon's functionality
     */
    var eltdSocialIconWidget = eltd.modules.shortcodes.eltdSocialIconWidget = function() {
        //get all social icons on page
        var icons = $('.eltd-social-icon-widget-holder');

        /**
         * Function that triggers icon hover color functionality
         */
        var socialIconHoverColor = function(icon) {
            if(typeof icon.data('hover-color') !== 'undefined') {
                var changeIconColor = function(event) {
                    event.data.icon.css('color', event.data.color);
                };

                var iconElement = icon;
                var hoverColor = icon.data('hover-color');
                var originalColor = iconElement.css('color');

                if(hoverColor !== '') {
                    icon.on('mouseenter', {icon: iconElement, color: hoverColor}, changeIconColor);
                    icon.on('mouseleave', {icon: iconElement, color: originalColor}, changeIconColor);
                }
            }
        };

        return {
            init: function() {
                if(icons.length) {
                    icons.each(function() {
                        socialIconHoverColor($(this));
                    });

                }
            }
        };
    };

    /**
     * Init testimonials shortcode
     */
    function eltdInitTestimonials(){

        var testimonial = $('.eltd-testimonials');
        if(testimonial.length){
            testimonial.each(function(){

                var thisTestimonial = $(this);

                thisTestimonial.appear(function() {
                    thisTestimonial.css('visibility','visible');
                },{accX: 0, accY: eltdGlobalVars.vars.eltdElementAppearAmount});

                var interval = 5000;
                var controlNav = false;
                var directionNav = true;
                var animationSpeed = 400;
                if(typeof thisTestimonial.data('animation-speed') !== 'undefined' && thisTestimonial.data('animation-speed') !== false) {
                    animationSpeed = thisTestimonial.data('animation-speed');
                }

                //var iconClasses = getIconClassesForNavigation(directionNavArrowsTestimonials); TODO

                thisTestimonial.owlCarousel({
                    singleItem: true,
                    autoPlay: interval,
                    navigation: directionNav,
                    transitionStyle : 'goDown', //fade, fadeUp, backSlide, goDown
                    autoHeight: true,
                    pagination: controlNav,
                    slideSpeed: animationSpeed,
                    addClassActive: true,
                    mouseDrag: false,
                    navigationText: [
                        '<span class="eltd-prev-icon"><i class="icon-arrows-slim-left"></i></span>',
                        '<span class="eltd-next-icon"><i class="icon-arrows-slim-right"></i></span>'
                    ],
                    afterMove: afterMove
                });

            	var navPrev = thisTestimonial.find('.owl-prev');

            	navPrev.after('<span class="eltd-nav-line"></span><span class="eltd-nav-line eltd-line-second"></span>');

				function afterMove(){
					var testimonialItem = thisTestimonial.find('.owl-item.active');
					var testimonialItemImageHeight = parseInt(testimonialItem.find('.eltd-testimonial-image-holder').height());
					var testimonialItemImageWidth = parseInt(testimonialItem.find('.eltd-testimonial-image-holder img').width());
					var testimonialItemTextHeight = parseInt(testimonialItem.find('.eltd-testimonial-text').outerHeight(true));
					var navHolder = thisTestimonial.find('.owl-buttons');
					var navHolderLine = navHolder.find('.eltd-nav-line');
					var navHolderLineSecond = navHolder.find('.eltd-nav-line.eltd-line-second');
					var navNext = navHolder.find('.owl-next');

					if (isNaN(testimonialItemImageHeight)){
						testimonialItemImageHeight = 96; //if image height is NaN set it to height of prev/next navigation + margin in order to separate it from text
					}
					if (isNaN(testimonialItemImageWidth)){
						testimonialItemImageWidth = 0;
					}

					var testimonialNavTop = testimonialItemTextHeight+testimonialItemImageHeight/2; //calculate navigation top position

					navHolder.animate({top: testimonialNavTop});
					navHolderLine.css('width',255); //add width to lines
					navHolderLineSecond.css('left',testimonialItemImageWidth); //move second line for image width
					navNext.css('left',testimonialItemImageWidth); //move next nav for image width
					navHolder.css('width',testimonialItemImageWidth + 610); //set navigation holder width as image width + 2 line widths + prev and next width (all in all 310)

                }

                afterMove();

            });

        }

    }

    /**
     * Init Carousel shortcode
     */
    function eltdInitCarousels() {

        var carouselHolders = $('.eltd-carousel-holder'),
            carousel,
            numberOfItems,
            navigation;

        if (carouselHolders.length) {
            carouselHolders.each(function(){
                carousel = $(this).children('.eltd-carousel');
                numberOfItems = carousel.data('items');
                navigation = (carousel.data('navigation') == 'yes') ? true : false;

                //Responsive breakpoints
                var items = [
                    [0,1],
                    [480,2],
                    [768,3],
                    [1024,numberOfItems]
                ];

                carousel.owlCarousel({
                    autoPlay: 3000,
                    items: numberOfItems,
                    itemsCustom: items,
                    pagination: false,
                    navigation: navigation,
                    slideSpeed: 600,
                    navigationText: [
                        '<span class="eltd-prev-icon"><i class="icon-arrows-slim-left"></i></span>',
                        '<span class="eltd-next-icon"><i class="icon-arrows-slim-right"></i></span>'
                    ]
                });

                //appear
                carouselHolders.css('visibility','visible');
                carouselHolders.animate({opacity:1},300);

                var singleCarouselItem = carousel.find('.eltd-carousel-item-holder');

                carouselHolders.appear(function(){
                    setTimeout(function(){
                        singleCarouselItem.each(function(i){
                            var thisItem = $(this);
                            thisItem.delay(i*120).animate({opacity:1,left:0},200,'easeOutSine');
                        });
                    },50);
                },{accX: 0, accY: eltdGlobalVars.vars.eltdElementAppearAmount});

            });
        }

    }

    /**
     * Init Pie Chart and Pie Chart With Icon shortcode
     */
    function eltdInitPieChart() {

        var pieCharts = $('.eltd-pie-chart-holder, .eltd-pie-chart-with-icon-holder');

        if (pieCharts.length) {

            pieCharts.each(function () {

                var pieChart = $(this),
                    percentageHolder = pieChart.children('.eltd-percentage, .eltd-percentage-with-icon'),
                    barColor = eltdGlobalVars.vars.eltdFirstColor,
                    trackColor = '#dedede',
                    lineWidth,
                    size = 190;

                percentageHolder.appear(function() {
                    initToCounterPieChart(pieChart);
                    percentageHolder.css('opacity', '1');

                    percentageHolder.easyPieChart({
                        barColor: barColor,
                        trackColor: trackColor,
                        scaleColor: false,
                        lineCap: 'butt',
                        lineWidth: lineWidth,
                        animate: 1500,
                        size: size
                    });
                },{accX: 0, accY: eltdGlobalVars.vars.eltdElementAppearAmount});

            });

        }

    }

    /*
     **	Counter for pie chart number from zero to defined number
     */
    function initToCounterPieChart( pieChart ){

        pieChart.css('opacity', '1');
        var counter = pieChart.find('.eltd-to-counter'),
            max = parseFloat(counter.text());
        counter.countTo({
            from: 0,
            to: max,
            speed: 1500,
            refreshInterval: 50
        });

    }

    /*
    **	Init tabs shortcode
    */
    function eltdInitTabs(){

       var tabs = $('.eltd-tabs');
        if(tabs.length){
            tabs.each(function(){
                var thisTabs = $(this),
                	backgroundHolderBckg = '';

                thisTabs.children('.eltd-tab-container').each(function(index){
                    index = index + 1;
                    var that = $(this),
                        link = that.attr('id'),
                        navItem = that.parent().find('.eltd-tabs-nav li:nth-child('+index+') a'),
                        navLink = navItem.attr('href');

                    link = '#'+link;

                    if(link.indexOf(navLink) > -1) {
                        navItem.attr('href',link);
                    }
                });

                var columnHolderBckg = thisTabs.closest('.wpb_column').css('background-color');
                var rowHolderBckg = thisTabs.closest('.vc_row').css('background-color');
                if(columnHolderBckg !== 'rgba(0, 0, 0, 0)' && columnHolderBckg !== 'transparent'){
                	backgroundHolderBckg = columnHolderBckg;
                }
                else if(rowHolderBckg !== 'rgba(0, 0, 0, 0)' && rowHolderBckg !== 'transparent'){
                	backgroundHolderBckg = rowHolderBckg;
                }

                if(thisTabs.hasClass('eltd-horizontal')){
                    thisTabs.tabs();
                }
                else if(thisTabs.hasClass('eltd-vertical')){
                    thisTabs.tabs({
						activate: function(){
							if (backgroundHolderBckg !== ''){
								thisTabs.find('li.ui-state-active a').css('background-color',backgroundHolderBckg);
							}
							eltdButton().initArrowType();
						},
						beforeActivate: function(){
							if (backgroundHolderBckg !== ''){
								thisTabs.find('li.ui-state-active a').css('background-color','transparent');
							}
						}
                    }).addClass( 'ui-tabs-vertical ui-helper-clearfix' );
                    thisTabs.find('.eltd-tabs-nav > ul >li').removeClass( 'ui-corner-top' ).addClass( 'ui-corner-left' );
					if (backgroundHolderBckg !== ''){
						thisTabs.find('li.ui-state-active a').css('background-color',backgroundHolderBckg);
					}
                }
            });
        }
    }

    /*
    **	Generate icons in tabs navigation
    */
    function eltdInitTabIcons(){

        var tabContent = $('.eltd-tab-container');
        if(tabContent.length){

            tabContent.each(function(){
                var thisTabContent = $(this);

                var id = thisTabContent.attr('id');
                var icon = '';
                if(typeof thisTabContent.data('icon-html') !== 'undefined' || thisTabContent.data('icon-html') !== 'false') {
                    icon = thisTabContent.data('icon-html');
                }

                var tabNav = thisTabContent.parents('.eltd-tabs').find('.eltd-tabs-nav > li > a[href=#'+id+']');

                if(typeof(tabNav) !== 'undefined') {
                    tabNav.children('.eltd-icon-frame').append(icon);
                }
            });
        }
    }

    /**
     * Button object that initializes whole button functionality
     * @type {Function}
     */
    var eltdButton = eltd.modules.shortcodes.eltdButton = function() {
        //all buttons on the page
        var buttons = $('.eltd-btn');
        var buttonsArrowType = $('.eltd-btn.eltd-btn-arrow');

        /**
         * Initializes button hover color
         * @param button current button
         */
        var buttonHoverColor = function(button) {
            if(typeof button.data('hover-color') !== 'undefined') {
                var changeButtonColor = function(event) {
                    event.data.button.css('color', event.data.color);
                };

                var originalColor = button.css('color');
                var hoverColor = button.data('hover-color');

                button.on('mouseenter', { button: button, color: hoverColor }, changeButtonColor);
                button.on('mouseleave', { button: button, color: originalColor }, changeButtonColor);
            }
        };



        /**
         * Initializes button hover background color
         * @param button current button
         */
        var buttonHoverBgColor = function(button) {
            if(typeof button.data('hover-bg-color') !== 'undefined') {
                var changeButtonBg = function(event) {
                    event.data.button.css('background-color', event.data.color);
                };

                var originalBgColor = button.css('background-color');
                var hoverBgColor = button.data('hover-bg-color');

                button.on('mouseenter', { button: button, color: hoverBgColor }, changeButtonBg);
                button.on('mouseleave', { button: button, color: originalBgColor }, changeButtonBg);
            }
        };

        /**
         * Initializes button border color
         * @param button
         */
        var buttonHoverBorderColor = function(button) {
            if(typeof button.data('hover-border-color') !== 'undefined') {
                var changeBorderColor = function(event) {
                    event.data.button.css('border-color', event.data.color);
                };

                var originalBorderColor = button.css('borderTopColor'); //take one of the four sides
                var hoverBorderColor = button.data('hover-border-color');

                button.on('mouseenter', { button: button, color: hoverBorderColor }, changeBorderColor);
                button.on('mouseleave', { button: button, color: originalBorderColor }, changeBorderColor);
            }
        };

        /*
        * Button type arrow width
        * @param button
        */
        var buttonArrowWidth = function(button) {
        	button.addClass('eltd-btn-visible');

        	var buttonArrowTextWidth = parseInt(button.find('.eltd-btn-text').width());
        	var buttonArrowIconWidth = parseInt(button.find('.eltd-btn-icon-arrow').width());
        	var buttonWidth;

            if (button.hasClass('eltd-btn-rotate')) {
            	if (buttonArrowIconWidth > buttonArrowTextWidth){
            		buttonWidth = buttonArrowIconWidth;
            	}
            	else{
            		buttonWidth = buttonArrowTextWidth;
            	}

                button.animate({width: buttonWidth + 20 + 'px'});  //20 to ensure text visibility during the animation

            } else if (button.hasClass('eltd-btn-hide-text')) {

                buttonWidth = buttonArrowIconWidth + buttonArrowTextWidth + 15; //15 empty space between arrow and text
                var textPadding = buttonArrowIconWidth;

                button.animate({width: buttonWidth + 'px'});
            	var buttonText = button.find('.eltd-btn-text');
            	var buttonArrow = button.find('.eltd-btn-icon-arrow');
                buttonText.css({'padding-left':textPadding + 15 +'px'}); //15 empty space between arrow and text

                button.on('mouseenter touch',function(){
					buttonArrow.css('width','85%');
                    buttonText.css({'-webkit-transform':'translateX('+(buttonArrowTextWidth+3)+'px)'}); //3 for hiding text fully when translating
                    buttonText.css({'transform':'translateX('+(buttonArrowTextWidth+3)+'px)'}); //3 for hiding text fully when translating
                });
                button.on('mouseleave',function(){
                	buttonArrow.css('width',buttonArrowIconWidth);
                    buttonText.css({'-webkit-transform':'translateX(0px)'});
                    buttonText.css({'transform':'translateX(0px)'});
                });
            }

            
        };

        return {
            init: function() {
                if(buttons.length) {
                    buttons.each(function() {
                        buttonHoverColor($(this));
                        buttonHoverBgColor($(this));
                        buttonHoverBorderColor($(this));
                    });
                }
            },
            initArrowType: function() {
                if(buttonsArrowType.length) {
                    buttonsArrowType.each(function() {
                        buttonArrowWidth($(this));
                    });
                }
            }
        };
    };
    

	/*
	**	Custom Font resizing
	*/
	function eltdCustomFontResize(){
		var customFont = $('.eltd-custom-font-holder');
		if (customFont.length){
			customFont.each(function(){
				var thisCustomFont = $(this);
				var fontSize;
				var lineHeight;
				var coef1 = 1;
				var coef2 = 1;

				if (eltd.windowWidth < 1200){
					coef1 = 0.8;
				}

				if (eltd.windowWidth < 1000){
					coef1 = 0.7;
				}

				if (eltd.windowWidth < 768){
					coef1 = 0.6;
					coef2 = 0.7;
				}

				if (eltd.windowWidth < 600){
					coef1 = 0.4;
					coef2 = 0.6;
				}

				if (eltd.windowWidth < 480){
					coef1 = 0.3;
					coef2 = 0.5;
				}

				if (typeof thisCustomFont.data('font-size') !== 'undefined' && thisCustomFont.data('font-size') !== false) {
					fontSize = parseInt(thisCustomFont.data('font-size'));

					if (fontSize > 70) {
						fontSize = Math.round(fontSize*coef1);
					}
					else if (fontSize > 35) {
						fontSize = Math.round(fontSize*coef2);
					}

					thisCustomFont.css('font-size',fontSize + 'px');
				}

				if (typeof thisCustomFont.data('line-height') !== 'undefined' && thisCustomFont.data('line-height') !== false) {
					lineHeight = parseInt(thisCustomFont.data('line-height'));

					if (lineHeight > 70 && eltd.windowWidth < 1200) {
						lineHeight = '1.2em';
					}
					else if (lineHeight > 35 && eltd.windowWidth < 768) {
						lineHeight = '1.2em';
					}
					else{
						lineHeight += 'px';
					}

					thisCustomFont.css('line-height', lineHeight);
				}
			});
		}
	}

    /*
     **	Show Google Map
     */
    function eltdShowGoogleMap(){

        if($('.eltd-google-map').length){
            $('.eltd-google-map').each(function(){

                var element = $(this);

                var customMapStyle;
                if(typeof element.data('custom-map-style') !== 'undefined') {
                    customMapStyle = element.data('custom-map-style');
                }

                var colorOverlay;
                if(typeof element.data('color-overlay') !== 'undefined' && element.data('color-overlay') !== false) {
                    colorOverlay = element.data('color-overlay');
                }

                var saturation;
                if(typeof element.data('saturation') !== 'undefined' && element.data('saturation') !== false) {
                    saturation = element.data('saturation');
                }

                var lightness;
                if(typeof element.data('lightness') !== 'undefined' && element.data('lightness') !== false) {
                    lightness = element.data('lightness');
                }

                var zoom;
                if(typeof element.data('zoom') !== 'undefined' && element.data('zoom') !== false) {
                    zoom = element.data('zoom');
                }

                var pin;
                if(typeof element.data('pin') !== 'undefined' && element.data('pin') !== false) {
                    pin = element.data('pin');
                }

                var mapHeight;
                if(typeof element.data('height') !== 'undefined' && element.data('height') !== false) {
                    mapHeight = element.data('height');
                }

                var uniqueId;
                if(typeof element.data('unique-id') !== 'undefined' && element.data('unique-id') !== false) {
                    uniqueId = element.data('unique-id');
                }

                var scrollWheel;
                if(typeof element.data('scroll-wheel') !== 'undefined') {
                    scrollWheel = element.data('scroll-wheel');
                }
                var addresses;
                if(typeof element.data('addresses') !== 'undefined' && element.data('addresses') !== false) {
                    addresses = element.data('addresses');
                }

                var map = "map_"+ uniqueId;
                var geocoder = "geocoder_"+ uniqueId;
                var holderId = "eltd-map-"+ uniqueId;

                eltdInitializeGoogleMap(customMapStyle, colorOverlay, saturation, lightness, scrollWheel, zoom, holderId, mapHeight, pin,  map, geocoder, addresses);
            });
        }

    }
    /*
     **	Init Google Map
     */
    function eltdInitializeGoogleMap(customMapStyle, color, saturation, lightness, wheel, zoom, holderId, height, pin,  map, geocoder, data){

        var mapStyles = [
            {
                stylers: [
                    {hue: color },
                    {saturation: saturation},
                    {lightness: lightness},
                    {gamma: 1}
                ]
            }
        ];

        var googleMapStyleId;

        if(customMapStyle){
            googleMapStyleId = 'eltd-style';
        } else {
            googleMapStyleId = google.maps.MapTypeId.ROADMAP;
        }

        var qoogleMapType = new google.maps.StyledMapType(mapStyles,
            {name: "Elated Google Map"});

        geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(-34.397, 150.644);

        if (!isNaN(height)){
            height = height + 'px';
        }

        var myOptions = {

            zoom: zoom,
            scrollwheel: wheel,
            center: latlng,
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL,
                position: google.maps.ControlPosition.RIGHT_CENTER
            },
            scaleControl: false,
            scaleControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            streetViewControl: false,
            streetViewControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            panControl: false,
            panControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            mapTypeControl: false,
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'eltd-style'],
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            mapTypeId: googleMapStyleId
        };

        map = new google.maps.Map(document.getElementById(holderId), myOptions);
        map.mapTypes.set('eltd-style', qoogleMapType);

        var index;

        for (index = 0; index < data.length; ++index) {
            eltdInitializeGoogleAddress(data[index], pin, map, geocoder);
        }

        var holderElement = document.getElementById(holderId);
        holderElement.style.height = height;
    }
    /*
     **	Init Google Map Addresses
     */
    function eltdInitializeGoogleAddress(data, pin,  map, geocoder){
        if (data === '')
            return;
        var contentString = '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
            '<div id="bodyContent">'+
            '<p>'+data+'</p>'+
            '</div>'+
            '</div>';
        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });
        geocoder.geocode( { 'address': data}, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location,
                    icon:  pin,
                    title: data['store_title']
                });
                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.open(map,marker);
                });

                google.maps.event.addDomListener(window, 'resize', function() {
                    map.setCenter(results[0].geometry.location);
                });

            }
        });
    }

    function eltdInitAccordions(){
        var accordion = $('.eltd-accordion-holder');
        if(accordion.length){
            accordion.each(function(){

               var thisAccordion = $(this);

				if(thisAccordion.hasClass('eltd-accordion')){

					thisAccordion.accordion({
						animate: "swing",
						collapsible: true,
						active: 0,
						icons: "",
						heightStyle: "content",
						activate: function () {
							eltdButton().initArrowType();
						}
					});
				}

				if(thisAccordion.hasClass('eltd-toggle')){

					var toggleAccordion = $(this);
					var toggleAccordionTitle = toggleAccordion.find('.eltd-title-holder');
					var toggleAccordionContent = toggleAccordionTitle.next();

					toggleAccordion.addClass("accordion ui-accordion ui-accordion-icons ui-widget ui-helper-reset");
					toggleAccordionTitle.addClass("ui-accordion-header ui-helper-reset ui-state-default ui-corner-top ui-corner-bottom");
					toggleAccordionContent.addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom").hide();

					toggleAccordionTitle.each(function(){
						var thisTitle = $(this);
						thisTitle.hover(function(){
							thisTitle.toggleClass("ui-state-hover");
						});

						thisTitle.on('click',function(){
							thisTitle.toggleClass('ui-accordion-header-active ui-state-active ui-state-default ui-corner-bottom');
							thisTitle.next().toggleClass('ui-accordion-content-active').slideToggle(400);
                            eltdButton().initArrowType();
						});
					});
				}
            });
        }
    }

    function eltdInitImageGallery() {

        var galleries = $('.eltd-image-gallery');

        if (galleries.length) {
            galleries.each(function () {
                var gallery = $(this).children('.eltd-image-gallery-slider'),
                    autoplay = gallery.data('autoplay'),
                    animation = (gallery.data('animation') == 'slide') ? false : gallery.data('animation'),
                    navigation = (gallery.data('navigation') == 'yes'),
                    pagination = (gallery.data('pagination') == 'yes');

                gallery.owlCarousel({
                    singleItem: true,
                    autoPlay: autoplay * 1000,
                    navigation: navigation,
                    transitionStyle : animation, //fade, fadeUp, backSlide, goDown
                    autoHeight: true,
                    pagination: pagination,
                    slideSpeed: 600,
                    navigationText: [
                        '<span class="eltd-prev-icon"><i class="icon-arrows-slim-left"></i></span>',
                        '<span class="eltd-next-icon"><i class="icon-arrows-slim-right"></i></span>'
                    ]
                });
            });
        }

    }
    /**
     * Initializes portfolio list
     */
    function eltdInitPortfolio(){
        var portList = $('.eltd-portfolio-list-holder-outer.eltd-ptf-standard, .eltd-portfolio-list-holder-outer.eltd-ptf-gallery');
        if(portList.length){            
            portList.each(function(){
                var thisPortList = $(this);
                thisPortList.appear(function(){
                    eltdInitPortMixItUp(thisPortList);
                });
            });
        }
    }

    /**
     * Initializes portfolio list
     */
    function eltdInitPortfolioSingleArrows() {
        if($('.eltd-portfolio-single-nav').length) {

            var button = $('.eltd-portfolio-prev a, .eltd-portfolio-next a');

            button.addClass('eltd-btn-visible');

            button.each(function() {
                var button = $(this);
                var buttonArrowTextWidth = parseInt(button.find('.eltd-btn-text').width());
                var buttonArrowIconWidth = parseInt(button.find('.eltd-btn-icon-arrow').width());
                var buttonWidth;

                buttonWidth = buttonArrowIconWidth + buttonArrowTextWidth + 15; //15 empty space between arrow and text
                var textPadding = buttonArrowIconWidth;

                button.css('width', buttonWidth + 'px');
                if(button.parent().hasClass('eltd-portfolio-next')) {
                    button.find('.eltd-btn-text').css({'padding-left': textPadding + 15 + 'px'}); //15 empty space between arrow and text
                } else {
                    button.find('.eltd-btn-text').css({'padding-right': textPadding + 15 + 'px'}); //15 empty space between arrow and text
                }

                button.mouseenter(function() {
                    if(button.parent().hasClass('eltd-portfolio-next')) {
                        button.find('.eltd-btn-text').css({'-webkit-transform': 'translateX(' + buttonArrowTextWidth + 'px)'});
                        button.find('.eltd-btn-text').css({'transform': 'translateX(' + buttonArrowTextWidth + 'px)'});
                    } else {
                        button.find('.eltd-btn-text').css({'-webkit-transform': 'translateX(' + -buttonArrowTextWidth + 'px)'});
                        button.find('.eltd-btn-text').css({'transform': 'translateX(' + -buttonArrowTextWidth + 'px)'});
                    }
                });
                button.mouseleave(function() {
                    button.find('.eltd-btn-text').css({'-webkit-transform': 'translateX(0px)'});
                    button.find('.eltd-btn-text').css({'transform': 'translateX(0px)'});
                });
            });

        }
    }
    /**
     * Initializes mixItUp function for specific container
     */
    function eltdInitPortMixItUp(container){
        var filterClass = '',
            loadMore = container.find('.eltd-ptf-list-paging');

        if(container.hasClass('eltd-ptf-has-filter')){
            filterClass = container.find('.eltd-portfolio-filter-holder-inner ul li').data('class');
            filterClass = '.'+filterClass;
        }
        
        var holderInner = container.find('.eltd-portfolio-list-holder');
        holderInner.mixItUp({
            callbacks: {
                onMixLoad: function(){
                    holderInner.find('article').css('visibility','visible');
                    eltdButton().initArrowType(); //because of the button that can be loaded on portfolio list
                    loadMore.animate({opacity:1},300); //add opacity to load more button
                    eltd.modules.common.eltdInitParallax();
                },
                onMixStart: function(){
                    holderInner.find('article').css('visibility','visible');

                },
                onMixBusy: function(){
                    holderInner.find('article').css('visibility','visible');
                } 
            },           
            selectors: {
                filter: filterClass
            },
            animation: {
                effects: 'fade translateY(80px) stagger(150ms)',
                duration: 300,
                easing: 'ease-in-out',
            }
            
        });
        
    }
     /*
    **	Init portfolio list masonry type
    */
    function eltdInitPortfolioListMasonry(){
        var portList = $('.eltd-portfolio-list-holder-outer.eltd-ptf-masonry');
        if(portList.length) {
            portList.each(function() {
                var thisPortList = $(this).children('.eltd-portfolio-list-holder');
                var size = thisPortList.find('.eltd-portfolio-list-masonry-grid-sizer').width();
                eltdResizeMasonry(size,thisPortList);
                
                eltdInitMasonry(thisPortList);
                $(window).resize(function(){
                    eltdResizeMasonry(size,thisPortList);
                    eltdInitMasonry(thisPortList);
                });
            });
        }
    }
    
    function eltdInitMasonry(container){
        container.animate({opacity: 1});
        container.isotope({
            itemSelector: '.eltd-portfolio-item',
            masonry: {
                columnWidth: '.eltd-portfolio-list-masonry-grid-sizer'
            }
        });
    }
    
    function eltdResizeMasonry(size,container){
        
        var defaultMasonryItem = container.find('.eltd-default-masonry-item');
        var largeWidthMasonryItem = container.find('.eltd-large-width-masonry-item');
        var largeHeightMasonryItem = container.find('.eltd-large-height-masonry-item');
        var largeWidthHeightMasonryItem = container.find('.eltd-large-width-height-masonry-item');

        defaultMasonryItem.css('height', size);
        largeHeightMasonryItem.css('height', Math.round(2*size));

        if(eltd.windowWidth > 600){
            largeWidthHeightMasonryItem.css('height', Math.round(2*size));
            largeWidthMasonryItem.css('height', size);
        }else{
            largeWidthHeightMasonryItem.css('height', size);
            largeWidthMasonryItem.css('height', Math.round(size/2));

        }
    }
    /**
     * Initializes portfolio pinterest 
     */
    function eltdInitPortfolioListPinterest(){
        
        var portList = $('.eltd-portfolio-list-holder-outer.eltd-ptf-pinterest');
        if(portList.length) {
            portList.each(function() {
                var thisPortList = $(this).children('.eltd-portfolio-list-holder');
                eltdInitPinterest(thisPortList);
                $(window).resize(function(){
                     eltdInitPinterest(thisPortList);
                });
            });
            
        }
    }
    
    function eltdInitPinterest(container){
        container.animate({opacity: 1});
        container.isotope({
            itemSelector: '.eltd-portfolio-item',
            masonry: {
                columnWidth: '.eltd-portfolio-list-masonry-grid-sizer'
            }
        });
        
    }
    /**
     * Initializes portfolio masonry filter
     */
    function eltdInitPortfolioMasonryFilter(){
        
        var filterHolder = $('.eltd-portfolio-filter-holder.eltd-masonry-filter');
        
        if(filterHolder.length){
            filterHolder.each(function(){
               
                var thisFilterHolder = $(this);
                
                var portfolioIsotopeAnimation = null;
                
                var filter = thisFilterHolder.find('ul li').data('class');
                
                thisFilterHolder.find('.filter:first').addClass('current');
                
                thisFilterHolder.find('.filter').click(function(){

                    var currentFilter = $(this);
                    clearTimeout(portfolioIsotopeAnimation);

                    $('.isotope, .isotope .isotope-item').css('transition-duration','0.8s');

                    portfolioIsotopeAnimation = setTimeout(function(){
                        $('.isotope, .isotope .isotope-item').css('transition-duration','0s'); 
                    },700);

                    var selector = $(this).attr('data-filter');
                    thisFilterHolder.siblings('.eltd-portfolio-list-holder-outer').find('.eltd-portfolio-list-holder').isotope({ filter: selector });

                    thisFilterHolder.find('.filter').removeClass('current');
                    currentFilter.addClass('current');

                    return false;

                });
                
            });
        }
    }
    /**
     * Initializes portfolio load more function
     */
    function eltdInitPortfolioLoadMore(){
        var portList = $('.eltd-portfolio-list-holder-outer.eltd-ptf-load-more');
        if(portList.length){
            portList.each(function(){
                
                var thisPortList = $(this);
                var thisPortListInner = thisPortList.find('.eltd-portfolio-list-holder');
                var nextPage; 
                var maxNumPages;
                var loadMoreButton = thisPortList.find('.eltd-ptf-list-load-more a');
                
                if (typeof thisPortList.data('max-num-pages') !== 'undefined' && thisPortList.data('max-num-pages') !== false) {  
                    maxNumPages = thisPortList.data('max-num-pages');
                }
                
                loadMoreButton.on('click', function (e) {  
                    var loadMoreDatta = eltdGetPortfolioAjaxData(thisPortList);
                    nextPage = loadMoreDatta.nextPage;
                    e.preventDefault();
                    e.stopPropagation(); 
                    if(nextPage <= maxNumPages){
                        var ajaxData = eltdSetPortfolioAjaxData(loadMoreDatta);
                        $.ajax({
                            type: 'POST',
                            data: ajaxData,
                            url: eltdCoreAjaxUrl,
                            success: function (data) {
                                nextPage++;
                                thisPortList.data('next-page', nextPage);
                                var response = $.parseJSON(data);
                                var responseHtml = eltdConvertHTML(response.html); //convert response html into jQuery collection that Mixitup can work with
                                thisPortList.waitForImages(function(){    
                                    setTimeout(function() {
                                        if(thisPortList.hasClass('eltd-ptf-masonry') || thisPortList.hasClass('eltd-ptf-pinterest') ){
                                            thisPortListInner.isotope().append( responseHtml ).isotope( 'appended', responseHtml ).isotope('reloadItems');
                                            eltdButton().initArrowType(); //because of the button that can be loaded on portfolio list
                                        } else {
                                            thisPortListInner.mixItUp('append',responseHtml, function() {
                                                eltdButton().initArrowType(); //because of the button that can be loaded on portfolio list
                                                }
                                            );
                                        }
                                        eltdButton().initArrowType();
                                    },400);                                    
                                });                           
                            }
                        });
                    }
                    if(nextPage === maxNumPages){
                        loadMoreButton.closest('.eltd-ptf-list-load-more').fadeOut();
                    }
                });
                
            });
        }
    }
    
    function eltdConvertHTML ( html ) {
        var newHtml = $.trim( html ),
                $html = $(newHtml ),
                $empty = $();

        $html.each(function ( index, value ) {
            if ( value.nodeType === 1) {
                $empty = $empty.add ( this );
            }
        });

        return $empty;
    }

    /**
     * Initializes portfolio load more data params
     * @param portfolio list container with defined data params
     * return array
     */
    function eltdGetPortfolioAjaxData(container){
        var returnValue = {};
        
        returnValue.type = '';
        returnValue.columns = '';
        returnValue.gridSize = '';
        returnValue.orderBy = '';
        returnValue.order = '';
        returnValue.number = '';
        returnValue.imageSize = '';
        returnValue.filter = '';
        returnValue.filterOrderBy = '';
        returnValue.category = '';
        returnValue.selectedProjectes = '';
        returnValue.showLoadMore = '';
        returnValue.titleTag = '';
        returnValue.nextPage = '';
        returnValue.maxNumPages = '';
        
        if (typeof container.data('type') !== 'undefined' && container.data('type') !== false) {
            returnValue.type = container.data('type');
        }
        if (typeof container.data('grid-size') !== 'undefined' && container.data('grid-size') !== false) {                    
            returnValue.gridSize = container.data('grid-size');
        }
        if (typeof container.data('columns') !== 'undefined' && container.data('columns') !== false) {                    
            returnValue.columns = container.data('columns');
        }
        if (typeof container.data('order-by') !== 'undefined' && container.data('order-by') !== false) {                    
            returnValue.orderBy = container.data('order-by');
        }
        if (typeof container.data('order') !== 'undefined' && container.data('order') !== false) {                    
            returnValue.order = container.data('order');
        }
        if (typeof container.data('number') !== 'undefined' && container.data('number') !== false) {                    
            returnValue.number = container.data('number');
        }
        if (typeof container.data('image-size') !== 'undefined' && container.data('image-size') !== false) {                    
            returnValue.imageSize = container.data('image-size');
        }
        if (typeof container.data('filter') !== 'undefined' && container.data('filter') !== false) {                    
            returnValue.filter = container.data('filter');
        }
        if (typeof container.data('filter-order-by') !== 'undefined' && container.data('filter-order-by') !== false) {                    
            returnValue.filterOrderBy = container.data('filter-order-by');
        }
        if (typeof container.data('category') !== 'undefined' && container.data('category') !== false) {                    
            returnValue.category = container.data('category');
        }
        if (typeof container.data('selected-projects') !== 'undefined' && container.data('selected-projects') !== false) {                    
            returnValue.selectedProjectes = container.data('selected-projects');
        }
        if (typeof container.data('show-load-more') !== 'undefined' && container.data('show-load-more') !== false) {                    
            returnValue.showLoadMore = container.data('show-load-more');
        }
        if (typeof container.data('title-tag') !== 'undefined' && container.data('title-tag') !== false) {                    
            returnValue.titleTag = container.data('title-tag');
        }
        if (typeof container.data('next-page') !== 'undefined' && container.data('next-page') !== false) {                    
            returnValue.nextPage = container.data('next-page');
        }
        if (typeof container.data('max-num-pages') !== 'undefined' && container.data('max-num-pages') !== false) {                    
            returnValue.maxNumPages = container.data('max-num-pages');
        }
        return returnValue;
    }
     /**
     * Sets portfolio load more data params for ajax function
     * @param portfolio list container with defined data params
     * return array
     */
    function eltdSetPortfolioAjaxData(container){
        var returnValue = {
            action: 'eltd_core_portfolio_ajax_load_more',
            type: container.type,
            columns: container.columns,
            gridSize: container.gridSize,
            orderBy: container.orderBy,
            order: container.order,
            number: container.number,
            imageSize: container.imageSize,
            filter: container.filter,
            filterOrderBy: container.filterOrderBy,
            category: container.category,
            selectedProjectes: container.selectedProjectes,
            showLoadMore: container.showLoadMore,
            titleTag: container.titleTag,
            nextPage: container.nextPage
        };
        return returnValue;
    }

    /*
    * Process Slider init
    */

    function eltdProcessSlider() {
        var processSlider = $('.eltd-process-slider');

        if (processSlider.length) {
            processSlider.each(function(){
                //vars
                var thisProcessSlider = $(this),
                    processSliderTitleArea = $(this).find('.eltd-process-slider-title-area'),
                    processSliderItems = $(this).find('.eltd-process-slider-content-area'),
                    processSliderItem = processSliderItems.find('.eltd-process-slider-item'),
                    processSliderNext = processSliderTitleArea.find('.eltd-process-slider-next-nav'),
                    processSliderHandler = thisProcessSlider.parent().parent().find('.eltd-process-handle'),
                    cycleTimeout= null,
                    processSliderContentWidth = processSliderItem.length*processSliderItem.outerWidth(),
                    processSliderContentHeight = processSliderItem.outerHeight(),
                    processSliderWidth = processSliderContentWidth + processSliderTitleArea.outerWidth(),
                    leftOffset = (Math.max(eltd.windowWidth - eltd.gridWidth , 0) / 2);

                    //appears
                    setTimeout(function(){
                        processSliderItem.each(function(i){
                            $(this).animate({opacity:1},400, 'easeOutSine');
                            processSliderHandler.parent().animate({opacity:1},400, 'easeOutSine');
                            $(this).find('.eltd-process-slide-item-number').append(i+1);
                        });
                    },200);

                        //calcs
                        thisProcessSlider.css({width:processSliderContentWidth+processSliderTitleArea.outerWidth()+'px'});
                        thisProcessSlider.css({height:processSliderContentHeight+'px'});
                        thisProcessSlider.css({'-webkit-transform':'translateX('+leftOffset+'px)'});
                        thisProcessSlider.css({'transform':'translateX('+leftOffset+'px)'});
                        thisProcessSlider.delay(200).animate({opacity:1},200);

                        //recalcs
                        $(window).resize(function(){
                            leftOffset = (Math.max($(window).width() - eltd.gridWidth , 0) / 2);
                            processSliderContentWidth = processSliderItem.length*processSliderItem.outerWidth();
                            processSliderContentHeight = processSliderItem.outerHeight();
                            processSliderWidth = processSliderContentWidth + processSliderTitleArea.outerWidth();
                            thisProcessSlider.css({width:processSliderContentWidth+processSliderTitleArea.outerWidth()+'px'});
                            thisProcessSlider.css({height:processSliderContentHeight+'px'});
                            thisProcessSlider.css({'-webkit-transform':'translateX('+leftOffset+'px)'});
                            thisProcessSlider.css({'transform':'translateX('+leftOffset+'px)'});
                            processSliderItem.each(function(i){
                                $(this).delay(200*i).animate({opacity:1},400, 'easeOutSine');
                                processSliderHandler.parent().animate({opacity:1},400, 'easeOutSine');
                            });
                        });

                        // clicks
                        processSliderNext.click(function(event){
                            event.preventDefault();
                            clearTimeout(cycleTimeout);
                            cycleTimeout = setTimeout(function(){
                                if(processSliderItems.hasClass('eltd-moved')) {
                                    setTimeout(function(){
                                        thisProcessSlider.css({'-webkit-transform':'translateX('+leftOffset+'px)'});
                                        thisProcessSlider.css({'transform':'translateX('+leftOffset+'px)'});
                                        processSliderHandler.css({marginLeft:0});
                                        processSliderItems.removeClass('eltd-moved');
                                    },500);
                                } else {
                                    setTimeout(function(){
                                        if (eltd.gridWidth < $(window).width()) {
                                            thisProcessSlider.css({'-webkit-transform':'translateX(0)'});
                                            thisProcessSlider.css({'transform':'translateX(0)'});
                                        } else {
                                            thisProcessSlider.css({'-webkit-transform':'translateX(-'+processSliderTitleArea.outerWidth()+'px)'});
                                            thisProcessSlider.css({'transform':'translateX(-'+processSliderTitleArea.outerWidth()+'px)'});
                                        }
                                        processSliderHandler.css({marginLeft:111}); //match the dragged margin offset
                                        processSliderItems.addClass('eltd-moved');
                                    },500);
                                }
                            }, 50);
                        });
            });
        }
    }

    /*
    * Scroll logic for process Slider
    */
    function eltdProcessScroll() {

            var scroller;
            var gridCalcs;

            function calculategridCalcs () {
                gridCalcs = (Math.max($('.eltd-process-slider').width() - eltd.gridWidth, 0) / 2);
                return gridCalcs;
            }

            function Scroller (opts) {
                var handle = opts.handle;
                var track = opts.track;
                var scroller = opts.scroller;
                var content = opts.content;
                var dragEndedCallback = opts.dragEndedCallback || function () {};
                var dragStartedCallback = opts.dragStartedCallback || function () {};   

                var contentMaxScroll = (content.width() + gridCalcs*0.3) - scroller.width(); //0.3 - coeff that results in the appropriate scroll amount
                var maxTravel = track.width() - handle.width();
                var window_mouseMove = null;
                var dragInProgress = false;


                handle.click(function (e) {e.preventDefault(); e.stopPropagation();});

                function mouseFollower (cb) {
                    return function (e) {
                        cb(e.clientX || e.originalEvent.targetTouches[0].clientX || 0,
                             e.clientY || e.originalEvent.targetTouches[0].clientY || 0);
                    };
                }

                function scrollContentToRatio (ratio, animated, callback) {
                    var newScroll = ratio * contentMaxScroll;
                    if (!animated) {
                        scroller.scrollLeft(newScroll);
                        return;
                    } else {
                        scroller.animate({scrollLeft: newScroll}, 400, 'swing', callback);
                    }
                }

                function moveHandleToRatio (ratio, animated, callback) {
                    var props = {marginLeft: ratio * maxTravel};
                    if (!animated) {
                        handle.css(props);
                        return;
                    } else {
                        handle.animate(props, 400, 'swing', callback);
                    }
                }

                function scrollToRatio (ratio, opts) {
                    opts = $.extend({
                        animate: false,
                        scrollbarCallback: function () {},
                        contentCallback: function () {}
                        }, opts);
                    ratio = Math.max(Math.min(ratio, 1.0), 0.0);
                    moveHandleToRatio(ratio, opts.animate, opts.scrollbarCallback);
                    scrollContentToRatio(ratio, opts.animate, opts.contentCallback);
                }

                function scrollByDelta (delta, opts) {
                    return scrollToRatio((scroller.scrollLeft() - delta) / contentMaxScroll, opts);
                }

                function getScrollbarRatio () {
                    return handle.offset().left / maxTravel;
                }

                function getContentRatio () {
                    return scroller.scrollLeft() / contentMaxScroll;
                }

                var endDrag;
                function beginDrag (e) {
                    endDrag();
                    e.preventDefault();
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    dragInProgress = true;

                    var pageX = e.pageX || e.originalEvent.targetTouches[0].clientX;

                    var xOnHandle = pageX - handle.offset().left + track.offset().left;
                    function scrollHandleCallback (x, y) {
                        var position = x - xOnHandle;

                        if (position < 0) {
                            position = 0;
                        } else if (position > maxTravel) {
                            position = maxTravel;
                        }

                        var scrollRatio = position / maxTravel;
                        scrollToRatio(scrollRatio);
                    }
                    window_mouseMove = mouseFollower(scrollHandleCallback);
                    $('body').bind('mousemove', window_mouseMove);
                    dragStartedCallback();
                    handle.addClass('start');
                    return false;
                }
                
                endDrag = function (e) {
                    dragInProgress = false;
                    $('body').unbind('mousemove', window_mouseMove);
                    dragEndedCallback();
                    handle.removeClass('start');
                    return false;
                };

                handle.get(0).onselectstart = function () {return false;};
                // handle.mousedown(beginDrag);
                handle.bind('mousedown', beginDrag);
                $('html,body').bind('mouseup', endDrag);

                //$(window).bind('mouseleave', endDrag);

                return {
                    scrollToRatio: scrollToRatio,
                    scrollByDelta: scrollByDelta,
                    getContentRatio: getContentRatio,
                    getScrollbarRatio: getScrollbarRatio,
                    moveToSelector: function (selector, opts) {
                        if (!selector) return false;
                        var dest = $(selector, content);
                        var destOffset = (dest.offset().left + scroller.scrollLeft()) - (gridCalcs + 10);
                        var destRatio =  destOffset / contentMaxScroll;
                        scrollToRatio(destRatio, opts);
                    },
                    swipeToSide: function (amount, opts) {
                        var position = parseInt(handle.css('marginLeft')) + amount;

                        if (position < 0) {
                            position = 0;
                        } else if (position > maxTravel) {
                            position = maxTravel;
                        }

                        var scrollRatio = position / maxTravel;
                        scrollToRatio(scrollRatio, opts);
                    },
                    dragInProgress: function () {
                        return dragInProgress;
                    },
                    destroy: function () {
                        handle.unbind('mousedown', beginDrag);
                        $(window).unbind('mouseup', endDrag).unbind('mousemove', window_mouseMove);
                    }
                };
            }

            /*
            * Process Slider Scroll init
            */
            function processSliderScroll () {
                if ($(window).width() >= 768) { 
                    $('.eltd-process-slider').each(function (idx, el) {
                        calculategridCalcs();
                        $('.eltd-process-track').css({width:(Math.min($(window).width(), eltd.gridWidth))});
                        //recalcs
                        $(window).resize(function(){
                            $('.eltd-process-track').css({width:(Math.min($(window).width(), eltd.gridWidth))});
                        });
                        scroller = Scroller({
                            track: $('.eltd-process-track'),
                            handle: $('.eltd-process-handle'),
                            scroller: $('.eltd-process-outer'),
                            content: $('.eltd-process-slider')
                        });


                        $('.eltd-process-slider').swipe( {
                            swipeLeft: function(){
                                scroller.swipeToSide(300, {animate: true});
                            },
                            swipeRight: function(){
                                scroller.swipeToSide(-300, {animate: true});
                            },
                            threshold:20
                        });
                    });
                }

            }

            processSliderScroll();

    }
})(jQuery);