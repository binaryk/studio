(function($) {
    "use strict";


    var blog = {};
    eltd.modules.blog = blog;

    blog.eltdInitAudioPlayer = eltdInitAudioPlayer;

    blog.eltdOnDocumentReady = eltdOnDocumentReady;
    blog.eltdOnWindowLoad = eltdOnWindowLoad;
    blog.eltdOnWindowResize = eltdOnWindowResize;
    blog.eltdOnWindowScroll = eltdOnWindowScroll;

    $(document).ready(eltdOnDocumentReady);
    $(window).load(eltdOnWindowLoad);
    $(window).resize(eltdOnWindowResize);
    $(window).scroll(eltdOnWindowScroll);
    
    /* 
        All functions to be called on $(document).ready() should be in this function
    */
    function eltdOnDocumentReady() {
        eltdInitAudioPlayer();
        eltdInitBlogSlider();
        eltdInitBlogSingleArrows();
    }

    /* 
        All functions to be called on $(window).load() should be in this function
    */
    function eltdOnWindowLoad() {

    }

    /* 
        All functions to be called on $(window).resize() should be in this function
    */
    function eltdOnWindowResize() {

    }

    /* 
        All functions to be called on $(window).scroll() should be in this function
    */
    function eltdOnWindowScroll() {

    }



    function eltdInitAudioPlayer() {

        var players = $('audio.eltd-blog-audio');

        players.mediaelementplayer({
            audioWidth: '100%'
        });
    }


    /**
     * Init Blog Slider shortcode
     */
    function eltdInitBlogSlider() {

        var sliderHolders = $('.eltd-blog-slider-holder'),
            slider;

        if (sliderHolders.length) {
            sliderHolders.each(function(){
                slider = $(this).children('.eltd-blog-slider');

                slider.owlCarousel({
                    autoPlay: 3000,
                    singleItem: true,
                    pagination: false,
                    navigation: true,
                    slideSpeed: 600,
                    mouseDrag: false,
                    transitionStyle: 'backSlide',
                    navigationText: [
                        '<span class="eltd-prev-icon"><i class="icon-arrows-slim-left"></i></span>',
                        '<span class="eltd-next-icon"><i class="icon-arrows-slim-right"></i></span>'
                    ]
                });

            });
        }

    }

    /**
     * Initializes blog navigation arrows
     */
    function eltdInitBlogSingleArrows() {
        if($('.eltd-blog-single-navigation').length) {

            var button = $('.eltd-blog-single-prev a, .eltd-blog-single-next a');

            button.addClass('eltd-btn-visible');

            button.each(function() {
                var button = $(this);
                var buttonArrowTextWidth = parseInt(button.find('.eltd-btn-text').width());
                var buttonArrowIconWidth = parseInt(button.find('.eltd-btn-icon-arrow').width());
                var buttonWidth;

                buttonWidth = buttonArrowIconWidth + buttonArrowTextWidth + 15; //15 empty space between arrow and text
                var textPadding = buttonArrowIconWidth;

                button.css('width', buttonWidth + 'px');
                if(button.parent().hasClass('eltd-blog-single-next')) {
                    button.find('.eltd-btn-text').css({'padding-left': textPadding + 15 + 'px'}); //15 empty space between arrow and text
                } else {
                    button.find('.eltd-btn-text').css({'padding-right': textPadding + 15 + 'px'}); //15 empty space between arrow and text
                }

                button.mouseenter(function() {
                    if(button.parent().hasClass('eltd-blog-single-next')) {
                        button.find('.eltd-btn-text').css({'-webkit-transform': 'translateX(' + buttonArrowTextWidth + 'px)'});
                        button.find('.eltd-btn-text').css({'transform': 'translateX(' + buttonArrowTextWidth + 'px)'});
                    } else {
                        button.find('.eltd-btn-text').css({'-webkit-transform': 'translateX(' + -buttonArrowTextWidth + 'px)'});
                        button.find('.eltd-btn-text').css({'transform': 'translateX(' + -buttonArrowTextWidth + 'px)'});
                    }
                });
                button.mouseleave(function() {
                    button.find('.eltd-btn-text').css({'-webkit-transform': 'translateX(0px)'});
                    button.find('.eltd-btn-text').css({'transform': 'translateX(0px)'});
                });
            });

        }
    }

})(jQuery);