<?php

$eltd_blog_categories = array();
$categories = get_categories();
foreach($categories as $category) {
    $eltd_blog_categories[$category->term_id] = $category->name;
}

$blog_meta_box = kreate_elated_add_meta_box(
    array(
        'scope' => array('page'),
        'title' => 'Blog',
        'name' => 'blog_meta'
    )
);

    kreate_elated_add_meta_box_field(
        array(
            'name'        => 'eltd_blog_category_meta',
            'type'        => 'selectblank',
            'label'       => 'Blog Category',
            'description' => 'Choose category of posts to display (leave empty to display all categories)',
            'parent'      => $blog_meta_box,
            'options'     => $eltd_blog_categories
        )
    );

    kreate_elated_add_meta_box_field(
        array(
            'name'        => 'eltd_show_posts_per_page_meta',
            'type'        => 'text',
            'label'       => 'Number of Posts',
            'description' => 'Enter the number of posts to display',
            'parent'      => $blog_meta_box,
            'options'     => $eltd_blog_categories,
            'args'        => array("col_width" => 3)
        )
    );

    kreate_elated_add_meta_box_field(
		array(
			'type' => 'select',
			'name' => 'eltd_number_of_cols_meta',
			'default_value' => '',
			'label' => 'Number of Columns',
			'parent' => $blog_meta_box,
			'description' => 'Enter a number of columns in blog',
			'options'     => array(
				''		=> 'Default',
				'1' 	=> 'One',
				'2' 	=> 'Two',
				'3' 	=> 'Three',
				'4' 	=> 'Four',
			),
		)
    );

	kreate_elated_add_meta_box_field(
		array(
			'type' => 'select',
			'name' => 'eltd_blog_hide_image_meta',
			'default_value' => '',
			'label' => 'Hide Image',
			'parent' => $blog_meta_box,
			'description' => 'Enable this option to hide featured image in blog list',
			'options' => array(
				'' => 'Default',
				'yes' => 'Yes',
				'no' => 'No'
			)
		)
	);

