<?php

$general_meta_box = kreate_elated_add_meta_box(
    array(
        'scope' => array('page', 'portfolio-item', 'post'),
        'title' => 'General',
        'name' => 'general_meta'
    )
);


    kreate_elated_add_meta_box_field(
        array(
            'name' => 'eltd_page_background_color_meta',
            'type' => 'color',
            'default_value' => '',
            'label' => 'Page Background Color',
            'description' => 'Choose background color for page content',
            'parent' => $general_meta_box
        )
    );

    kreate_elated_add_meta_box_field(
        array(
            'name' => 'eltd_content_top_padding_meta',
            'type' => 'text',
            'default_value' => '',
            'label' => 'Content Top Padding',
            'description' => 'Set content top padding',
            'parent' => $general_meta_box,
            'args' => array(
                'col_width' => 2,
                'suffix' => 'px'
            )
        )
    );

    if(kreate_elated_options()->getOptionValue('boxed') == 'no') {
        kreate_elated_add_meta_box_field(
            array(
                'name' => 'eltd_fixed_background_image_meta',
                'type' => 'image',
                'label' => 'Fixed Background Image',
                'description' => 'Choose an image that will be displayed in the page background as fixed',
                'parent' => $general_meta_box
            )
        );
    }

    kreate_elated_add_meta_box_field(
        array(
            'name' => 'eltd_page_slider_meta',
            'type' => 'text',
            'default_value' => '',
            'label' => 'Slider Shortcode',
            'description' => 'Paste your slider shortcode here',
            'parent' => $general_meta_box
        )
    );

    kreate_elated_add_meta_box_field(
        array(
            'name'        => 'eltd_page_comments_meta',
            'type'        => 'selectblank',
            'label'       => 'Show Comments',
            'description' => 'Enabling this option will show comments on your page',
            'parent'      => $general_meta_box,
            'options'     => array(
                'yes' => 'Yes',
                'no' => 'No',
            )
        )
    );