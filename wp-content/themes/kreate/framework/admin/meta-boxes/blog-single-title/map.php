<?php

$post_single = false;

$blog_single_title_meta_box = kreate_elated_add_meta_box(
    array(
        'scope' => array('post'),
        'title' => 'Title',
        'name' => 'blog_single_title_meta'
    )
);

    kreate_elated_add_meta_box_field(
        array(
            'name' => 'eltd_show_title_area_meta',
            'type' => 'select',
            'default_value' => '',
            'label' => 'Show Title Area',
            'description' => 'Disabling this option will turn off page title area',
            'parent' => $blog_single_title_meta_box,
            'options' => array(
                '' => '',
                'no' => 'No',
                'yes' => 'Yes'
            ),
            'args' => array(
                "dependence" => true,
                "hide" => array(
                    "" => "",
                    "no" => "#eltd_eltd_show_title_area_meta_container",
                    "yes" => ""
                ),
                "show" => array(
                    "" => "#eltd_eltd_show_title_area_meta_container",
                    "no" => "",
                    "yes" => "#eltd_eltd_show_title_area_meta_container"
                )
            )
        )
    );

    $show_title_area_meta_container = kreate_elated_add_admin_container(
        array(
            'parent' => $blog_single_title_meta_box,
            'name' => 'eltd_show_title_area_meta_container',
            'hidden_property' => 'eltd_show_title_area_meta',
            'hidden_value' => 'no'
        )
    );

        kreate_elated_add_meta_box_field(
            array(
                'name' => 'eltd_title_area_animation_meta',
                'type' => 'select',
                'default_value' => '',
                'label' => 'Animations',
                'description' => 'Choose an animation for Title Area',
                'parent' => $show_title_area_meta_container,
                'options' => array(
                    '' => '',
                    'no' => 'No Animation',
                    'right-left' => 'Text right to left',
                    'left-right' => 'Text left to right'
                )
            )
        );

        kreate_elated_add_meta_box_field(
            array(
                'name' => 'eltd_title_area_vertial_alignment_meta',
                'type' => 'select',
                'default_value' => '',
                'label' => 'Vertical Alignment',
                'description' => 'Specify title vertical alignment',
                'parent' => $show_title_area_meta_container,
                'options' => array(
                    '' => '',
                    'header_bottom' => 'From Bottom of Header',
                    'window_top' => 'From Window Top'
                )
            )
        );

        kreate_elated_add_meta_box_field(
            array(
                'name' => 'eltd_title_area_content_alignment_meta',
                'type' => 'select',
                'default_value' => '',
                'label' => 'Content Alignment',
                'description' => 'Specify title content alignment',
                'parent' => $show_title_area_meta_container,
                'options' => array(
                    '' => '',
                    'left' => 'Left',
                    'center' => 'Center',
                    'right' => 'Right'
                )
            )
        );

        kreate_elated_add_meta_box_field(
            array(
                'name' => 'eltd_title_text_color_meta',
                'type' => 'color',
                'label' => 'Title Color',
                'description' => 'Choose a color for title text',
                'parent' => $show_title_area_meta_container
            )
        );


        kreate_elated_add_meta_box_field(
            array(
                'name' => 'eltd_title_area_background_color_meta',
                'type' => 'color',
                'label' => 'Background Color',
                'description' => 'Choose a background color for Title Area',
                'parent' => $show_title_area_meta_container
            )
        );

        kreate_elated_add_meta_box_field(
            array(
                'name' => 'eltd_hide_background_image_meta',
                'type' => 'yesno',
                'default_value' => 'no',
                'label' => 'Hide Background Image',
                'description' => 'Enable this option to hide background image in Title Area',
                'parent' => $show_title_area_meta_container,
                'args' => array(
                    "dependence" => true,
                    "dependence_hide_on_yes" => "#eltd_eltd_hide_background_image_meta_container",
                    "dependence_show_on_yes" => ""
                )
            )
        );

        $hide_background_image_meta_container = kreate_elated_add_admin_container(
            array(
                'parent' => $show_title_area_meta_container,
                'name' => 'eltd_hide_background_image_meta_container',
                'hidden_property' => 'eltd_hide_background_image_meta',
                'hidden_value' => 'yes'
            )
        );

        kreate_elated_add_meta_box_field(
            array(
                'name' => 'eltd_title_area_background_image_responsive_meta',
                'type' => 'select',
                'default_value' => '',
                'label' => 'Background Responsive Image',
                'description' => 'Enabling this option will make Title background image responsive',
                'parent' => $hide_background_image_meta_container,
                'options' => array(
                    '' => '',
                    'no' => 'No',
                    'yes' => 'Yes'
                ),
                'args' => array(
                    "dependence" => true,
                    "hide" => array(
                        "" => "",
                        "no" => "",
                        "yes" => "#eltd_eltd_title_area_background_image_responsive_meta_container, #eltd_eltd_title_area_height_meta"
                    ),
                    "show" => array(
                        "" => "#eltd_eltd_title_area_background_image_responsive_meta_container, #eltd_eltd_title_area_height_meta",
                        "no" => "#eltd_eltd_title_area_background_image_responsive_meta_container, #eltd_eltd_title_area_height_meta",
                        "yes" => ""
                    )
                )
            )
        );

        $title_area_background_image_responsive_meta_container = kreate_elated_add_admin_container(
            array(
                'parent' => $hide_background_image_meta_container,
                'name' => 'eltd_title_area_background_image_responsive_meta_container',
                'hidden_property' => 'eltd_title_area_background_image_responsive_meta',
                'hidden_value' => 'yes'
            )
        );

            kreate_elated_add_meta_box_field(
                array(
                    'name' => 'eltd_title_area_background_image_parallax_meta',
                    'type' => 'select',
                    'default_value' => '',
                    'label' => 'Background Image in Parallax',
                    'description' => 'Enabling this option will make Title background image parallax',
                    'parent' => $title_area_background_image_responsive_meta_container,
                    'options' => array(
                        '' => '',
                        'no' => 'No',
                        'yes' => 'Yes',
                        'yes_zoom' => 'Yes, with zoom out'
                    )
                )
            );

        kreate_elated_add_meta_box_field(array(
            'name' => 'eltd_title_area_height_meta',
            'type' => 'text',
            'label' => 'Height',
            'description' => 'Set a height for Title Area',
            'parent' => $show_title_area_meta_container,
            'args' => array(
                'col_width' => 2,
                'suffix' => 'px'
            )
        ));