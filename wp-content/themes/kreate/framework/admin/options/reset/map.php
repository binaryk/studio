<?php

if ( ! function_exists('kreate_elated_reset_options_map') ) {
	/**
	 * Reset options panel
	 */
	function kreate_elated_reset_options_map() {

		kreate_elated_add_admin_page(
			array(
				'slug'  => '_reset_page',
				'title' => 'Reset',
				'icon'  => 'fa fa-retweet'
			)
		);

		$panel_reset = kreate_elated_add_admin_panel(
			array(
				'page'  => '_reset_page',
				'name'  => 'panel_reset',
				'title' => 'Reset'
			)
		);

		kreate_elated_add_admin_field(array(
			'type'	=> 'yesno',
			'name'	=> 'reset_to_defaults',
			'default_value'	=> 'no',
			'label'			=> 'Reset to Defaults',
			'description'	=> 'This option will reset all Elated Options values to defaults',
			'parent'		=> $panel_reset
		));

	}

	add_action( 'kreate_elated_options_map', 'kreate_elated_reset_options_map', 100 );

}