<?php

if ( ! function_exists('kreate_elated_load_elements_map') ) {
	/**
	 * Add Elements option page for shortcodes
	 */
	function kreate_elated_load_elements_map() {

		kreate_elated_add_admin_page(
			array(
				'slug' => '_elements_page',
				'title' => 'Elements',
				'icon' => 'fa fa-star'
			)
		);

		do_action( 'kreate_elated_options_elements_map' );

	}

	add_action('kreate_elated_options_map', 'kreate_elated_load_elements_map',12);

}