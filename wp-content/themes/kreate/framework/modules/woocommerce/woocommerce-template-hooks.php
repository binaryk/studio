<?php

if (!function_exists('kreate_elated_woocommerce_products_per_page')) {
	/**
	 * Function that sets number of products per page. Default is 12
	 * @return int number of products to be shown per page
	 */
	function kreate_elated_woocommerce_products_per_page() {

		$products_per_page = 12;

		if (kreate_elated_options()->getOptionValue('eltd_woo_products_per_page')) {
			$products_per_page = kreate_elated_options()->getOptionValue('eltd_woo_products_per_page');
		}

		return $products_per_page;

	}

}

if (!function_exists('kreate_elated_woocommerce_related_products_args')) {
	/**
	 * Function that sets number of displayed related products. Hooks to woocommerce_output_related_products_args filter
	 * @param $args array array of args for the query
	 * @return mixed array of changed args
	 */
	function kreate_elated_woocommerce_related_products_args($args) {

		if (kreate_elated_options()->getOptionValue('eltd_woo_product_list_columns')) {

			$related = kreate_elated_options()->getOptionValue('eltd_woo_product_list_columns');
			switch ($related) {
				case 'eltd-woocommerce-columns-4':
					$args['posts_per_page'] = 4;
					break;
				case 'eltd-woocommerce-columns-3':
					$args['posts_per_page'] = 3;
					break;
				default:
					$args['posts_per_page'] = 3;
			}

		} else {
			$args['posts_per_page'] = 3;
		}

		return $args;

	}

}

if (!function_exists('kreate_elated_woocommerce_template_loop_product_title')) {
	/**
	 * Function for overriding product title template in Product List Loop
	 */
	function kreate_elated_woocommerce_template_loop_product_title() {

		$tag = kreate_elated_options()->getOptionValue('eltd_products_list_title_tag');
		the_title('<' . $tag . ' class="eltd-product-list-product-title">', '</' . $tag . '>');

	}

}

if (!function_exists('kreate_elated_woocommerce_template_single_title')) {
	/**
	 * Function for overriding product title template in Single Product template
	 */
	function kreate_elated_woocommerce_template_single_title() {

		$tag = kreate_elated_options()->getOptionValue('eltd_single_product_title_tag');
		the_title('<' . $tag . '  itemprop="name" class="eltd-single-product-title">', '</' . $tag . '>');

		echo kreate_elated_execute_shortcode('eltd_separator',array(
			'type' => 'full-width',
			'border_style' => 'dotted_multiple',
			'color' => 'black',
			'thickness' => '3',
			'top_margin' => '15',
			'bottom_margin' => '30'
		));

	}

}

if (!function_exists('kreate_elated_woocommerce_sale_flash')) {
	/**
	 * Function for overriding Sale Flash Template
	 *
	 * @return string
	 */
	function kreate_elated_woocommerce_sale_flash() {
		global $product;
		$html = '';

		if ($product->is_on_sale()){
			$html = '<span class="eltd-onsale"><span class="eltd-onsale-inner">' . esc_html__('Sale!', 'kreate') . '</span></span>';
		}
		elseif (!$product->is_in_stock()){
			$html = '<span class="eltd-out-of-stock"><span class="eltd-out-of-stock-inner">' . esc_html__('Out of stock', 'kreate') . '</span></span>';
		}

		return $html;

	}

}

if (!function_exists('kreate_elated_custom_override_checkout_fields')) {
	/**
	 * Overrides placeholder values for checkout fields
	 * @param array all checkout fields
	 * @return array checkout fields with overriden values
	 */
	function kreate_elated_custom_override_checkout_fields($fields) {
		//billing fields
		$args_billing = array(
			'first_name'	=> esc_html__('First name','kreate'),
			'last_name'		=> esc_html__('Last name','kreate'),
			'company'		=> esc_html__('Company name','kreate'),
			'address_1'		=> esc_html__('Address','kreate'),
			'email'			=> esc_html__('Email','kreate'),
			'phone'			=> esc_html__('Phone','kreate'),
			'postcode'		=> esc_html__('Postcode / ZIP','kreate'),
			'state'			=> esc_html__('State / County', 'kreate')
		);

		//shipping fields
		$args_shipping = array(
			'first_name' => esc_html__('First name','kreate'),
			'last_name'  => esc_html__('Last name','kreate'),
			'company'    => esc_html__('Company name','kreate'),
			'address_1'  => esc_html__('Address','kreate'),
			'postcode'   => esc_html__('Postcode / ZIP','kreate')
		);

		//override billing placeholder values
		foreach ($args_billing as $key => $value) {
			$fields["billing"]["billing_{$key}"]["placeholder"] = $value;
		}

		//override shipping placeholder values
		foreach ($args_shipping as $key => $value) {
			$fields["shipping"]["shipping_{$key}"]["placeholder"] = $value;
		}

		return $fields;
	}

}

if (!function_exists('kreate_elated_woocommerce_loop_add_to_cart_link')) {
	/**
	 * Function that overrides default woocommerce add to cart button on product list
	 * Uses HTML from eltd_button
	 *
	 * @return mixed|string
	 */
	function kreate_elated_woocommerce_loop_add_to_cart_link() {

		global $product;

		$button_url = $product->add_to_cart_url();
		$button_classes = sprintf('%s product_type_%s',
			$product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
			esc_attr( $product->product_type )
		);
		$button_classes .= ' eltd-loop-add-to-cart';
		$button_text = $product->add_to_cart_text();
		$button_attrs = array(
			'href' => $button_url,
			'target' => '_blank',
			'class' => $button_classes,
			'rel' => 'nofollow',
			'data-product_id' => esc_attr( $product->id ),
			'data-product_sku' => esc_attr( $product->get_sku() ),
			'data-quantity' => esc_attr( isset( $quantity ) ? $quantity : 1 )
		);

		$button_icon = 'icon-ecommerce-bag-plus';
		if (!$product->is_in_stock()){
			$button_icon = 'icon-ecommerce-bag-search';
		}

		$button_html = '<a '.kreate_elated_get_inline_attrs($button_attrs).'>';

		$button_html .= '<span class="eltd-add-to-cart '.$button_icon.'"></span>';

		$button_html .= '</a>';

		return $button_html;

	}

}

if (!function_exists('kreate_elated_get_woocommerce_add_to_cart_button')) {
	/**
	 * Function that overrides default woocommerce add to cart button on simple and grouped product single template
	 * Uses HTML from eltd_button
	 */
	function kreate_elated_get_woocommerce_add_to_cart_button() {

		global $product;

		$add_to_cart_button = kreate_elated_get_button_html(
			array(
				'custom_class'	=> 'single_add_to_cart_button alt',
				'text'			=> $product->single_add_to_cart_text(),
				'html_type'		=> 'button'
			)
		);

		print $add_to_cart_button;

	}

}

if (!function_exists('kreate_elated_get_woocommerce_add_to_cart_button_external')) {
	/**
	 * Function that overrides default woocommerce add to cart button on external product single template
	 * Uses HTML from eltd_button
	 */
	function kreate_elated_get_woocommerce_add_to_cart_button_external() {

		global $product;

		$add_to_cart_button = kreate_elated_get_button_html(
			array(
				'link'			=> $product->add_to_cart_url(),
				'custom_class'	=> 'single_add_to_cart_button alt',
				'text'			=> $product->single_add_to_cart_text(),
				'custom_attrs'	=> array(
					'rel' 		=> 'nofollow'
				)
			)
		);

		print $add_to_cart_button;

	}

}

if ( ! function_exists('kreate_elated_woocommerce_single_variation_add_to_cart_button') ) {
	/**
	 * Function that overrides default woocommerce add to cart button on variable product single template
	 * Uses HTML from eltd_button
	 */
	function kreate_elated_woocommerce_single_variation_add_to_cart_button() {
		global $product;

		$html = '<div class="variations_button">';
		woocommerce_quantity_input( array( 'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : 1 ) );

		$button = kreate_elated_get_button_html(array(
			'html_type'		=> 'button',
			'custom_class'	=> 'single_add_to_cart_button alt',
			'text'			=> $product->single_add_to_cart_text()
		));

		$html .= $button;

		$html .= '<input type="hidden" name="add-to-cart" value="' . absint( $product->id ) . '" />';
		$html .= '<input type="hidden" name="product_id" value="' . absint( $product->id ) . '" />';
		$html .= '<input type="hidden" name="variation_id" class="variation_id" value="" />';
		$html .= '</div>';

		print $html;

	}

}

if (!function_exists('kreate_elated_get_woocommerce_apply_coupon_button')) {
	/**
	 * Function that overrides default woocommerce apply coupon button
	 * Uses HTML from eltd_button
	 */
	function kreate_elated_get_woocommerce_apply_coupon_button() {

		$coupon_button = kreate_elated_get_button_html(array(
			'html_type'		=> 'input',
			'input_name'	=> 'apply_coupon',
			'text'			=> esc_html__( 'Apply Coupon', 'kreate' )
		));

		print $coupon_button;

	}

}

if (!function_exists('kreate_elated_get_woocommerce_update_cart_button')) {
	/**
	 * Function that overrides default woocommerce update cart button
	 * Uses HTML from eltd_button
	 */
	function kreate_elated_get_woocommerce_update_cart_button() {

		$update_cart_button = kreate_elated_get_button_html(array(
			'html_type'		=> 'input',
			'input_name'	=> 'update_cart',
			'text'			=> esc_html__( 'Update Cart', 'kreate' )
		));

		print $update_cart_button;

	}

}

if (!function_exists('kreate_elated_woocommerce_button_proceed_to_checkout')) {
	/**
	 * Function that overrides default woocommerce proceed to checkout button
	 * Uses HTML from eltd_button
	 */
	function kreate_elated_woocommerce_button_proceed_to_checkout() {

		$proceed_to_checkout_button = kreate_elated_get_button_html(array(
			'link'			=> WC()->cart->get_checkout_url(),
			'custom_class'	=> 'checkout-button alt wc-forward',
			'text'			=> esc_html__( 'Proceed to Checkout', 'kreate' )
		));

		print $proceed_to_checkout_button;

	}

}

if (!function_exists('kreate_elated_get_woocommerce_update_totals_button')) {
	/**
	 * Function that overrides default woocommerce update totals button
	 * Uses HTML from eltd_button
	 */
	function kreate_elated_get_woocommerce_update_totals_button() {

		$update_totals_button = kreate_elated_get_button_html(array(
			'html_type'		=> 'button',
			'text'			=> esc_html__( 'Update Totals', 'kreate' ),
			'custom_attrs'	=> array(
				'value'		=> 1,
				'name'		=> 'calc_shipping'
			)
		));

		print $update_totals_button;

	}

}

if (!function_exists('kreate_elated_woocommerce_pay_order_button_html')) {
	/**
	 * Function that overrides default woocommerce pay order button on checkout page
	 * Uses HTML from eltd_button
	 */
	function kreate_elated_woocommerce_pay_order_button_html() {

		$pay_order_button_text = esc_html__('Pay for order', 'kreate');

		$place_order_button = kreate_elated_get_button_html(array(
			'html_type'		=> 'input',
			'custom_class'	=> 'alt',
			'custom_attrs'	=> array(
				'id'			=> 'place_order',
				'data-value'	=> $pay_order_button_text
			),
			'text'			=> $pay_order_button_text,
		));

		return $place_order_button;

	}

}

if (!function_exists('kreate_elated_woocommerce_order_button_html')) {
	/**
	 * Function that overrides default woocommerce place order button on checkout page
	 * Uses HTML from eltd_button
	 */
	function kreate_elated_woocommerce_order_button_html() {

		$pay_order_button_text = esc_html__('Place Order', 'kreate');

		$place_order_button = kreate_elated_get_button_html(array(
			'html_type'		=> 'input',
			'custom_class'	=> 'alt',
			'custom_attrs'	=> array(
				'id'			=> 'place_order',
				'data-value'	=> $pay_order_button_text,
				'name'			=> 'woocommerce_checkout_place_order'
			),
			'text'			=> $pay_order_button_text,
		));

		return $place_order_button;

	}

}
 
if (!function_exists('kreate_elated_woocommerce_desc_addi_heading')){
	/**
	 * Function that overrides default woocommerce description and additional information heading
	 */
	function kreate_elated_woocommerce_desc_addi_heading() {
		return '';
	}
}

if (!function_exists('kreate_elated_loop_columns')) {
	/**
	 * Function sets number of products showing on shop page
	 */	
	function kreate_elated_loop_columns() {

		$sidebar_layout = '';
		$column_number = kreate_elated_options()->getOptionValue('eltd_woo_product_list_columns');
		$shop_id = get_option( 'woocommerce_shop_page_id' );

		$page_sidebar_meta = get_post_meta($shop_id, 'eltd_sidebar_meta', true);

		if(($page_sidebar_meta !== '') && $shop_id !== -1) {
			if($page_sidebar_meta == 'no-sidebar') {
				$sidebar_layout = '';
			} else {
				$sidebar_layout = $page_sidebar_meta;
			}
		}

		if (in_array($sidebar_layout, array('sidebar-33-right','sidebar-25-right','sidebar-33-left','sidebar-25-left'))){
			if ($column_number == 'eltd-woocommerce-columns-3'){
				return 8;
			}
			else{
				return 9;
			}
		}
		else{
			if ($column_number == 'eltd-woocommerce-columns-3'){
				return 9;
			}
			else{
				return 12;
			}	
		}
	}
}