<?php
class KreateWoocommerceDropdownCart extends WP_Widget {
	public function __construct() {
		parent::__construct(
			'eltd_woocommerce_dropdown_cart', // Base ID
			'Elated Woocommerce Dropdown Cart', // Name
			array( 'description' => esc_html__( 'Elated Woocommerce Dropdown Cart', 'kreate' ), ) // Args
		);
	}

	public function widget( $args, $instance ) {
		global $post;
		extract( $args );
		
		global $woocommerce; 
		global $kreate_elated_options;
		
		$cart_style = 'eltd-with-icon';
		
		?>
		<div class="eltd-shopping-cart-outer">
			<div class="eltd-shopping-cart-inner">
				<div class="eltd-shopping-cart-header">
					<a class="eltd-header-cart" href="<?php echo esc_url($woocommerce->cart->get_cart_url()); ?>">
						<i class="icon-ecommerce-bag"></i>
						<span class="eltd-cart-no"><?php echo esc_html($woocommerce->cart->cart_contents_count); ?></span>
					</a>
					<div class="eltd-shopping-cart-dropdown">
						<?php
						$cart_is_empty = sizeof( $woocommerce->cart->get_cart() ) <= 0;
						$list_class = array( 'eltd-cart-list', 'product_list_widget' );
						?>
						<ul>

							<?php if ( !$cart_is_empty ) : ?>

								<?php foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $cart_item ) :

									$_product = $cart_item['data'];

									// Only display if allowed
									if ( ! $_product->exists() || $cart_item['quantity'] == 0 ) {
										continue;
									}

									// Get price
									$product_price = get_option( 'woocommerce_tax_display_cart' ) == 'excl' ? $_product->get_price_excluding_tax() : $_product->get_price_including_tax();
									?>


									<li>
										<div class="eltd-item-image-holder">
											<a href="<?php echo esc_url(get_permalink( $cart_item['product_id'] )); ?>">
												<?php echo wp_kses($_product->get_image(), array(
													'img' => array(
														'src' => true,
														'width' => true,
														'height' => true,
														'class' => true,
														'alt' => true,
														'title' => true,
														'id' => true
													)
												)); ?>
											</a>
										</div>
										<div class="eltd-item-info-holder">
											<div class="eltd-item-left">
												<a href="<?php echo esc_url(get_permalink( $cart_item['product_id'])); ?>">
													<?php echo apply_filters('woocommerce_widget_cart_product_title', $_product->get_title(), $_product ); ?>
												</a>
												<?php echo apply_filters( 'woocommerce_cart_item_price_html', wc_price( $product_price ), $cart_item, $cart_item_key ); ?>
											</div>
											<div class="eltd-item-right">
												<?php echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf('<a href="%s" class="remove" title="%s"><span class="icon-arrows-circle-remove"></span></a>', esc_url( $woocommerce->cart->get_remove_url( $cart_item_key ) ), esc_html__('Remove this item', 'kreate') ), $cart_item_key ); ?>

											</div>
										</div>
									</li>

								<?php endforeach; ?>
							</ul>
								<div class="eltd-cart-bottom">
									<div class="eltd-subtotal-holder clearfix">
										<span class="eltd-total"><?php esc_html_e( 'Subtotal', 'kreate' ); ?></span>
										<span class="eltd-total-amount">
											<?php echo wp_kses($woocommerce->cart->get_cart_subtotal(), array(
												'span' => array(
													'class' => true,
													'id' => true
												)
											)); ?>
										</span>
									</div>
									<div class="eltd-btns-holder clearfix">
										<?php echo kreate_elated_execute_shortcode('eltd_button',array(
											'type' => 'arrow',
											'hover_type' => 'hide_text',
											'text' => esc_html__('View Cart', 'kreate'),
											'link' => $woocommerce->cart->get_cart_url(),
											'target' => '_blank',
											'color' => '#fff',
											'custom_class' => 'eltd-cart-btn-bottom view-cart'
										)) ?>
										<?php echo kreate_elated_execute_shortcode('eltd_button',array(
											'type' => 'arrow',
											'hover_type' => 'hide_text',
											'text' => esc_html__('Checkout','kreate'),
											'link' => $woocommerce->cart->get_checkout_url(),
											'target' => '_blank',
											'color' => '#fff',
											'custom_class' => 'eltd-cart-btn-bottom checkout'
										)) ?>
									</div>
								</div>
							<?php else : ?>

								<li class="eltd-empty-cart">
									<span class="eltd-cart-icon-empty icon-basic-info"></span>
									<span><?php esc_html_e( 'Cart is empty', 'kreate' ); ?></span>
									<?php echo kreate_elated_execute_shortcode('eltd_button',array(
										'type' => 'arrow',
										'hover_type' => 'hide_text',
										'text' => esc_html__('Shop','kreate'),
										'link' => get_permalink(woocommerce_get_page_id('shop')),
										'target' => '_blank',
										'color' => '#fff',
										'custom_class' => 'eltd-cart-button'
									)) ?>
								</li>
							</ul>

							<?php endif; ?>
						<?php if ( sizeof( $woocommerce->cart->get_cart() ) <= 0 ) : ?>

						<?php endif; ?>
						

						<?php if ( sizeof( $woocommerce->cart->get_cart() ) <= 0 ) : ?>

						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		<?php
	}

}
add_action( 'widgets_init', create_function( '', 'register_widget( "KreateWoocommerceDropdownCart" );' ) );
?>
<?php
add_filter('add_to_cart_fragments', 'kreate_elated_woocommerce_header_add_to_cart_fragment');
function kreate_elated_woocommerce_header_add_to_cart_fragment( $fragments ) {
	global $woocommerce;

	ob_start();
	?>
	<div class="eltd-shopping-cart-header">
		<a class="eltd-header-cart" href="<?php echo esc_url($woocommerce->cart->get_cart_url()); ?>">
			<i class="icon-ecommerce-bag"></i>
			<span class="eltd-cart-no"><?php echo esc_html($woocommerce->cart->cart_contents_count); ?></span>
		</a>		
		<div class="eltd-shopping-cart-dropdown">
			<?php
			$cart_is_empty = sizeof( $woocommerce->cart->get_cart() ) <= 0;
			//$list_class = array( 'eltd-cart-list', 'product_list_widget' );
			?>
			<ul>

				<?php if ( !$cart_is_empty ) : ?>

					<?php foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $cart_item ) :

						$_product = $cart_item['data'];

						// Only display if allowed
						if ( ! $_product->exists() || $cart_item['quantity'] == 0 ) {
							continue;
						}

						// Get price
						$product_price = get_option( 'woocommerce_tax_display_cart' ) == 'excl' ? $_product->get_price_excluding_tax() : $_product->get_price_including_tax();
						?>

						<li>
							<div class="eltd-item-image-holder">
								<?php echo wp_kses($_product->get_image(), array(
									'img' => array(
										'src' => true,
										'width' => true,
										'height' => true,
										'class' => true,
										'alt' => true,
										'title' => true,
										'id' => true
									)
								)); ?>
							</div>
							<div class="eltd-item-info-holder">
								<div class="eltd-item-left">
									<a href="<?php echo esc_url(get_permalink( $cart_item['product_id'] )); ?>">
										<?php echo apply_filters('woocommerce_widget_cart_product_title', $_product->get_title(), $_product ); ?>
									</a>
									<?php echo apply_filters( 'woocommerce_cart_item_price_html', wc_price( $product_price ), $cart_item, $cart_item_key ); ?>
								</div>
								<div class="eltd-item-right">
									<?php echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf('<a href="%s" class="remove" title="%s"><span class="icon-arrows-circle-remove"></span></a>', esc_url( $woocommerce->cart->get_remove_url( $cart_item_key ) ), esc_html__('Remove this item', 'kreate') ), $cart_item_key ); ?>

								</div>
							</div>
						</li>

					<?php endforeach; ?>
				</ul>
						<div class="eltd-cart-bottom">
							<div class="eltd-subtotal-holder clearfix">
								<span class="eltd-total"><?php esc_html_e( 'Subtotal', 'kreate' ); ?>:</span>
								<span class="eltd-total-amount">
									<?php echo wp_kses($woocommerce->cart->get_cart_subtotal(), array(
										'span' => array(
											'class' => true,
											'id' => true
										)
									)); ?>
								</span>
							</div>
							<div class="eltd-btns-holder clearfix">
								<?php echo kreate_elated_execute_shortcode('eltd_button',array(
									'type' => 'arrow',
									'hover_type' => 'hide_text',
									'text' => esc_html__('View Cart', 'kreate'),
									'link' => $woocommerce->cart->get_cart_url(),
									'target' => '_blank',
									'color' => '#fff',
									'custom_class' => 'eltd-cart-btn-bottom view-cart'
								)) ?>
								<?php echo kreate_elated_execute_shortcode('eltd_button',array(
									'type' => 'arrow',
									'hover_type' => 'hide_text',
									'text' => esc_html__('Checkout','kreate'),
									'link' => $woocommerce->cart->get_checkout_url(),
									'target' => '_blank',
									'color' => '#fff',
									'custom_class' => 'eltd-cart-btn-bottom checkout'
								)) ?>
							</div>
						</div>
				<?php else : ?>

					<li class="eltd-empty-cart">
						<span class="eltd-cart-icon-empty icon-basic-info"></span>
						<span><?php esc_html_e( 'Cart is empty', 'kreate' ); ?></span>
						<?php echo kreate_elated_execute_shortcode('eltd_button',array(
							'type' => 'arrow',
							'hover_type' => 'hide_text',
							'text' => esc_html__('Shop','kreate'),
							'link' => get_permalink(woocommerce_get_page_id('shop')),
							'target' => '_blank',
							'color' => '#fff',
							'custom_class' => 'eltd-cart-button'
						)) ?>
					</li>
			</ul>

				<?php endif; ?>

			<?php if ( sizeof( $woocommerce->cart->get_cart() ) <= 0 ) : ?>

			<?php endif; ?>
			

			<?php if ( sizeof( $woocommerce->cart->get_cart() ) <= 0 ) : ?>

			<?php endif; ?>
		</div>
	</div>

	<?php
	$fragments['div.eltd-shopping-cart-header'] = ob_get_clean();
	return $fragments;
}
?>