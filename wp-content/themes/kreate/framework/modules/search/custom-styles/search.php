<?php

if (!function_exists('kreate_elated_search_opener_icon_size')) {

	function kreate_elated_search_opener_icon_size()
	{

		if (kreate_elated_options()->getOptionValue('header_search_icon_size')) {
			echo kreate_elated_dynamic_css('.eltd-search-opener', array(
				'font-size' => kreate_elated_filter_px(kreate_elated_options()->getOptionValue('header_search_icon_size')) . 'px'
			));
		}

	}

	add_action('kreate_elated_style_dynamic', 'kreate_elated_search_opener_icon_size');

}

if (!function_exists('kreate_elated_search_opener_icon_colors')) {

	function kreate_elated_search_opener_icon_colors()
	{

		if (kreate_elated_options()->getOptionValue('header_search_icon_color') !== '') {
			echo kreate_elated_dynamic_css('.eltd-search-opener', array(
				'color' => kreate_elated_options()->getOptionValue('header_search_icon_color')
			));
		}

		if (kreate_elated_options()->getOptionValue('header_search_icon_hover_color') !== '') {
			echo kreate_elated_dynamic_css('.eltd-search-opener:hover', array(
				'color' => kreate_elated_options()->getOptionValue('header_search_icon_hover_color')
			));
		}

		if (kreate_elated_options()->getOptionValue('header_light_search_icon_color') !== '') {
			echo kreate_elated_dynamic_css('.eltd-light-header .eltd-page-header > div:not(.eltd-sticky-header) .eltd-search-opener,
			.eltd-light-header.eltd-header-style-on-scroll .eltd-page-header .eltd-search-opener,
			.eltd-light-header .eltd-top-bar .eltd-search-opener', array(
				'color' => kreate_elated_options()->getOptionValue('header_light_search_icon_color') . ' !important'
			));
		}

		if (kreate_elated_options()->getOptionValue('header_light_search_icon_hover_color') !== '') {
			echo kreate_elated_dynamic_css('.eltd-light-header .eltd-page-header > div:not(.eltd-sticky-header) .eltd-search-opener:hover,
			.eltd-light-header.eltd-header-style-on-scroll .eltd-page-header .eltd-search-opener:hover,
			.eltd-light-header .eltd-top-bar .eltd-search-opener:hover', array(
				'color' => kreate_elated_options()->getOptionValue('header_light_search_icon_hover_color') . ' !important'
			));
		}

		if (kreate_elated_options()->getOptionValue('header_dark_search_icon_color') !== '') {
			echo kreate_elated_dynamic_css('.eltd-dark-header .eltd-page-header > div:not(.eltd-sticky-header) .eltd-search-opener,
			.eltd-dark-header.eltd-header-style-on-scroll .eltd-page-header .eltd-search-opener,
			.eltd-dark-header .eltd-top-bar .eltd-search-opener', array(
				'color' => kreate_elated_options()->getOptionValue('header_dark_search_icon_color') . ' !important'
			));
		}
		if (kreate_elated_options()->getOptionValue('header_dark_search_icon_hover_color') !== '') {
			echo kreate_elated_dynamic_css('.eltd-dark-header .eltd-page-header > div:not(.eltd-sticky-header) .eltd-search-opener:hover,
			.eltd-dark-header.eltd-header-style-on-scroll .eltd-page-header .eltd-search-opener:hover,
			.eltd-dark-header .eltd-top-bar .eltd-search-opener:hover', array(
				'color' => kreate_elated_options()->getOptionValue('header_dark_search_icon_hover_color') . ' !important'
			));
		}

	}

	add_action('kreate_elated_style_dynamic', 'kreate_elated_search_opener_icon_colors');

}

if (!function_exists('kreate_elated_search_opener_icon_background_colors')) {

	function kreate_elated_search_opener_icon_background_colors()
	{

		if (kreate_elated_options()->getOptionValue('search_icon_background_color') !== '') {
			echo kreate_elated_dynamic_css('.eltd-search-opener', array(
				'background-color' => kreate_elated_options()->getOptionValue('search_icon_background_color')
			));
		}

		if (kreate_elated_options()->getOptionValue('search_icon_background_hover_color') !== '') {
			echo kreate_elated_dynamic_css('.eltd-search-opener:hover', array(
				'background-color' => kreate_elated_options()->getOptionValue('search_icon_background_hover_color')
			));
		}

	}

	add_action('kreate_elated_style_dynamic', 'kreate_elated_search_opener_icon_background_colors');
}

if (!function_exists('kreate_elated_search_opener_text_styles')) {

	function kreate_elated_search_opener_text_styles()
	{
		$text_styles = array();

		if (kreate_elated_options()->getOptionValue('search_icon_text_color') !== '') {
			$text_styles['color'] = kreate_elated_options()->getOptionValue('search_icon_text_color');
		}
		if (kreate_elated_options()->getOptionValue('search_icon_text_fontsize') !== '') {
			$text_styles['font-size'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('search_icon_text_fontsize')) . 'px';
		}
		if (kreate_elated_options()->getOptionValue('search_icon_text_lineheight') !== '') {
			$text_styles['line-height'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('search_icon_text_lineheight')) . 'px';
		}
		if (kreate_elated_options()->getOptionValue('search_icon_text_texttransform') !== '') {
			$text_styles['text-transform'] = kreate_elated_options()->getOptionValue('search_icon_text_texttransform');
		}
		if (kreate_elated_options()->getOptionValue('search_icon_text_google_fonts') !== '-1') {
			$text_styles['font-family'] = kreate_elated_get_formatted_font_family(kreate_elated_options()->getOptionValue('search_icon_text_google_fonts')) . ', sans-serif';
		}
		if (kreate_elated_options()->getOptionValue('search_icon_text_fontstyle') !== '') {
			$text_styles['font-style'] = kreate_elated_options()->getOptionValue('search_icon_text_fontstyle');
		}
		if (kreate_elated_options()->getOptionValue('search_icon_text_fontweight') !== '') {
			$text_styles['font-weight'] = kreate_elated_options()->getOptionValue('search_icon_text_fontweight');
		}

		if (!empty($text_styles)) {
			echo kreate_elated_dynamic_css('.eltd-search-icon-text', $text_styles);
		}
		if (kreate_elated_options()->getOptionValue('search_icon_text_color_hover') !== '') {
			echo kreate_elated_dynamic_css('.eltd-search-opener:hover .eltd-search-icon-text', array(
				'color' => kreate_elated_options()->getOptionValue('search_icon_text_color_hover')
			));
		}

	}

	add_action('kreate_elated_style_dynamic', 'kreate_elated_search_opener_text_styles');
}

if (!function_exists('kreate_elated_search_opener_spacing')) {

	function kreate_elated_search_opener_spacing()
	{
		$spacing_styles = array();

		if (kreate_elated_options()->getOptionValue('search_padding_left') !== '') {
			$spacing_styles['padding-left'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('search_padding_left')) . 'px';
		}
		if (kreate_elated_options()->getOptionValue('search_padding_right') !== '') {
			$spacing_styles['padding-right'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('search_padding_right')) . 'px';
		}
		if (kreate_elated_options()->getOptionValue('search_margin_left') !== '') {
			$spacing_styles['margin-left'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('search_margin_left')) . 'px';
		}
		if (kreate_elated_options()->getOptionValue('search_margin_right') !== '') {
			$spacing_styles['margin-right'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('search_margin_right')) . 'px';
		}

		if (!empty($spacing_styles)) {
			echo kreate_elated_dynamic_css('.eltd-search-opener', $spacing_styles);
		}

	}

	add_action('kreate_elated_style_dynamic', 'kreate_elated_search_opener_spacing');
}

if (!function_exists('kreate_elated_search_bar_background')) {

	function kreate_elated_search_bar_background()
	{

		if (kreate_elated_options()->getOptionValue('search_background_color') !== '') {
			echo kreate_elated_dynamic_css('.eltd-search-fade .eltd-fullscreen-search-holder .eltd-fullscreen-search-table, .eltd-fullscreen-search-overlay', array(
				'background-color' => kreate_elated_options()->getOptionValue('search_background_color')
			));
		}
	}

	add_action('kreate_elated_style_dynamic', 'kreate_elated_search_bar_background');
}

if (!function_exists('kreate_elated_search_text_styles')) {

	function kreate_elated_search_text_styles()
	{
		$text_styles = array();

		if (kreate_elated_options()->getOptionValue('search_text_color') !== '') {
			$text_styles['color'] = kreate_elated_options()->getOptionValue('search_text_color');
			echo kreate_elated_dynamic_css('.eltd-fullscreen-search-holder .eltd-form-holder .eltd-search-field:-moz-placeholder', array('color' => $text_styles['color']));
			echo kreate_elated_dynamic_css('.eltd-fullscreen-search-holder .eltd-form-holder .eltd-search-field::-moz-placeholder', array('color' => $text_styles['color'],'opacity' => '1'));
			echo kreate_elated_dynamic_css('.eltd-fullscreen-search-holder .eltd-form-holder .eltd-search-field:-ms-input-placeholder', array('color' => $text_styles['color']));
			echo kreate_elated_dynamic_css('.eltd-fullscreen-search-holder .eltd-form-holder .eltd-search-field::-webkit-input-placeholder', array('color' => $text_styles['color']));

		}
		if (kreate_elated_options()->getOptionValue('search_text_fontsize') !== '') {
			$text_styles['font-size'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('search_text_fontsize')) . 'px';
		}
		if (kreate_elated_options()->getOptionValue('search_text_texttransform') !== '') {
			$text_styles['text-transform'] = kreate_elated_options()->getOptionValue('search_text_texttransform');
		}
		if (kreate_elated_options()->getOptionValue('search_text_google_fonts') !== '-1') {
			$text_styles['font-family'] = kreate_elated_get_formatted_font_family(kreate_elated_options()->getOptionValue('search_text_google_fonts')) . ', sans-serif';
		}
		if (kreate_elated_options()->getOptionValue('search_text_fontstyle') !== '') {
			$text_styles['font-style'] = kreate_elated_options()->getOptionValue('search_text_fontstyle');
		}
		if (kreate_elated_options()->getOptionValue('search_text_fontweight') !== '') {
			$text_styles['font-weight'] = kreate_elated_options()->getOptionValue('search_text_fontweight');
		}
		if (kreate_elated_options()->getOptionValue('search_text_letterspacing') !== '') {
			$text_styles['letter-spacing'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('search_text_letterspacing')) . 'px';
		}

		if (!empty($text_styles)) {
			echo kreate_elated_dynamic_css('.eltd-fullscreen-search-holder .eltd-search-field,.eltd-fullscreen-search-opened .eltd-form-holder .eltd-search-field', $text_styles);
		}

	}

	add_action('kreate_elated_style_dynamic', 'kreate_elated_search_text_styles');
}

if (!function_exists('kreate_elated_search_label_styles')) {

	function kreate_elated_search_label_styles()
	{
		$text_styles = array();

		if (kreate_elated_options()->getOptionValue('search_label_text_color') !== '') {
			$text_styles['color'] = kreate_elated_options()->getOptionValue('search_label_text_color');
		}
		if (kreate_elated_options()->getOptionValue('search_label_text_fontsize') !== '') {
			$text_styles['font-size'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('search_label_text_fontsize')) . 'px';
		}
		if (kreate_elated_options()->getOptionValue('search_label_text_texttransform') !== '') {
			$text_styles['text-transform'] = kreate_elated_options()->getOptionValue('search_label_text_texttransform');
		}
		if (kreate_elated_options()->getOptionValue('search_label_text_google_fonts') !== '-1') {
			$text_styles['font-family'] = kreate_elated_get_formatted_font_family(kreate_elated_options()->getOptionValue('search_label_text_google_fonts')) . ', sans-serif';
		}
		if (kreate_elated_options()->getOptionValue('search_label_text_fontstyle') !== '') {
			$text_styles['font-style'] = kreate_elated_options()->getOptionValue('search_label_text_fontstyle');
		}
		if (kreate_elated_options()->getOptionValue('search_label_text_fontweight') !== '') {
			$text_styles['font-weight'] = kreate_elated_options()->getOptionValue('search_label_text_fontweight');
		}
		if (kreate_elated_options()->getOptionValue('search_label_text_letterspacing') !== '') {
			$text_styles['letter-spacing'] = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('search_label_text_letterspacing')) . 'px';
		}

		if (!empty($text_styles)) {
			echo kreate_elated_dynamic_css('.eltd-fullscreen-search-holder .eltd-search-label', $text_styles);
		}

	}

	add_action('kreate_elated_style_dynamic', 'kreate_elated_search_label_styles');
}

if (!function_exists('kreate_elated_search_icon_styles')) {

	function kreate_elated_search_icon_styles()
	{

		if (kreate_elated_options()->getOptionValue('search_icon_color') !== '') {
			echo kreate_elated_dynamic_css('.eltd-fullscreen-search-holder .eltd-search-submit', array(
				'color' => kreate_elated_options()->getOptionValue('search_icon_color')
			));
		}
		if (kreate_elated_options()->getOptionValue('search_icon_hover_color') !== '') {
			echo kreate_elated_dynamic_css('.eltd-fullscreen-search-holder .eltd-search-submit:hover', array(
				'color' => kreate_elated_options()->getOptionValue('search_icon_hover_color')
			));
		}
		if (kreate_elated_options()->getOptionValue('search_icon_size') !== '') {
			echo kreate_elated_dynamic_css('.eltd-fullscreen-search-holder .eltd-search-submit', array(
				'font-size' => kreate_elated_filter_px(kreate_elated_options()->getOptionValue('search_icon_size')) . 'px'
			));
		}

	}

	add_action('kreate_elated_style_dynamic', 'kreate_elated_search_icon_styles');
}

if (!function_exists('kreate_elated_search_close_icon_styles')) {

	function kreate_elated_search_close_icon_styles()
	{

		if (kreate_elated_options()->getOptionValue('search_close_color') !== '') {
			echo kreate_elated_dynamic_css('.eltd-fullscreen-search-holder .eltd-fullscreen-search-close-container a .eltd-line-close', array(
				'background-color' => kreate_elated_options()->getOptionValue('search_close_color')
			));
		}
		if (kreate_elated_options()->getOptionValue('search_close_hover_color') !== '') {
			echo kreate_elated_dynamic_css('.eltd-fullscreen-search-holder .eltd-fullscreen-search-close-container a:hover .eltd-line-close', array(
				'background-color' => kreate_elated_options()->getOptionValue('search_close_hover_color')
			));
		}

	}

	add_action('kreate_elated_style_dynamic', 'kreate_elated_search_close_icon_styles');
}

if (!function_exists('kreate_elated_search_border_style')) {

	function kreate_elated_search_border_style()
	{

		if (kreate_elated_options()->getOptionValue('search_border_color') !== '') {
			echo kreate_elated_dynamic_css('.eltd-fullscreen-search-holder .eltd-field-holder', array(
				'border-bottom-color' => kreate_elated_options()->getOptionValue('search_border_color')
			));
		}
		if (kreate_elated_options()->getOptionValue('search_border_focus_color') !== '') {
			echo kreate_elated_dynamic_css('.eltd-fullscreen-search-holder .eltd-field-holder .eltd-line', array(
				'background-color' => kreate_elated_options()->getOptionValue('search_border_focus_color')
			));
		}

	}

	add_action('kreate_elated_style_dynamic', 'kreate_elated_search_border_style');
}

?>
