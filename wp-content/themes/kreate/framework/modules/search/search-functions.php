<?php

if( !function_exists('kreate_elated_search_body_class') ) {
	/**
	 * Function that adds body classes for different search types
	 *
	 * @param $classes array original array of body classes
	 *
	 * @return array modified array of classes
	 */
	function kreate_elated_search_body_class($classes) {

		if ( is_active_widget( false, false, 'eltd_search_opener' ) ) {

			$classes[] = 'eltd-fullscreen-search';

			$classes[] = 'eltd-' . kreate_elated_options()->getOptionValue('search_animation');

		}
		return $classes;

	}

	add_filter('body_class', 'kreate_elated_search_body_class');
}

if ( ! function_exists('kreate_elated_get_search') ) {
	/**
	 * Loads search HTML based on search type option.
	 */
	function kreate_elated_get_search() {

		if ( kreate_elated_active_widget( false, false, 'eltd_search_opener' ) ) {

			kreate_elated_load_search_template();

		}
	}

}

if ( ! function_exists('kreate_elated_set_search_position_mobile') ) {
	/**
	 * Hooks search template to mobile header
	 */
	function kreate_elated_set_search_position_mobile() {

		add_action( 'kreate_elated_after_mobile_header_html_open', 'kreate_elated_load_search_template');

	}

}

if ( ! function_exists('kreate_elated_load_search_template') ) {
	/**
	 * Loads HTML template with parameters
	 */
	function kreate_elated_load_search_template() {
		global $kreate_elated_IconCollections;

		$search_icon = '';
		$search_icon_close = '';
		if ( kreate_elated_options()->getOptionValue('search_icon_pack') !== '' ) {
			$search_icon = $kreate_elated_IconCollections->getSearchIcon( kreate_elated_options()->getOptionValue('search_icon_pack'), true );
			$search_icon_close = $kreate_elated_IconCollections->getSearchClose( kreate_elated_options()->getOptionValue('search_icon_pack'), true );
		}

		$parameters = array(
			'search_in_grid'		=> kreate_elated_options()->getOptionValue('search_in_grid') == 'yes' ? true : false,
			'search_icon'			=> $search_icon,
			'search_icon_close'		=> $search_icon_close
		);

		kreate_elated_get_module_template_part( 'templates/types/fullscreen-search', 'search', '', $parameters );

	}

}