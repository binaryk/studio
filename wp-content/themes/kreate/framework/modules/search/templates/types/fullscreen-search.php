<div class="eltd-fullscreen-search-holder">
	<div class="eltd-fullscreen-search-close-container">
		<div class="eltd-search-close-holder">
			<a class="eltd-fullscreen-search-close" href="javascript:void(0)">
                <span class="eltd-line-close line1"></span>
                <span class="eltd-line-close line2"></span>
			</a>
		</div>
	</div>
	<div class="eltd-fullscreen-search-table">
		<div class="eltd-fullscreen-search-cell">
			<div class="eltd-fullscreen-search-inner">
				<?php if ( $search_in_grid ) { ?>
				<div class="eltd-container">
					<div class="eltd-container-inner clearfix">
				<?php } ?>
						<form action="<?php echo esc_url(home_url('/')); ?>" class="eltd-fullscreen-search-form" method="get">
							<div class="eltd-form-holder">
								<span class="eltd-search-label"><?php esc_html_e('Enter your search terms:', 'kreate'); ?></span>
								<div class="eltd-field-holder">
									<input type="text"  name="s" class="eltd-search-field" autocomplete="off" placeholder="<?php esc_html_e('Type your search','kreate'); ?>"/>
									<div class="eltd-line"></div>
								</div>
								<input type="submit" class="eltd-search-submit" value="#" />
							</div>
						</form>
						<?php if ( $search_in_grid ) { ?>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>