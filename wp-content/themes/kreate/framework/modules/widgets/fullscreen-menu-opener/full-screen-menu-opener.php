<?php

class KreateFullScreenMenuOpener extends KreateWidget {
    public function __construct() {
        parent::__construct(
            'eltd_full_screen_menu_opener', // Base ID
            'Elated Full Screen Menu Opener' // Name
        );

		$this->setParams();
    }

	protected function setParams() {

		$this->params = array(
			array(
				'name'			=> 'fullscreen_menu_opener_icon_color',
				'type'			=> 'textfield',
				'title'			=> 'Icon Color',
				'description'	=> 'Define color for Side Area opener icon'
			)
		);

	}

    public function widget($args, $instance) {

		$fullscreen_icon_styles = array();

		if ( !empty($instance['fullscreen_menu_opener_icon_color']) ) {
			$fullscreen_icon_styles[] = 'background-color: ' . $instance['fullscreen_menu_opener_icon_color'];
		}

		?>
        <div class="widget">
            <a href="javascript:void(0)" class="eltd-fullscreen-menu-opener">

                <span class="eltd-fullscreen-menu-opener-inner">
    <!--                --><?php //kreate_elated_inline_style($fullscreen_icon_styles); ?>
                    <span class="eltd-line line1"></span>
                    <span class="eltd-line line2"></span>
                    <span class="eltd-line line3"></span>
                    <span class="eltd-line line4"></span>
                    <span class="eltd-line line5"></span>
                </span>
            </a>
        </div>
    <?php }

}