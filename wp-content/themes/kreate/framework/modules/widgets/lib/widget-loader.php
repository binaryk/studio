<?php

if (!function_exists('kreate_elated_register_widgets')) {

	function kreate_elated_register_widgets() {

		$widgets = array(
			'KreateFullScreenMenuOpener',
			'KreateLatestPosts',
			'KreateSearchOpener',
			'KreateSocialIconWidget',
		);

		foreach ($widgets as $widget) {
			register_widget($widget);
		}
	}
}

add_action('widgets_init', 'kreate_elated_register_widgets');