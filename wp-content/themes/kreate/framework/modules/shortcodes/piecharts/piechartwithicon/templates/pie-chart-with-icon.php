<div class="eltd-pie-chart-with-icon-holder">
	<div class="eltd-percentage-with-icon" <?php echo kreate_elated_get_inline_attrs($pie_chart_data); ?>>
		<?php print $icon; ?>
	</div>
	<div class="eltd-pie-chart-text" <?php kreate_elated_inline_style($pie_chart_style)?>>
		<?php if ($subtitle !== ''){ ?>
		<h5><?php echo esc_html($subtitle); ?></h5>
		<?php } ?>
		<<?php echo esc_html($title_tag)?> class="eltd-pie-title">
			<?php echo esc_html($title); ?>
		</<?php echo esc_html($title_tag)?>>
		<p><?php echo esc_html($text); ?></p>
	</div>
</div>