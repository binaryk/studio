<?php
/**
 * Pie Chart Basic Shortcode Template
 */
?>
<div class="eltd-pie-chart-holder">
	<div class="eltd-percentage" <?php echo kreate_elated_get_inline_attrs($pie_chart_data); ?>>
		<span class="eltd-to-counter">
			<?php echo esc_html($percent ); ?>
		</span>
	</div>
	<div class="eltd-pie-chart-text" <?php kreate_elated_inline_style($pie_chart_style); ?>>
			<?php if ($subtitle !== ''){ ?>
			<h5><?php echo esc_html($subtitle); ?></h5>
			<?php } ?>
			<<?php echo esc_attr($title_tag); ?> class="eltd-pie-title">
				<?php echo esc_html($title); ?>
			</<?php echo esc_attr($title_tag); ?>>
		<p><?php echo esc_html($text); ?></p>
	</div>
</div>