<div class="eltd-blog-slider-item" style="background-image: url(<?php echo wp_get_attachment_url(get_post_thumbnail_id());?>);">
	<div class="eltd-blog-slider-item-inner">
		<div class="eltd-item-text-holder">
			<div class="eltd-item-info-section">
				<?php kreate_elated_post_info(array(
					'category' => 'yes',
				)) ?>
			</div>
			<<?php echo esc_html( $title_tag)?> class="eltd-item-title">
				<a href="<?php echo esc_url(get_permalink()) ?>" >
					<?php echo esc_attr(get_the_title()) ?>
				</a>
			</<?php echo esc_html($title_tag) ?>>
			<?php echo kreate_elated_execute_shortcode('eltd_separator',array(
					'type' => 'normal',
					'position' => 'center',
					'color_dot' => 'black',
					'border_style' => 'dotted_multiple',
					'thickness' => '3',
					'width' => '25%',
					'top_margin' => '25px',
					'bottom_margin' => '20px',
			)); ?>
			<?php if ($text_length != '0') {
				$excerpt = ($text_length > 0) ? substr(get_the_excerpt(), 0, intval($text_length)) : get_the_excerpt(); ?>
				<p class="eltd-excerpt"><?php echo esc_html($excerpt)?></p>
			<?php } ?>
		</div>
		<div class="eltd-item-bottom-holder">
			<div class="eltd-item-btm-left-holder">
				<div class="eltd-item-author-image">
					<?php echo kreate_elated_kses_img(get_avatar(get_the_author_meta( 'ID' ), 30)); ?>
				</div>
				<?php kreate_elated_post_info(array(
					'author' => 'yes',
				)) ?>
			</div>
			<div class="eltd-item-btn-right-holder">
				<?php echo kreate_elated_execute_shortcode('eltd_button',array(
					'type' => 'arrow',
					'text' => 'More',
					'link' => esc_url(get_permalink())
				));?>
			</div>
		</div>
	</div>	
</div>