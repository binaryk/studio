<div class="eltd-tabs <?php echo esc_attr($tab_class) ?> clearfix">
	<ul class="eltd-tabs-nav">
		<?php  foreach ($tabs_titles as $tab_title) {?>
			<li>
				<a href="#tab-<?php echo sanitize_title($tab_title)?>">
					<span class="eltd-tabs-arrow icon-arrows-right"></span>
					<span class="eltd-tabs-text"><?php echo esc_attr($tab_title)?></span>
				</a>
			</li>
		<?php } ?>
	</ul> 
	<?php echo do_shortcode($content) ?>
</div>

