<?php
if(!function_exists('kreate_elated_tabs_typography_styles')){
	function kreate_elated_tabs_typography_styles(){
		$selector = '.eltd-tabs .eltd-tabs-nav li a';
		$tabs_tipography_array = array();
		$font_family = kreate_elated_options()->getOptionValue('tabs_font_family');
		
		if(kreate_elated_is_font_option_valid($font_family)){
			$tabs_tipography_array['font-family'] = kreate_elated_is_font_option_valid($font_family);
		}
		
		$text_transform = kreate_elated_options()->getOptionValue('tabs_text_transform');
        if(!empty($text_transform)) {
            $tabs_tipography_array['text-transform'] = $text_transform;
        }

        $font_style = kreate_elated_options()->getOptionValue('tabs_font_style');
        if(!empty($font_style)) {
            $tabs_tipography_array['font-style'] = $font_style;
        }

        $letter_spacing = kreate_elated_options()->getOptionValue('tabs_letter_spacing');
        if($letter_spacing !== '') {
            $tabs_tipography_array['letter-spacing'] = kreate_elated_filter_px($letter_spacing).'px';
        }

        $font_weight = kreate_elated_options()->getOptionValue('tabs_font_weight');
        if(!empty($font_weight)) {
            $tabs_tipography_array['font-weight'] = $font_weight;
        }

        echo kreate_elated_dynamic_css($selector, $tabs_tipography_array);
	}
	add_action('kreate_elated_style_dynamic', 'kreate_elated_tabs_typography_styles');
}

if(!function_exists('kreate_elated_tabs_inital_color_styles')){
	function kreate_elated_tabs_inital_color_styles(){
		$selector = '.eltd-tabs .eltd-tabs-nav li a';
		$styles = array();
		
		if(kreate_elated_options()->getOptionValue('tabs_color')) {
            $styles['color'] = kreate_elated_options()->getOptionValue('tabs_color');
        }
		
		echo kreate_elated_dynamic_css($selector, $styles);
	}
	add_action('kreate_elated_style_dynamic', 'kreate_elated_tabs_inital_color_styles');
}
if(!function_exists('kreate_elated_tabs_active_color_styles')){
	function kreate_elated_tabs_active_color_styles(){
		$selector = '.eltd-tabs .eltd-tabs-nav li.ui-state-active a, .eltd-tabs .eltd-tabs-nav li.ui-state-hover a';
		$styles = array();
		
		if(kreate_elated_options()->getOptionValue('tabs_color_active')) {
            $styles['color'] = kreate_elated_options()->getOptionValue('tabs_color_active');
        }
		
		echo kreate_elated_dynamic_css($selector, $styles);
	}
	add_action('kreate_elated_style_dynamic', 'kreate_elated_tabs_active_color_styles');
}