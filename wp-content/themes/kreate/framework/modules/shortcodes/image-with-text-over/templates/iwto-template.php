<div class="eltd-image-with-text-over">
	<div class="eltd-iwto-content-holder">
		<?php if ($link !== ''){?>
			<a href="<?php echo esc_url($link);?>" target="<?php echo esc_attr($target);?>">
		<?php } ?>
			<div class="eltd-iwto-text-bckg-holder">
				<div class="eltd-iwto-text-holder-outer">
					<div class="eltd-iwto-text-holder-inner">
						<<?php echo esc_attr($text_tag_valid); ?> class="eltd-iwto-text-holder" <?php kreate_elated_inline_style($text_style); ?>>
							<?php echo esc_html($text); ?>
						</<?php echo esc_attr($text_tag_valid); ?>>
						<?php echo kreate_elated_execute_shortcode('eltd_separator',array(
							'type' => 'normal',
							'class_name' => 'eltd-iwto-separator',
							'position' => 'left',
							'color_dot' => 'white',
							'border_style' => 'dotted_multiple',
							'thickness' => '3',
							'width' => '60px'
						)); ?>
					</div>
				</div>
			</div>
			<div class="eltd-iwto-image">
				<?php echo wp_get_attachment_image($image,'full');?>
			</div>
		<?php if ($link !== ''){?>
			</a>
		<?php } ?>
	</div>
</div>