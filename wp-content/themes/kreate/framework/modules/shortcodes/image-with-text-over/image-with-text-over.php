<?php
namespace Kreate\Modules\Shortcodes\ImageWithTextOver;

use Kreate\Modules\Shortcodes\Lib\ShortcodeInterface;

/**
 * Class ImageWithTextOver
 * @package Kreate\Modules\Shortcodes\ImageWithTextOver
 */
class ImageWithTextOver implements ShortcodeInterface {
	/**
	 * @var string
	 */
	private $base;

	/**
	 *
	 */
	public function __construct() {
		$this->base = 'eltd_image_with_text_over';

		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 *
	 */
	public function vcMap() {
		vc_map(array(
			'name'                      => 'Image With Text Over',
			'base'                      => $this->base,
			'icon'                      => 'icon-wpb-image-with-text-over extended-custom-icon',
			'category'                  => 'by ELATED',
			'allowed_container_element' => 'vc_row',
			'params'                    => array(
				array(
					'type'       => 'attach_image',
					'heading'    => 'Image',
					'param_name' => 'image'
				),
				array(
					'type'        => 'textfield',
					'heading'     => 'Text',
					'param_name'  => 'text'
				),
				array(
					'type'       => 'dropdown',
					'heading'    => 'Text Tag',
					'param_name' => 'text_tag',
					"value" => array(
						""   => "",
						"h2" => "h2",
						"h3" => "h3",
						"h4" => "h4",
						"h5" => "h5",
						"h6" => "h6",
					),
					'dependency' => array('element' => 'text', 'not_empty' => true),
					'group'      => 'Design Options'
				),
				array(
					'type'       => 'colorpicker',
					'heading'    => 'Text Color',
					'param_name' => 'text_color',
					'dependency' => array('element' => 'text', 'not_empty' => true),
					'group'      => 'Design Options'
				),
				array(
					'type'        => 'textfield',
					'heading'     => 'Link',
					'param_name'  => 'link',
					'value'       => '',
					'admin_label' => true
				),
				array(
					'type'       => 'dropdown',
					'heading'    => 'Target',
					'param_name' => 'target',
					'value'      => array(
						''      => '',
						'Self'  => '_self',
						'Blank' => '_blank'
					),
					'dependency' => array('element' => 'link', 'not_empty' => true),
				),
			)
		));
	}

	/**
	 * @param array $atts
	 * @param null $content
	 *
	 * @return string
	 */
	public function render($atts, $content = null) {
		$args = array(
			'image' => '',
			'text' => '',
			'text_tag' => 'h3',
			'text_color' => '',
			'link' => '',
			'target' => '_blank'
		);

		$params       = shortcode_atts($args, $atts);

		$params['text_style'] = $this->getTextStyle($params);
		$params['text_tag_valid'] = $this->getTextTag($params,$args);

		return kreate_elated_get_shortcode_module_template_part('templates/iwto-template', 'image-with-text-over', '', $params);
	}

	/**
	 * Returns style for text
	 *
	 * @param $params
	 *
	 * @return string
	 */

	private function getTextStyle($params) {
		$text_style = array();

		if ($params['text_color'] !== ''){
			$text_style[] = 'color: '.$params['text_color'];
		}

		return implode('; ', $text_style);
	}

	/**
	 * Return Text Tag. If provided heading isn't valid get the default one
	 *
	 * @param $params
	 * @return string
	 */
	private function getTextTag($params,$args) {
		$headings_array = array('h2', 'h3', 'h4', 'h5', 'h6');
		return (in_array($params['text_tag'], $headings_array)) ? $params['text_tag'] : $args['text_tag'];
	}
}