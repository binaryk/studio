<?php

namespace Kreate\Modules\Shortcodes\Lib;

use Kreate\Modules\Counter\Countdown;
use Kreate\Modules\Counter\Counter;
use Kreate\Modules\ElementsHolder\ElementsHolder;
use Kreate\Modules\ElementsHolderItem\ElementsHolderItem;
use Kreate\Modules\ProcessSlider\ProcessSlider;
use Kreate\Modules\ProcessSliderItem\ProcessSliderItem;
use Kreate\Modules\GoogleMap\GoogleMap;
use Kreate\Modules\Separator\Separator;
use Kreate\Modules\PieCharts\PieChartBasic\PieChartBasic;
use Kreate\Modules\PieCharts\PieChartWithIcon\PieChartWithIcon;
use Kreate\Modules\Shortcodes\Icon\Icon;
use Kreate\Modules\Shortcodes\ImageGallery\ImageGallery;
use Kreate\Modules\SocialShare\SocialShare;
use Kreate\Modules\Team\Team;
use Kreate\Modules\OrderedList\OrderedList;
use Kreate\Modules\UnorderedList\UnorderedList;
use Kreate\Modules\ProgressBar\ProgressBar;
use Kreate\Modules\IconListItem\IconListItem;
use Kreate\Modules\Tabs\Tabs;
use Kreate\Modules\Tab\Tab;
use Kreate\Modules\PricingTables\PricingTables;
use Kreate\Modules\PricingTable\PricingTable;
use Kreate\Modules\Accordion\Accordion;
use Kreate\Modules\AccordionTab\AccordionTab;
use Kreate\Modules\BlogList\BlogList;
use Kreate\Modules\BlogSlider\BlogSlider;
use Kreate\Modules\Shortcodes\Button\Button;
use Kreate\Modules\Blockquote\Blockquote;
use Kreate\Modules\CustomFont\CustomFont;
use Kreate\Modules\Highlight\Highlight;
use Kreate\Modules\Dropcaps\Dropcaps;
use Kreate\Modules\Shortcodes\IconWithText\IconWithText;
use Kreate\Modules\Shortcodes\ImageWithTextOver\ImageWithTextOver;
use Kreate\Modules\InfoTable\InfoTable;
use Kreate\Modules\InfoTableRow\InfoTableRow;
/**
 * Class ShortcodeLoader
 */
class ShortcodeLoader
{
	/**
	 * @var private instance of current class
	 */
	private static $instance;
	/**
	 * @var array
	 */
	private $loadedShortcodes = array();

	/**
	 * Private constuct because of Singletone
	 */
	private function __construct() {}

	/**
	 * Private sleep because of Singletone
	 */
	private function __wakeup() {}

	/**
	 * Private clone because of Singletone
	 */
	private function __clone() {}

	/**
	 * Returns current instance of class
	 * @return ShortcodeLoader
	 */
	public static function getInstance() {
		if(self::$instance == null) {
			return new self;
		}

		return self::$instance;
	}

	/**
	 * Adds new shortcode. Object that it takes must implement ShortcodeInterface
	 * @param ShortcodeInterface $shortcode
	 */
	private function addShortcode(ShortcodeInterface $shortcode) {
		if(!array_key_exists($shortcode->getBase(), $this->loadedShortcodes)) {
			$this->loadedShortcodes[$shortcode->getBase()] = $shortcode;
		}
	}

	/**
	 * Adds all shortcodes.
	 *
	 * @see ShortcodeLoader::addShortcode()
	 */
	private function addShortcodes() {
		$this->addShortcode(new ElementsHolder());
		$this->addShortcode(new ElementsHolderItem());
		$this->addShortcode(new ProcessSlider());
		$this->addShortcode(new ProcessSliderItem());
		$this->addShortcode(new Team());
		$this->addShortcode(new Icon());
		$this->addShortcode(new OrderedList());
		$this->addShortcode(new UnorderedList());
		$this->addShortcode(new Counter());
		$this->addShortcode(new Countdown());
		$this->addShortcode(new ProgressBar());
		$this->addShortcode(new IconListItem());
		$this->addShortcode(new Tabs());
		$this->addShortcode(new Tab());
		$this->addShortcode(new PricingTables());
		$this->addShortcode(new PricingTable());
		$this->addShortcode(new PieChartBasic());
		$this->addShortcode(new PieChartWithIcon());
		$this->addShortcode(new Accordion());
		$this->addShortcode(new AccordionTab());
		$this->addShortcode(new BlogList());
		$this->addShortcode(new BlogSlider());
		$this->addShortcode(new Button());
		$this->addShortcode(new Blockquote());
		$this->addShortcode(new CustomFont());
		$this->addShortcode(new Highlight());
		$this->addShortcode(new ImageGallery());
		$this->addShortcode(new GoogleMap());
		$this->addShortcode(new Separator());
		$this->addShortcode(new Dropcaps());
		$this->addShortcode(new IconWithText());
		$this->addShortcode(new SocialShare());
		$this->addShortcode(new ImageWithTextOver());
		$this->addShortcode(new InfoTable());
		$this->addShortcode(new InfoTableRow());
	}
	/**
	 * Calls ShortcodeLoader::addShortcodes and than loops through added shortcodes and calls render method
	 * of each shortcode object
	 */
	public function load() {
		$this->addShortcodes();

		foreach ($this->loadedShortcodes as $shortcode) {
			add_shortcode($shortcode->getBase(), array($shortcode, 'render'));
		}

	}
}

$shortcodeLoader = ShortcodeLoader::getInstance();
$shortcodeLoader->load();