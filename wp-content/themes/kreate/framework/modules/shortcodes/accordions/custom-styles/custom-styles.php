<?php 

if(!function_exists('kreate_elated_accordions_typography_styles')){
	function kreate_elated_accordions_typography_styles(){
		$selector = '.eltd-accordion-holder .eltd-title-holder';
		$styles = array();
		
		$font_family = kreate_elated_options()->getOptionValue('accordions_font_family');
		if(kreate_elated_is_font_option_valid($font_family)){
			$styles['font-family'] = kreate_elated_get_font_option_val($font_family);
		}
		
		$text_transform = kreate_elated_options()->getOptionValue('accordions_text_transform');
       if(!empty($text_transform)) {
           $styles['text-transform'] = $text_transform;
       }

       $font_style = kreate_elated_options()->getOptionValue('accordions_font_style');
       if(!empty($font_style)) {
           $styles['font-style'] = $font_style;
       }

       $letter_spacing = kreate_elated_options()->getOptionValue('accordions_letter_spacing');
       if($letter_spacing !== '') {
           $styles['letter-spacing'] = kreate_elated_filter_px($letter_spacing).'px';
       }

       $font_weight = kreate_elated_options()->getOptionValue('accordions_font_weight');
       if(!empty($font_weight)) {
           $styles['font-weight'] = $font_weight;
       }

       echo kreate_elated_dynamic_css($selector, $styles);
	}
	add_action('kreate_elated_style_dynamic', 'kreate_elated_accordions_typography_styles');
}

if(!function_exists('kreate_elated_accordions_inital_title_color_styles')){
	function kreate_elated_accordions_inital_title_color_styles(){
		$selector = '.eltd-accordion-holder.eltd-initial .eltd-title-holder';
		$styles = array();
		
		if(kreate_elated_options()->getOptionValue('accordions_title_color')) {
           $styles['color'] = kreate_elated_options()->getOptionValue('accordions_title_color');
       }
		echo kreate_elated_dynamic_css($selector, $styles);
	}
	add_action('kreate_elated_style_dynamic', 'kreate_elated_accordions_inital_title_color_styles');
}

if(!function_exists('kreate_elated_accordions_active_title_color_styles')){
	
	function kreate_elated_accordions_active_title_color_styles(){
		$selector = array(
			'.eltd-accordion-holder.eltd-initial .eltd-title-holder.ui-state-active',
			'.eltd-accordion-holder.eltd-initial .eltd-title-holder.ui-state-hover'
		);
		$styles = array();
		
		if(kreate_elated_options()->getOptionValue('accordions_title_color_active')) {
           $styles['color'] = kreate_elated_options()->getOptionValue('accordions_title_color_active');
       }
		
		echo kreate_elated_dynamic_css($selector, $styles);
	}
	add_action('kreate_elated_style_dynamic', 'kreate_elated_accordions_active_title_color_styles');
}
if(!function_exists('kreate_elated_accordions_inital_icon_color_styles')){
	
	function kreate_elated_accordions_inital_icon_color_styles(){
		$selector = '.eltd-accordion-holder.eltd-initial .eltd-title-holder .eltd-accordion-mark';
		$styles = array();
		
		if(kreate_elated_options()->getOptionValue('accordions_icon_color')) {
           $styles['color'] = kreate_elated_options()->getOptionValue('accordions_icon_color');
       }
		echo kreate_elated_dynamic_css($selector, $styles);
	}
	add_action('kreate_elated_style_dynamic', 'kreate_elated_accordions_inital_icon_color_styles');
}
if(!function_exists('kreate_elated_accordions_active_icon_color_styles')){
	
	function kreate_elated_accordions_active_icon_color_styles(){
		$selector = array(
			'.eltd-accordion-holder.eltd-initial .eltd-title-holder.ui-state-active  .eltd-accordion-mark',
			'.eltd-accordion-holder.eltd-initial .eltd-title-holder.ui-state-hover  .eltd-accordion-mark'
		);
		$styles = array();
		
		if(kreate_elated_options()->getOptionValue('accordions_icon_color_active')) {
           $styles['color'] = kreate_elated_options()->getOptionValue('accordions_icon_color_active');
       }
		echo kreate_elated_dynamic_css($selector, $styles);
	}
	add_action('kreate_elated_style_dynamic', 'kreate_elated_accordions_active_icon_color_styles');
}