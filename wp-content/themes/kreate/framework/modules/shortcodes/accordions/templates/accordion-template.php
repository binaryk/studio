<<?php echo esc_attr($title_tag)?> class="clearfix eltd-title-holder">
	<span class="eltd-tab-title">
		<span class="eltd-tab-title-inner">
			<?php echo esc_attr($title)?>
		</span>
	</span>
	<span class="eltd-accordion-mark">
		<span class="eltd-accordion-mark-icon">
			<span class="icon-arrows-plus"></span>
			<span class="icon-arrows-minus"></span>
		</span>
	</span>
</<?php echo esc_attr($title_tag)?>>
<div class="eltd-accordion-content">
	<div class="eltd-accordion-content-inner">
		<?php echo do_shortcode($content)?>
	</div>
</div>
