<div class="eltd-image-gallery">
	<div class="eltd-image-gallery-slider" <?php echo kreate_elated_get_inline_attrs($slider_data); ?>>
		<?php foreach ($images as $image) { ?>
			<div class="eltd-image-gallery-slider-item">
				<div class="eltd-image-gallery-slider-image">
				<?php
				if ($pretty_photo) { ?>
					<a href="<?php echo esc_url($image['url'])?>" data-rel="prettyPhoto[single_pretty_photo]" title="<?php echo esc_attr($image['title']); ?>">
				<?php } ?>
				<?php if(is_array($image_size) && count($image_size)) : ?>
					<?php echo kreate_elated_generate_thumbnail($image['image_id'], null, $image_size[0], $image_size[1]); ?>
				<?php else: ?>
					<?php echo wp_get_attachment_image($image['image_id'], $image_size); ?>
				<?php endif; ?>
				<?php if ($pretty_photo) { ?>
					</a>
				<?php } ?>
				</div>
				<div class="eltd-image-slider-info">
					<div class="eltd-image-slider-info-inner">
						<div class="eltd-image-slider-info-holder">
							<?php
							if ($image['image_alt'] !== ''){ ?>
							<h5><?php echo esc_html($image['image_alt']);?></h5>
							<?php }
							if ($image['image_title'] !== ''){ ?>
							<h3><?php echo esc_html($image['image_title']);?></h3>
							<?php }
							if ($image['image_desc'] !== ''){ ?>
							<p><?php echo esc_html($image['image_desc']);?></p>
							<?php } ?>
						</div>
						<div class="eltd-image-slider-btn-holder">
							<?php
							echo kreate_elated_execute_shortcode('eltd_button',array(
								'type' => 'arrow',
								'hover_type' => 'hide_text',
								'text' => $image['image_link_text'],
								'link' => $image['image_link'],
								'target' => '_blank',
								'color' => '#fff'
							));
							?>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>
</div>