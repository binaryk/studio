<?php if($icon_animation_holder) : ?>
    <span class="eltd-icon-animation-holder" <?php kreate_elated_inline_style($icon_animation_holder_styles); ?>>
<?php endif; ?>

    <span <?php kreate_elated_class_attribute($icon_holder_classes); ?> <?php kreate_elated_inline_style($icon_holder_styles); ?> <?php echo kreate_elated_get_inline_attrs($icon_holder_data); ?>>
        <?php if($link !== '') : ?>
            <a href="<?php echo esc_url($link); ?>" target="<?php echo esc_attr($target); ?>">
			<?php if (($type == "normal") || ($type == "circle" && $icon_hover_effect_direction == "from_left")) : 
				echo kreate_elated_icon_collections()->renderIcon('icon-arrows-slim-right', 'linea_icons', $icon_arrow_params);
			endif; ?>
            <?php if ($type == "circle" && $icon_hover_effect_direction == "from_top") : 
                echo kreate_elated_icon_collections()->renderIcon('icon-arrows-slim-down', 'linea_icons', $icon_arrow_params);
            endif; ?>
        <?php endif; ?>

        <?php echo kreate_elated_icon_collections()->renderIcon($icon, $icon_pack, $icon_params); ?>

        <?php if($link !== '') : ?>
            </a>
        <?php endif; ?>
    </span>

<?php if($icon_animation_holder) : ?>
    </span>
<?php endif; ?>
