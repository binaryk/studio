<a href="<?php echo esc_url($link); ?>" target="<?php echo esc_attr($target); ?>" <?php kreate_elated_inline_style($button_styles); ?> <?php kreate_elated_class_attribute($button_classes); ?> <?php echo kreate_elated_get_inline_attrs($button_data); ?> <?php echo kreate_elated_get_inline_attrs($button_custom_attrs); ?>>
    <span class="eltd-btn-text"><?php echo esc_html($text); ?></span>
    <?php
    if ($type !== 'arrow'){
    	echo kreate_elated_icon_collections()->renderIcon($icon, $icon_pack);
	}else{ ?>

	<span class="eltd-btn-icon-arrow">
		<span <?php kreate_elated_inline_style($arrow_styles); ?> class="eltd-line-1"></span>
		<span <?php kreate_elated_inline_style($arrow_styles); ?> class="eltd-line-2"></span>
		<span <?php kreate_elated_inline_style($arrow_styles); ?> class="eltd-line-3"></span>
	</span>
	<?php }  ?>
</a>