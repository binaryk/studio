<button type="submit" <?php kreate_elated_inline_style($button_styles); ?> <?php kreate_elated_class_attribute($button_classes); ?> <?php echo kreate_elated_get_inline_attrs($button_data); ?> <?php echo kreate_elated_get_inline_attrs($button_custom_attrs); ?>>
    <span class="eltd-btn-text"><?php echo esc_html($text); ?></span>
    <?php echo kreate_elated_icon_collections()->renderIcon($icon, $icon_pack); ?>
</button>