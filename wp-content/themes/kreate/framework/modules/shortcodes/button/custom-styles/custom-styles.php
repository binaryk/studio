<?php

if(!function_exists('kreate_elated_button_typography_styles')) {
    /**
     * Typography styles for all button types
     */
    function kreate_elated_button_typography_styles() {
        $selector = '.eltd-btn';
        $styles = array();

        $font_family = kreate_elated_options()->getOptionValue('button_font_family');
        if(kreate_elated_is_font_option_valid($font_family)) {
            $styles['font-family'] = kreate_elated_get_font_option_val($font_family);
        }

        $text_transform = kreate_elated_options()->getOptionValue('button_text_transform');
        if(!empty($text_transform)) {
            $styles['text-transform'] = $text_transform;
        }

        $font_style = kreate_elated_options()->getOptionValue('button_font_style');
        if(!empty($font_style)) {
            $styles['font-style'] = $font_style;
        }

        $letter_spacing = kreate_elated_options()->getOptionValue('button_letter_spacing');
        if($letter_spacing !== '') {
            $styles['letter-spacing'] = kreate_elated_filter_px($letter_spacing).'px';
        }

        $font_weight = kreate_elated_options()->getOptionValue('button_font_weight');
        if(!empty($font_weight)) {
            $styles['font-weight'] = $font_weight;
        }

        echo kreate_elated_dynamic_css($selector, $styles);
    }

    add_action('kreate_elated_style_dynamic', 'kreate_elated_button_typography_styles');
}

if(!function_exists('kreate_elated_button_outline_styles')) {
    /**
     * Generate styles for outline button
     */
    function kreate_elated_button_outline_styles() {
        //outline styles
        $outline_styles   = array();
        $outline_selector = '.eltd-btn.eltd-btn-outline';

        if(kreate_elated_options()->getOptionValue('btn_outline_text_color')) {
            $outline_styles['color'] = kreate_elated_options()->getOptionValue('btn_outline_text_color');
        }

        if(kreate_elated_options()->getOptionValue('btn_outline_border_color')) {
            $outline_styles['border-color'] = kreate_elated_options()->getOptionValue('btn_outline_border_color');
        }

        echo kreate_elated_dynamic_css($outline_selector, $outline_styles);

        //outline hover styles
        if(kreate_elated_options()->getOptionValue('btn_outline_hover_text_color')) {
            echo kreate_elated_dynamic_css(
                '.eltd-btn.eltd-btn-outline:not(.eltd-btn-custom-hover-color):hover',
                array('color' => kreate_elated_options()->getOptionValue('btn_outline_hover_text_color').'!important')
            );
        }

        if(kreate_elated_options()->getOptionValue('btn_outline_hover_bg_color')) {
            echo kreate_elated_dynamic_css(
                '.eltd-btn.eltd-btn-outline:not(.eltd-btn-custom-hover-bg):hover',
                array('background-color' => kreate_elated_options()->getOptionValue('btn_outline_hover_bg_color').'!important')
            );
        }

        if(kreate_elated_options()->getOptionValue('btn_outline_hover_border_color')) {
            echo kreate_elated_dynamic_css(
                '.eltd-btn.eltd-btn-outline:not(.eltd-btn-custom-border-hover):hover',
                array('border-color' => kreate_elated_options()->getOptionValue('btn_outline_hover_border_color').'!important')
            );
        }
    }

    add_action('kreate_elated_style_dynamic', 'kreate_elated_button_outline_styles');
}

if(!function_exists('kreate_elated_button_arrow_styles')) {
    /**
     * Generate styles for arrow button
     */
    function kreate_elated_button_arrow_styles() {
        //arrow styles
        $arrow_styles   = array();
        $arrow_selector = '.eltd-btn.eltd-btn-arrow';
        $arrow_icon_styles   = array();
        $arrow_icon_selector = '.eltd-btn.eltd-btn-arrow .eltd-btn-icon-arrow .eltd-line-1,
                                .eltd-btn.eltd-btn-arrow .eltd-btn-icon-arrow .eltd-line-2,
                                .eltd-btn.eltd-btn-arrow .eltd-btn-icon-arrow .eltd-line-3';

        if(kreate_elated_options()->getOptionValue('btn_arrow_color')) {
            $arrow_styles['color'] = kreate_elated_options()->getOptionValue('btn_arrow_color');
            $arrow_icon_styles['border-bottom-color'] = kreate_elated_options()->getOptionValue('btn_arrow_color');
        }

        echo kreate_elated_dynamic_css($arrow_selector, $arrow_styles);
        echo kreate_elated_dynamic_css($arrow_icon_selector, $arrow_icon_styles);

    }

    add_action('kreate_elated_style_dynamic', 'kreate_elated_button_arrow_styles');
}

if(!function_exists('kreate_elated_button_solid_styles')) {
    /**
     * Generate styles for solid type buttons
     */
    function kreate_elated_button_solid_styles() {
        //solid styles
        $solid_selector = '.eltd-btn.eltd-btn-solid';
        $solid_styles = array();

        if(kreate_elated_options()->getOptionValue('btn_solid_text_color')) {
            $solid_styles['color'] = kreate_elated_options()->getOptionValue('btn_solid_text_color');
        }

        if(kreate_elated_options()->getOptionValue('btn_solid_border_color')) {
            $solid_styles['border-color'] = kreate_elated_options()->getOptionValue('btn_solid_border_color');
        }

        if(kreate_elated_options()->getOptionValue('btn_solid_bg_color')) {
            $solid_styles['background-color'] = kreate_elated_options()->getOptionValue('btn_solid_bg_color');
        }

        echo kreate_elated_dynamic_css($solid_selector, $solid_styles);

        //solid hover styles
        if(kreate_elated_options()->getOptionValue('btn_solid_hover_text_color')) {
            echo kreate_elated_dynamic_css(
                '.eltd-btn.eltd-btn-solid:not(.eltd-btn-custom-hover-color):hover',
                array('color' => kreate_elated_options()->getOptionValue('btn_solid_hover_text_color').'!important')
            );
        }

        if(kreate_elated_options()->getOptionValue('btn_solid_hover_bg_color')) {
            echo kreate_elated_dynamic_css(
                '.eltd-btn.eltd-btn-solid:not(.eltd-btn-custom-hover-bg):hover',
                array('background-color' => kreate_elated_options()->getOptionValue('btn_solid_hover_bg_color').'!important')
            );
        }

        if(kreate_elated_options()->getOptionValue('btn_solid_hover_border_color')) {
            echo kreate_elated_dynamic_css(
                '.eltd-btn.eltd-btn-solid:not(.eltd-btn-custom-hover-bg):hover',
                array('border-color' => kreate_elated_options()->getOptionValue('btn_solid_hover_border_color').'!important')
            );
        }
    }

    add_action('kreate_elated_style_dynamic', 'kreate_elated_button_solid_styles');
}