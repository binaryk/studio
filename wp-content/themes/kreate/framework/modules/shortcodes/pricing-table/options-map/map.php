<?php

if ( ! function_exists('kreate_elated_pricing_table_options_map') ) {
	/**
	 * Add Pricing Table options to elements page
	 */
	function kreate_elated_pricing_table_options_map() {

		$panel_pricing_table = kreate_elated_add_admin_panel(
			array(
				'page' => '_elements_page',
				'name' => 'panel_pricing_table',
				'title' => 'Pricing Table'
			)
		);

	}

	add_action( 'kreate_elated_options_elements_map', 'kreate_elated_pricing_table_options_map');

}