<div <?php kreate_elated_class_attribute($pricing_table_classes)?>>
	<div class="eltd-price-table-inner">
		<?php if($active == 'yes'){ ?>
			<div class="eltd-active-text">
				<h3 class="eltd-active-text-inner">
					<?php echo esc_attr($active_text) ?>
				</h3>
				<?php echo kreate_elated_execute_shortcode('eltd_separator',array(
						'type' => 'full-width',
						'position' => 'center',
						'color_dot' => 'black',
						'border_style' => 'dotted_multiple',
						'thickness' => '3',
						'top_margin' => '10px',
						'bottom_margin' => '10px',
				)); ?>
			</div>
		<?php } ?>
		<div class="eltd-table-prices">
			<div class="eltd-price-in-table">
				<span class="eltd-price"><?php echo esc_attr($price)?></span>
				<sup class="eltd-value"><?php echo esc_attr($currency) ?></sup>
				<h6 class="eltd-mark"><?php echo esc_attr($price_period)?></h6>
			</div>	
		</div>
		<ul>
			<li class="eltd-table-title">
				<h5 class="eltd-subtitle-content"><?php echo esc_html($subtitle) ?></h5>
				<h3 class="eltd-title-content"><?php echo esc_html($title) ?></h3>
			</li>
			<li class="eltd-table-info">
				<p><?php echo esc_html($info) ?></p>
			</li>
			<li class="eltd-table-content">
				<?php echo do_shortcode($content)?>
			</li>
			<?php 
			if($show_button == "yes" && $button_text !== ''){ ?>
				<li class="eltd-price-button">
					<?php echo kreate_elated_get_button_html(array(
						'link' => $link,
						'text' => $button_text,
						'type' => 'arrow',
						'arrow_alignment' => 'left'
					)); ?>
				</li>				
			<?php } ?>
		</ul>
	</div>
	<?php if ($btm_text !== '') {?>
	<div class="eltd-table-btm">
		<span class="eltd-table-icon icon-basic-lightbulb"></span>
		<h6><?php echo esc_html($btm_text) ?></h6>
	</div>
	<?php } ?>
</div>
