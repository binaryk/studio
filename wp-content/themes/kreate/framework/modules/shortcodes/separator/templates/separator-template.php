<div class="eltd-separator-holder clearfix  <?php echo esc_attr($separator_class); ?>" <?php kreate_elated_inline_style($separator_holder_style); ?>>
	<div class="eltd-separator" <?php kreate_elated_inline_style($separator_style); ?>></div>
</div>
