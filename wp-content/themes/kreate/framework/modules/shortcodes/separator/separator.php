<?php
namespace Kreate\Modules\Separator;

use Kreate\Modules\Shortcodes\Lib\ShortcodeInterface;

class Separator implements ShortcodeInterface{

	private $base;

	function __construct() {
		$this->base = 'eltd_separator';
		add_action('vc_before_init', array($this, 'vcMap'));
	}

	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {

		vc_map(
			array(
				'name' => 'Separator',
				'base' => $this->base,
				'category' => 'by ELATED',
				'icon' => 'icon-wpb-separator extended-custom-icon',
				'show_settings_on_create' => true,
				'class' => 'wpb_vc_separator',
				'custom_markup' => '<div></div>',
				'params' => array(
					array(
						'type' => 'textfield',
						'heading' => 'Extra class name',
						'param_name' => 'class_name',
						'value' => '',
						'description' => 'Style particular content element differently - add a class name and refer to it in custom CSS.'
					),
					array(
						'type' => 'dropdown',
						'heading' => 'Type',
						'param_name' => 'type',
						'value' => array(
							'Normal'		=>	'normal',
							'Full Width'	=>	'full-width'
						),
						'description' => ''
					),
					array(
						'type' => 'dropdown',
						'heading' => 'Position',
						'param_name' => 'position',
						'value' => array(
							'Center'		=> 'center',
							'Left'			=> 'left',
							'Right'			=> 'right'
						),
						'save_always' => true,
						'dependency' => array('element' => 'type', 'value' => array('normal'))
					),
					array(
						'type' => 'dropdown',
						'heading' => 'Border Style',
						'param_name' => 'border_style',
						'value' => array(
							'Default' => '',
							'Dashed' => 'dashed',
							'Solid' => 'solid',
							'Dotted' => 'dotted',
							'Dotted Multiple' => 'dotted_multiple'
						)
					),
					array(
						'type' => 'colorpicker',
						'heading' => 'Color',
						'param_name' => 'color',
						'value' => '',
						'dependency' => array('element' => 'border_style', 'value' => array('','dashed','dotted','solid'))
					),
					array(
						'type' => 'dropdown',
						'heading' => 'Color',
						'param_name' => 'color_dot',
						'value' => array(
							'Black' => 'black',
							'White' => 'white'
						),
						'save_always' => true,
						'dependency' => array('element' => 'border_style', 'value' => array('dotted_multiple'))
					),
					array(
						'type' => 'textfield',
						'heading' => 'Width',
						'param_name' => 'width',
						'value' => '',
						'description' => '',
						'dependency' => array('element' => 'type', 'value' => array('normal'))
					),
					array(
						'type' => 'textfield',
						'heading' => 'Thickness (px)',
						'param_name' => 'thickness',
						'value' => '',
						'description' => 'For Dotted Multiple style this sets number of lines'
					),
					array(
						'type' => 'textfield',
						'heading' => 'Top Margin',
						'param_name' => 'top_margin',
						'value' => '',
						'description' => ''
					),
					array(
						'type' => 'textfield',
						'heading' => 'Bottom Margin',
						'param_name' => 'bottom_margin',
						'value' => '',
					)
				)
			)
		);

	}

	public function render($atts, $content = null) {
		$args = array(
			'class_name'	=>	'',
			'type'			=>	'',
			'position'		=>	'center',
			'color'			=>	'',
			'color_dot'		=>	'',
			'border_style'	=>	'',
			'width'			=>	'',
			'thickness'		=>	'',
			'top_margin'	=>	'',
			'bottom_margin'	=>	''
		);
		
		$params = shortcode_atts($args, $atts);
		extract($params);
		$params['separator_class'] = $this->getSeparatorClass($params);
		$params['separator_style'] = $this->getSeparatorStyle($params);
		$params['separator_holder_style'] = $this->getSeparatorHolderStyle($params);


		$html = kreate_elated_get_shortcode_module_template_part('templates/separator-template', 'separator', '', $params);

		return $html;
	}


	/**
	 * Return Separator classes
	 *
	 * @param $params
	 * @return array
	 */
	private function getSeparatorClass($params) {

		$separator_class = array();

		if ($params['class_name'] !== '') {
			$separator_class[] = $params['class_name'];
		}
		if ($params['position'] !== '') {
			$separator_class[] = 'eltd-separator-'.$params['position'];
		}
		if ($params['type'] !== '') {
			$separator_class[] = 'eltd-separator-'.$params['type'];
		}
		if ($params['border_style'] == 'dotted_multiple'){
			$separator_class[] = 'eltd-dotted-multiple';
			if ($params['color_dot'] !== '') {
				switch ($params['color_dot']) {
					case 'white':
						$separator_class[] = 'eltd-sep-white';
						break;
					default:
						$separator_class[] = 'eltd-sep-black';
						break;
				}
			}
		}

		return implode(' ', $separator_class);

	}

	/**
	 * Return Separator Holder style
	 *
	 * @param $params
	 * @return array
	 */
	private function getSeparatorHolderStyle($params) {

		$separator_holder_style = array();


		if ($params['top_margin'] !== '') {
			if(kreate_elated_string_ends_with($params['top_margin'], '%') || kreate_elated_string_ends_with($params['top_margin'], 'px')) {
				$separator_holder_style[] = 'margin-top: ' . $params['top_margin'];
			}else{
				$separator_holder_style[] = 'margin-top: ' . $params['top_margin'] . 'px';
			}
		}
		if ($params['bottom_margin'] !== '') {
			if(kreate_elated_string_ends_with($params['bottom_margin'], '%') || kreate_elated_string_ends_with($params['bottom_margin'], 'px')) {
				$separator_holder_style[] = 'margin-bottom: ' . $params['bottom_margin'];
			}else{
				$separator_holder_style[] = 'margin-bottom: ' . $params['bottom_margin'] . 'px';
			}
		}

		return implode(';', $separator_holder_style);

	}

	/**
	 * Return Separator style
	 *
	 * @param $params
	 * @return array
	 */
	private function getSeparatorStyle($params) {

		$separator_style = array();

		if ($params['width'] !== '') {
			if(kreate_elated_string_ends_with($params['width'], '%') || kreate_elated_string_ends_with($params['width'], 'px')) {
				$separator_style[] = 'width: ' . $params['width'];
			}else{
				$separator_style[] = 'width: ' . $params['width'] . 'px';
			}
		}

		if ($params['border_style'] !== 'dotted_multiple') {
			if ($params['color'] !== '') {
				$separator_style[] = 'border-color: ' . $params['color'];
			}
			if ($params['border_style'] !== '') {
				$separator_style[] = 'border-style: ' . $params['border_style'];
			}
			if ($params['thickness'] !== '') {
				$separator_style[] = 'border-bottom-width: ' . $params['thickness'] . 'px';
			}
		}
		else{
			if ($params['thickness'] !== '') {
				$separator_style[] = 'height: ' . (5*(intval($params['thickness'])-1) + 1) . 'px';
			}
		}
		return implode(';', $separator_style);

	}

}
