<li class="eltd-blog-list-item clearfix">
	<div class="eltd-blog-list-item-inner">
		<?php if ($hide_image == "no") { ?>
		<div class="eltd-item-image">
				<?php
					if (kreate_elated_get_post_format_class() !== 'gallery'){ ?>
					<a href="<?php echo esc_url(get_permalink()); ?>" target="_blank">
						<?php echo get_the_post_thumbnail(get_the_ID(), $thumb_image_size); ?>
					</a>
				<?php
					}
				else{
					kreate_elated_get_module_template_part('parts/gallery', 'shortcodes/blog-list','',array('thumb_image_size' => $thumb_image_size));
				}
				?>
		</div>
		<?php } ?>
		<div class="eltd-item-text-holder">
			<div class="eltd-post-mark eltd-post-<?php echo kreate_elated_get_post_format_class();?>">
				<span class="<?php echo kreate_elated_get_post_format_icon_class();?>"></span>
			</div>
			<div class="eltd-item-info-section">
				<?php kreate_elated_post_info(array('category' => 'yes', 'date' => 'no', 'comments' => 'no', 'share' => 'no', 'like' => 'no')) ?>
			</div>
			<<?php echo esc_html( $title_tag)?> class="eltd-item-title">
				<a href="<?php echo esc_url(get_permalink()) ?>" >
					<?php echo esc_attr(get_the_title()) ?>
				</a>
			</<?php echo esc_html($title_tag) ?>>
			<?php if ($text_length != '0') {
				$excerpt = ($text_length > 0) ? substr(get_the_excerpt(), 0, intval($text_length)) : get_the_excerpt(); ?>
				<p class="eltd-excerpt"><?php echo esc_html($excerpt)?>...</p>
			<?php } ?>
		</div>
		<div class="eltd-item-bottom-holder">
			<div class="eltd-item-bottom-holder-inner">
				<div class="eltd-item-btm-left-holder">
					<div class="eltd-item-author-image">
						<?php echo kreate_elated_kses_img(get_avatar(get_the_author_meta( 'ID' ), 30)); ?>
					</div>
					<?php kreate_elated_post_info(array(
						'author' => 'yes',
					)) ?>
				</div>
				<div class="eltd-item-btn-right-holder">
					<?php echo kreate_elated_execute_shortcode('eltd_button',array(
						'type' => 'arrow',
						'text' => 'More',
						'arrow_alignment' => 'right',
						'link' => esc_url(get_permalink())
					));?>
				</div>
			</div>
		</div>
	</div>
</li>