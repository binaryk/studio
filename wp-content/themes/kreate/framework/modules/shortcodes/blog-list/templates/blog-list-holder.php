<div class="eltd-blog-list-holder <?php echo esc_attr($holder_classes) ?>">
	<ul class="eltd-blog-list">
	<?php
	$html = '';
		if($query_result->have_posts()):
		while ($query_result->have_posts()) : $query_result->the_post();
			$html .= kreate_elated_get_shortcode_module_template_part('templates/'.$type, 'blog-list', '', $params);
		endwhile;
		print $html;
		else: ?>
		<div class="eltd-blog-list-messsage">
			<p><?php esc_html_e('No posts were found.', 'kreate'); ?></p>
		</div>
		<?php endif;
		wp_reset_postdata();
	?>
	</ul>	
</div>
