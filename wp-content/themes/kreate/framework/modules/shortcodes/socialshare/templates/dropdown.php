<div class="eltd-social-share-holder eltd-dropdown">
	<a href="javascript:void(0)" target="_self" class="eltd-social-share-dropdown-opener">
		<i class="icon-basic-share social_share"></i>
		<span class="eltd-social-share-title"><?php esc_html_e('Share this', 'kreate') ?></span>
	</a>
	<div class="eltd-social-share-dropdown">
		<ul>
			<?php
			$i = 0;
			foreach ($networks as $net) {
				$i++;
				if ($no_shown == ''){
					print $net;
				}
				elseif($i <= $no_shown) {
					print $net;
				}
				else{
					break;
				}
			} ?>
		</ul>
	</div>
</div>