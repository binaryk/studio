<li class="eltd-<?php echo esc_html($name) ?>-share">
	<a class="eltd-share-link" href="#" onclick="<?php print $link; ?>">
		<?php if (kreate_elated_options()->getOptionValue('enable_top_text') == "yes") { ?>
		<span class="eltd-share-label"><?php echo esc_html($top_text); ?></span>
		<?php } ?>
		<span class="eltd-share-name"><?php echo esc_html($text); ?></span>
		<?php if ($custom_icon !== '') { ?>
			<img src="<?php echo esc_url($custom_icon); ?>" alt="<?php echo esc_html($name); ?>" />
		<?php } else { ?>
		<span class="eltd-social-icons-holder">
			<span class="eltd-social-network-icon <?php echo esc_attr($icon); ?>"></span>
			<span class="eltd-social-arrow icon-arrows-slim-right"></span>
		</span>
		<?php } ?>
	</a>
</li>