<div class="eltd-process-slider-item">
	<?php if ($image != '') { ?>
		<div class="eltd-process-image-holder">
			<?php echo wp_get_attachment_image($image,'full');?>
		</div>
	<?php } ?>
	<div class="eltd-process-slide-item-title-holder">
		<span  class="eltd-process-slide-item-number" <?php kreate_elated_inline_style($text_styles); ?>></span>
		<div class="eltd-process-slide-item-titles">
			<?php if ($subtitle != '') {?>
				<h5 class="eltd-process-subtitle" <?php kreate_elated_inline_style($subtitle_styles); ?>><?php echo esc_html($subtitle);?></h5>
			<?php } if ($title != '') {?>
				<h4 class="eltd-process-title" <?php kreate_elated_inline_style($title_styles); ?>>
                    <?php if ($link != '') { ?>
                        <a href="<?php echo esc_url($link); ?>">
                    <?php } ?>
                        <?php echo esc_html($title);?>
                    <?php if ($link != '') { ?>
                        </a>
                    <?php } ?>
                </h4>
			<?php } ?>
		</div>
	</div>
	<?php if ($excerpt != '') { ?>
		<p class="eltd-process-excerpt" <?php kreate_elated_inline_style($text_styles); ?>><?php echo esc_html($excerpt);?></p>
	<?php } ?>
</div>