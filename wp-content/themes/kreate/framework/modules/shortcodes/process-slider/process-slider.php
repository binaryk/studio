<?php
namespace Kreate\Modules\ProcessSlider;

use Kreate\Modules\Shortcodes\Lib\ShortcodeInterface;

class ProcessSlider implements ShortcodeInterface{
	private $base;
	function __construct() {
		$this->base = 'eltd_process_slider';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		vc_map( array(
			'name' => 'Process Slider',
			'base' => $this->base,
			'icon' => 'icon-wpb-process-slider extended-custom-icon',
			'category' => 'by ELATED',
			'as_parent' => array('only' => 'eltd_process_slider_item'),
			'js_view' => 'VcColumnView',
			'params' => array(
				array(
					'type' => 'textfield',
					'admin_label' => true,
					'save_always' => true,
					'heading' => 'Title',
					'param_name' => 'title',
					'value' => 'Our Creative Process',
					'description' => ''
				),
				array(
					'type' => 'dropdown',
					'class' => '',
					'heading' => 'Show Separator',
					'param_name' => 'show_separator',
					'value' => array(
						'Yes'    	=> 'yes',
						'No'     => 'no'
					),
					'description' => '',
					'save_always' => true
				),
				array(
					'type' => 'dropdown',
					'class' => '',
					'heading' => 'Separator Skin',
					'param_name' => 'separator_skin',
					'value' => array(
						'Black'    	=> 'black',
						'White'     => 'white'
					),
					'description' => '',
					'save_always' => true,
					'dependency' => array('element' => 'show_separator', 'value' => 'yes')
				),
				array(
					'type' => 'colorpicker',
					'class' => '',
					'heading' => 'Slider Background Color',
					'param_name' => 'background_color',
					'value' => '',
					'description' => ''
				),
				array(
					'type' => 'colorpicker',
					'class' => '',
					'heading' => 'Icon Background Color',
					'param_name' => 'icon_background_color',
					'value' => '',
					'description' => ''
				),
			)
		));
	}

	public function render($atts, $content = null) {
	
		$args = array(
			'title'				=> 'Our Creative Process',
			'show_separator'	=> 'yes',
			'separator_skin'	=> 'black',
			'background_color' 	=> '',
			'icon_background_color' => ''
		);
		$params = shortcode_atts($args, $atts);
		extract($params);

		$html = '';

		$process_slider_classes = array();
		$process_slider_classes[] = 'eltd-process-slider';
		$process_slider_style = '';
		$icon_style = '';
		$sep_classes = array();

		if($background_color != ''){
			$process_slider_style .= 'background-color:'. $background_color . ';';
		}

		if($icon_background_color != ''){
			$icon_style .= 'background-color:'.$icon_background_color.';';
		}

		if ($separator_skin == 'black') {
			$sep_classes[] = 'eltd-sep-black';
		} else {
			$sep_classes[] = 'eltd-sep-white';
		}

		$process_slider_class = implode(' ', $process_slider_classes);
		$sep_classes = implode(' ', $sep_classes);

        $html .= '<div class="eltd-process-outer">';
		$html .= '<div ' . kreate_elated_get_class_attribute($process_slider_class) . ' ' . kreate_elated_get_inline_attr($process_slider_style, 'style'). '>';
		$html .= '<div class="eltd-process-slider-title-area">';

		$html .= '<h1>'.esc_attr($title).'</h1>';
		if($show_separator == 'yes') {
			$html .= '<div class="eltd-separator-holder clearfix  eltd-separator-left eltd-dotted-multiple '. esc_attr($sep_classes) .'">';
			$html .= '<div class="eltd-separator"></div>';
			$html .= '</div>';
		}
		$html .= '<a href="javascript:void(0)" class="eltd-process-slider-next-nav">';
		$html .= '<span class="eltd-icon-shortcode circle eltd-icon-small" ' .kreate_elated_get_inline_attr($icon_style, 'style'). '><i class="eltd-icon-linea-icon icon-arrows-slim-right eltd-icon-element"></i><i class="eltd-icon-linea-icon icon-arrows-slim-right eltd-icon-element eltd-hover-icon"></i></span>';
        $html .= '</a>';

		$html .= '</div>'; //close process slider title area

		$html .= '<div class="eltd-process-slider-content-inner">';
		$html .= '<div class="eltd-process-slider-content-area clearfix">';
		$html .= do_shortcode($content);
		$html .= '</div>';
		$html .= '</div>';

		$html .= '</div>'; // close process slider
		$html .= '</div>'; // close process outer
        $html .= '<div class="eltd-process-track"><div class="eltd-process-handle"></div></div>'; //dragger

		return $html;

	}

}
