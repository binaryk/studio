<?php
namespace Kreate\Modules\ProcessSliderItem;

use Kreate\Modules\Shortcodes\Lib\ShortcodeInterface;

class ProcessSliderItem implements ShortcodeInterface{
	private $base;

	function __construct() {
		$this->base = 'eltd_process_slider_item';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		if(function_exists('vc_map')){
			vc_map( 
				array(
					'name' => 'Process Slider Item', 'kreate',
					'base' => $this->base,
					'as_child' => array('only' => 'eltd_process_slider'),
					'content_element' => true,
					'category' => 'by ELATED',
					'icon' => 'icon-wpb-process-slider-item extended-custom-icon',
					'show_settings_on_create' => true,
					'params' => array(
						array(
							'type' => 'attach_image',
							'class' => '',
							'heading' => 'Image',
							'param_name' => 'image',
							'value' => '',
							'description' => ''
						),
						array(
						    'type'        => 'textfield',
						    'heading'     => 'Title',
						    'param_name'  => 'title',
						    'value'       => '',
						    'admin_label' => true
						),
					    array(
					        'type'       => 'colorpicker',
					        'heading'    => 'Title Color',
					        'param_name' => 'title_color',
					        'dependency' => array('element' => 'title', 'not_empty' => true),
					        'group'      => 'Text Settings'
					    ),
					    array(
					    	'type'       => 'textfield',
					    	'heading'    => 'Subtitle',
					    	'param_name' => 'subtitle',
						),
						array(
						    'type'       => 'colorpicker',
						    'heading'    => 'Subtitle Color',
						    'param_name' => 'subtitle_color',
						    'dependency' => array('element' => 'subtitle', 'not_empty' => true),
						    'group'      => 'Text Settings'
						),
						array(
						    'type'        => 'textfield',
						    'heading'     => 'Excerpt',
						    'param_name'  => 'excerpt',
						    'value'       => '',
						    'admin_label' => true
						),
					    array(
					        'type'       => 'colorpicker',
					        'heading'    => 'Text Color',
					        'param_name' => 'text_color',
					        'dependency' => array('element' => 'excerpt', 'not_empty' => true),
						    'group'      => 'Text Settings'
					    ),
                        array(
                            'type'        => 'textfield',
                            'heading'     => 'Link',
                            'param_name'  => 'link',
                            'value'       => '',
                            'admin_label' => true
                        ),
					)
				)
			);			
		}
	}

	public function render($atts, $content = null) {
		$args = array(
			'image' => '',
			'title' => '',
			'title_color' => '',
			'subtitle' => '',
			'subtitle_color' => '',
			'excerpt' => '',
			'text_color' => '',
			'link' => ''
		);
		
		$params = shortcode_atts($args, $atts);
		extract($params);
		$params['content']= $content;

		$rand_class = 'eltd-process-slider-item-' . mt_rand(100000,1000000);

		$params['process_slider_item_style'] = $this->getProcessSliderItemStyle($params);
		$params['title_styles'] = $this->getTitleStyles($params);
		$params['subtitle_styles'] = $this->getSubtitleStyles($params);
		$params['text_styles'] = $this->getTextStyles($params);


		$html = kreate_elated_get_shortcode_module_template_part('templates/process-slider-item-template', 'process-slider', '', $params);

		return $html;
	}


	/**
	 * Return Process Slider Item style
	 *
	 * @param $params
	 * @return array
	 */
	private function getProcessSliderItemStyle($params) {

		$process_slider_item_style = array();

		return implode(';', $process_slider_item_style);

	}

	private function getTitleStyles($params) {
	    $styles = array();

	    if(!empty($params['title_color'])) {
	        $styles[] = 'color: '.$params['title_color'];
	    }

	    return $styles;
	}

	private function getSubtitleStyles($params) {
	    $styles = array();

	    if(!empty($params['subtitle_color'])) {
	        $styles[] = 'color: '.$params['subtitle_color'];
	    }

	    return $styles;
	}

	private function getTextStyles($params) {
	    $styles = array();

	    if(!empty($params['text_color'])) {
	        $styles[] = 'color: '.$params['text_color'];
	    }

	    return $styles;
	}

}


