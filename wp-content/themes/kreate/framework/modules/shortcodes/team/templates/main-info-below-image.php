<?php
/**
 * Team info on hover shortcode template
 */
global $kreate_elated_IconCollections;
$number_of_social_icons = 5;
?>

<div class="eltd-team <?php echo esc_attr($team_type) ?>">
	<div class="eltd-team-inner">
		<?php if ($team_image !== '') { ?>
			<div class="eltd-team-image">
				<?php echo wp_get_attachment_image($team_image,'full');?>
			</div>
		<?php } ?>

		<?php if ($team_name !== '' || $team_position !== '' || $team_description != "" || $show_skills == 'yes') { ?>
			<div class="eltd-team-info">
				<?php if ($team_name !== '' || $team_position !== '') { ?>
					<div class="eltd-team-title-holder">
						<?php if ($team_position !== "") { ?>
							<h5 class="eltd-team-position"><?php echo esc_attr($team_position) ?></h5>
						<?php } ?>
						<?php if ($team_name !== '') { ?>
							<<?php echo esc_attr($team_name_tag); ?> class="eltd-team-name" <?php kreate_elated_inline_style($team_name_style);?>>
								<?php echo esc_attr($team_name); ?>
							</<?php echo esc_attr($team_name_tag); ?>>
						<?php } ?>
					</div>
				<?php } ?>

				<?php if ($team_description != "") { ?>
					<div class='eltd-team-text'>
						<div class='eltd-team-text-inner'>
							<div class='eltd-team-description' <?php kreate_elated_inline_style($team_desc_style);?>>
								<p><?php echo esc_attr($team_description) ?></p>
							</div>
						</div>
					</div>
				<?php }
			} ?>

			<div class="eltd-team-social-holder-between">
				<div class="eltd-team-social <?php echo esc_attr($team_social_icon_type) ?>">
					<div class="eltd-team-social-inner">
						<div class="eltd-team-social-wrapp">

							<?php foreach( $team_social_icons as $team_social_icon ) {
								print $team_social_icon;
							} ?>

						</div>
					</div>
				</div>
			</div>
		<?php if ($team_name !== '' || $team_position !== '' || $team_description != "" || $show_skills == 'yes') { ?>
			</div>
		<?php } ?>
	</div>
</div>