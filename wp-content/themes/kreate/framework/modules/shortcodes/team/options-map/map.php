<?php

if ( ! function_exists('kreate_elated_team_options_map') ) {
	/**
	 * Add Team options to elements page
	 */
	function kreate_elated_team_options_map() {

		$panel_team = kreate_elated_add_admin_panel(
			array(
				'page' => '_elements_page',
				'name' => 'panel_team',
				'title' => 'Team'
			)
		);

	}

	add_action( 'kreate_elated_options_elements_map', 'kreate_elated_team_options_map');

}