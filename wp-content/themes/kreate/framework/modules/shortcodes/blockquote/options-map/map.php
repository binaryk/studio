<?php

if ( ! function_exists('kreate_elated_blockquote_options_map') ) {
	/**
	 * Add Blockquote options to elements page
	 */
	function kreate_elated_blockquote_options_map() {

		$panel_blockquote = kreate_elated_add_admin_panel(
			array(
				'page' => '_elements_page',
				'name' => 'panel_blockquote',
				'title' => 'Blockquote'
			)
		);

	}

	add_action( 'kreate_elated_options_elements_map', 'kreate_elated_blockquote_options_map');

}