<div <?php kreate_elated_class_attribute($holder_classes); ?>>
    <div class="eltd-iwt-content-holder">
        <div class="eltd-iwt-icon-title-holder">
            <div class="eltd-iwt-icon-holder">
                <?php echo kreate_elated_get_shortcode_module_template_part('templates/icon', 'icon-with-text', '', array('icon_parameters' => $icon_parameters)); ?>
            </div>
            <div class="eltd-iwt-title-holder">
				<?php if ($subtitle !== ''){ ?>
				<h5 class="eltd-iwt-subtitle" <?php kreate_elated_inline_style($subtitle_styles); ?>>
					<?php echo esc_html($subtitle);?>
				</h5>
				<?php } ?>
				<?php  if ($title !== '') { ?>
                	<<?php echo esc_attr($title_tag); ?> <?php kreate_elated_inline_style($title_styles); ?>><?php echo esc_html($title); ?></<?php echo esc_attr($title_tag); ?>>
                <?php } ?>
            </div>
        </div>
        <div class="eltd-iwt-text-holder">
            <p <?php kreate_elated_inline_style($text_styles); ?>><?php echo esc_html($text); ?></p>
            <?php if (is_array($btn_params) && count($btn_params)) {
                echo kreate_elated_execute_shortcode('eltd_button',$btn_params);
            } ?>
        </div>
    </div>
</div>