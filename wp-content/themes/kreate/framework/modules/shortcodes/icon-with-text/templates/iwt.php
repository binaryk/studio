<div <?php kreate_elated_class_attribute($holder_classes); ?>>
    <div class="eltd-iwt-icon-holder">
        <?php if(!empty($custom_icon)) : ?>
            <?php echo wp_get_attachment_image($custom_icon, 'full'); ?>
        <?php else: ?>
            <?php echo kreate_elated_get_shortcode_module_template_part('templates/icon', 'icon-with-text', '', array('icon_parameters' => $icon_parameters)); ?>
        <?php endif; ?>
    </div>
    <div class="eltd-iwt-content-holder" <?php kreate_elated_inline_style($content_styles); ?>>
        <div class="eltd-iwt-title-holder">
			<?php if ($subtitle !== ''){ ?>
			<h5 class="eltd-iwt-subtitle" <?php kreate_elated_inline_style($subtitle_styles); ?>>
				<?php echo esc_html($subtitle);?>
			</h5>
			<?php } ?>
			<?php  if ($title !== '') { ?>
     	       <<?php echo esc_attr($title_tag); ?> <?php kreate_elated_inline_style($title_styles); ?>><?php echo esc_html($title); ?></<?php echo esc_attr($title_tag); ?>>
            <?php } ?>
        </div>
        <div class="eltd-iwt-text-holder">
            <p <?php kreate_elated_inline_style($text_styles); ?>><?php echo esc_html($text); ?></p>
            <?php if (is_array($btn_params) && count($btn_params)) {
                echo kreate_elated_execute_shortcode('eltd_button',$btn_params);
            } ?>
        </div>
    </div>
</div>