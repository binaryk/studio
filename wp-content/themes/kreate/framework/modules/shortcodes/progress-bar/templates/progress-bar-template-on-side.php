<div class="eltd-progress-bar eltd-progress-on-side">
	<div class="eltd-progress-left-holder">
		<<?php echo esc_attr($title_tag);?> class="eltd-progress-title-holder clearfix" <?php kreate_elated_inline_style($progress_style['text_style']);?>>
			<span class="eltd-progress-title"><?php echo esc_attr($title)?></span>
		</<?php echo esc_attr($title_tag)?>>
		<div class="eltd-progress-content-outer" <?php kreate_elated_inline_style($progress_style['bar_style']);?>>
			<div data-percentage=<?php echo esc_attr($percent)?> class="eltd-progress-content" <?php kreate_elated_inline_style($progress_style['active_bar_style']);?>></div>
		</div>
	</div>
	<div class="eltd-progress-number-wrapper">
		<span class="eltd-progress-number" <?php kreate_elated_inline_style($progress_style['percent_style']);?>>
			<span class="eltd-percent">0</span>
		</span>
	</div>
</div>	