<?php
namespace Kreate\Modules\ProgressBar;

use Kreate\Modules\Shortcodes\Lib\ShortcodeInterface;

class ProgressBar implements ShortcodeInterface{
	private $base;
	
	function __construct() {
		$this->base = 'eltd_progress_bar';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {

		vc_map( array(
			'name' => 'Progress Bar',
			'base' => $this->base,
			'icon' => 'icon-wpb-progress-bar extended-custom-icon',
			'category' => 'by ELATED',
			'allowed_container_element' => 'vc_row',
			'params' => array(
				array(
					'type' => 'textfield',
					'admin_label' => true,
					'heading' => 'Title',
					'param_name' => 'title',
					'description' => ''
				),
				array(
					'type' => 'dropdown',
					'admin_label' => true,
					'heading' => 'Title Tag',
					'param_name' => 'title_tag',
					'value' => array(
						''   => '',
						'h2' => 'h2',
						'h3' => 'h3',
						'h4' => 'h4',	
						'h5' => 'h5',	
						'h6' => 'h6',	
					),
					'description' => ''
				),
				array(
					'type' => 'textfield',
					'admin_label' => true,
					'heading' => 'Percentage',
					'param_name' => 'percent',
					'description' => ''
				),	
				array(
					'type' => 'dropdown',
					'admin_label' => true,
					'heading' => 'Percentage Type',
					'param_name' => 'percentage_type',
					'value' => array(
						'Static' => 'static',
						'On side' => 'on_side',
						'Floating'  => 'floating'
					),
					'dependency' => Array('element' => 'percent', 'not_empty' => true)
				),	
				array(
					'type' => 'dropdown',
					'admin_label' => true,
					'heading' => 'Floating Type',
					'param_name' => 'floating_type',
					'value' => array(
						'Outside Floating'  => 'floating_outside',
						'Inside Floating' => 'floating_inside'
					),
					'dependency' => array('element' => 'percentage_type', 'value' => array('floating'))
				),
				array(
					'type' => 'colorpicker',
					'heading' => 'Bar Color',
					'param_name' => 'bar_color',
					'group' => 'Design Options'
				),
				array(
					'type' => 'colorpicker',
					'heading' => 'Active Bar Color',
					'param_name' => 'active_bar_color',
					'group' => 'Design Options'
				),
				array(
					'type' => 'colorpicker',
					'heading' => 'Text Colors',
					'param_name' => 'text_color',
					'group' => 'Design Options',
					'description' => 'Choose color for title and percent (background color for floating type)'
				)
			)
		) );

	}

	public function render($atts, $content = null) {
		$args = array(
            'title' => '',
            'title_tag' => 'h4',
            'percent' => '100',
            'percentage_type' => 'static',
            'floating_type' => 'floating_outside',
            'bar_color' => '',
            'active_bar_color' => '',
            'text_color' => ''
        );
		$params = shortcode_atts($args, $atts);
		
		//Extract params for use in method
		extract($params);
		$headings_array = array('h2', 'h3', 'h4', 'h5', 'h6');

        //get correct heading value. If provided heading isn't valid get the default one
        $title_tag = (in_array($title_tag, $headings_array)) ? $title_tag : $args['title_tag'];
		
		$params['percentage_classes'] = $this->getPercentageClasses($params);
		$params['progress_style'] = $this->getProgressBarStyle($params);
		$slug = '';
		if ($percentage_type == 'on_side'){
			$slug = 'on-side';
		}
        //init variables
		$html = kreate_elated_get_shortcode_module_template_part('templates/progress-bar-template', 'progress-bar', $slug, $params);
		
        return $html;
		
	}
	/**
    * Generates css classes for progress bar
    *
    * @param $params
    *
    * @return array
    */
	private function getPercentageClasses($params){
		
		$percentClassesArray = array();
		
		if(!empty($params['percentage_type']) !=''){
			
			if($params['percentage_type'] == 'floating'){
				
				$percentClassesArray[]= 'eltd-floating';
				
				if($params['floating_type'] == 'floating_outside'){
					
					$percentClassesArray[] = 'eltd-floating-outside';
					
				}
				
				elseif($params['floating_type'] == 'floating_inside'){
					
					$percentClassesArray[] = 'eltd-floating-inside';
				}

			}
			elseif($params['percentage_type'] == 'static'){
				
				$percentClassesArray[] = 'eltd-static';
				
			}
		}
		return implode(' ', $percentClassesArray);
	}

	/**
    * Generates style for progress bar colors
    *
    * @param $params
    *
    * @return array
    */
	private function getProgressBarStyle($params){
		
		$progress_style = array();
		$progress_style['bar_style'] = '';
		$progress_style['active_bar_style'] = '';
		$progress_style['text_style'] = '';
		$progress_style['percent_style'] = '';
		$progress_style['percent_arrow_style'] = '';
		
		if($params['bar_color'] !== ''){
			$progress_style['bar_style'] .= 'background-color: '.$params['bar_color'].';';
		}
		if($params['active_bar_color'] !== ''){
			$progress_style['active_bar_style'] .= 'background-color: '.$params['active_bar_color'].';';
		}
		if($params['text_color'] !== ''){
			$progress_style['text_style'] .= 'color: '.$params['text_color'].';';
			if ($params['percentage_type'] !== 'floating'){
				$progress_style['percent_style'] .= 'color: '.$params['text_color'].';';
			}
			else{
				$progress_style['percent_style'] .= 'background-color: '.$params['text_color'].';';
				$progress_style['percent_arrow_style'] .= 'border-top-color: '.$params['text_color'].';';
			}
		}
		return $progress_style;
	}
}