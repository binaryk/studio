<?php
namespace Kreate\Modules\InfoTableRow;

use Kreate\Modules\Shortcodes\Lib\ShortcodeInterface;

class InfoTableRow implements ShortcodeInterface{
	private $base;

	function __construct() {
		$this->base = 'eltd_info_table_row';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		if(function_exists('vc_map')){
			vc_map( 
				array(
					'name' => esc_html__('Info Table Row', 'kreate'),
					'base' => $this->base,
					'as_child' => array('only' => 'eltd_info_table'),
					'is_container' => false,
					'category' => 'by ELATED',
					'icon' => 'icon-wpb-info-table-row extended-custom-icon',
					'params' => array(
						array(
							'type' => 'attach_image',
							'class' => '',
							'heading' => 'Image',
							'param_name' => 'image',
							'value' => '',
							'description' => ''
						),
						array(
							'type' => 'textfield',
							'class' => '',
							'heading' => 'Title',
							'param_name' => 'title',
							'value' => ''
						),
						array(
							'type' => 'dropdown',
							'class' => '',
							'heading' => 'Title Tag',
							'param_name' => 'title_tag',
							'value' => array(
								'' => '',
								'h2' => 'h2',
								'h3' => 'h3',
								'h4' => 'h4',
								'h5' => 'h5',
								'h6' => 'h6',
							),
							'dependency' => array('element' => 'title', 'not_empty' => true)
						),
						array(
							'type' => 'textarea',
							'class' => '',
							'heading' => 'Address',
							'param_name' => 'address'
						),
						array(
							'type' => 'textfield',
							'class' => '',
							'heading' => 'Phone Number',
							'param_name' => 'phone_number'
						),
						array(
							'type' => 'textfield',
							'class' => '',
							'heading' => 'Email',
							'param_name' => 'email'
						),
						array(
							'type' => 'dropdown',
							'class' => '',
							'heading' => 'Horizontal Alignment',
							'param_name' => 'horizontal_aligment',
							'value' => array(
								'Default'   => '',
								'Left'    	=> 'left',
								'Center'    => 'center',
								'Right'     => 'right'
							),
							'description' => ''
						),
					)
				)
			);			
		}
	}

	public function render($atts, $content = null) {
		$args = array(
			'image' => '',
			'title' => '',
			'title_tag' => 'h4',
			'address' => '',
			'phone_number' => '',
			'email' => '',
			'horizontal_aligment' => 'left'
		);
		
		$params = shortcode_atts($args, $atts);
		extract($params);

		$params['row_class'] = $this->getRowClass($params);
		$html = kreate_elated_get_shortcode_module_template_part('templates/info-table-row-template', 'info-table', '', $params);

		return $html;
	}

	/**
	 * Return Info Table Row classes
	 *
	 * @param $params
	 * @return array
	 */
	private function getRowClass($params) {

		$row_class = array();

		if ($params['horizontal_aligment'] !== '') {
			$row_class[] = 'eltd-info-'. $params['horizontal_aligment'];
		}


		return implode(' ', $row_class);

	}
}
