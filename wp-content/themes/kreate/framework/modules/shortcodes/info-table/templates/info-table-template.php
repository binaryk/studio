<div  class="eltd-info-table <?php echo esc_attr($info_table_classes);?>">
    <div  class="eltd-info-table-inner">
        <?php echo do_shortcode($content); ?>
    </div>
</div>

