<div  class="eltd-info-table-row clearfix <?php echo esc_attr($row_class);?>">
    <div class="eltd-info-cell">
        <div class="eltd-info-image">
			<?php echo wp_get_attachment_image($image,'full');?>
        </div>
        <?php if ($title !== '') { ?>
        	<<?php echo esc_attr($title_tag);?> class="eltd-info-title"><?php echo esc_html($title);?></<?php echo esc_attr($title_tag);?>>
        <?php } ?>
    </div>
    <div class="eltd-info-cell">
        <p><?php echo wp_kses_post($address); ?></p>
    </div>
    <div class="eltd-info-cell">
        <p><?php echo wp_kses_post($phone_number); ?></p>
        <a href="mailto:<?php echo esc_attr($email);?>"><?php echo esc_attr($email);?></a>
    </div>
</div>
