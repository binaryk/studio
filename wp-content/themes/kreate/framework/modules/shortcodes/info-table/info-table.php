<?php
namespace Kreate\Modules\InfoTable;

use Kreate\Modules\Shortcodes\Lib\ShortcodeInterface;

class InfoTable implements ShortcodeInterface{
	private $base;
	function __construct() {
		$this->base = 'eltd_info_table';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		vc_map( array(
			'name' => esc_html__('Info Table', 'kreate'),
			'base' => $this->base,
			'as_parent' => array('only' => 'eltd_info_table_row'),
			'content_element' => true,
			'show_settings_on_create' => true,
			'category' => 'by ELATED',
			'icon' => 'icon-wpb-info-table extended-custom-icon',
			'js_view' => 'VcColumnView',
			'params' => array(
				array(
					'type' => 'dropdown',
					'class' => '',
					'group' => 'Width & Responsiveness',
					'heading' => 'Switch to One Column',
					'param_name' => 'switch_to_one_column',
					'value' => array(
						'Default'    		=> '',
						'Below 1024px'    	=> '1024',
						'Below 768px'     	=> '768',
						'Below 600px'    	=> '600',
						'Below 480px'    	=> '480',
						'Never'    			=> 'never'
					),
					'description' => 'Choose on which stage cells will be in one column'
				),
			)
		));
	}

	public function render($atts, $content = null) {
	
		$args = array(
			'switch_to_one_column'		=> '',
		);
		$params = shortcode_atts($args, $atts);
		extract($params);
        $params['content'] = $content;

		$html						= '';

		$params['info_table_classes'] = $this->getInfoTableClasses($params);

        $output = '';
        $output = kreate_elated_get_shortcode_module_template_part('templates/info-table-template','info-table', '', $params);
        return $output;
	}


	/**
	 * Return Info Table Row classes
	 *
	 * @param $params
	 * @return string
	 */
	private function getInfoTableClasses($params) {

		$info_table_classes = array();

		if($params['switch_to_one_column'] != ''){
			$info_table_classes[] = 'eltd-responsive-mode-' . $params['switch_to_one_column'];
		} else {
			$info_table_classes[] = 'eltd-responsive-mode-768' ;
		}

		return implode(' ', $info_table_classes);
	}

}
