<?php do_action('kreate_elated_before_page_title'); ?>
<?php if($show_title_area) { ?>

    <div class="eltd-title <?php echo kreate_elated_title_classes(); ?>" style="<?php echo esc_attr($title_height); echo esc_attr($title_background_color); echo esc_attr($title_background_image); ?>" data-height="<?php echo esc_attr(intval(preg_replace('/[^0-9]+/', '', $title_height), 10));?>" <?php echo esc_attr($title_background_image_width); ?>>
        <div class="eltd-title-image"><?php if($title_background_image_src != ""){ ?><img src="<?php echo esc_url($title_background_image_src); ?>" alt="&nbsp;" /> <?php } ?></div>

        <?php if($background_type == "video") {
            if($v_image) { ?>
                <div class="eltd-mobile-video-image" <?php echo kreate_elated_get_inline_attr($video_mobile_style, 'style'); ?>></div>
            <?php } ?>

            <div <?php echo kreate_elated_get_class_attribute($video_overlay_class).kreate_elated_get_inline_attr($video_overlay_style, 'style'); ?>></div>
            <div class="eltd-video-wrap">
                <video class="eltd-video" width="1920" height="800" <?php echo kreate_elated_get_inline_attr($video_attrs, 'poster'); ?> controls="controls" preload="auto" loop autoplay muted>
                    <?php if($video_webm !== "") { ?>
                        <source type="video/webm" src="<?php echo esc_url($video_webm); ?>">
                    <?php } ?>
                    <?php if($video_mp4 !== "") { ?>
                        <source type="video/mp4" src="<?php echo esc_url($video_mp4); ?>">
                    <?php } ?>
                    <?php if($video_ogv !== "") { ?>
                        <source type="video/ogg" src="<?php echo esc_url($video_ogv); ?>">
                    <?php } ?>
                    <object width="320" height="240" type="application/x-shockwave-flash" data="<?php echo ELATED_ASSETS_ROOT; ?>/js/flashmediaelement.swf">
                        <param name="movie" value="<?php echo ELATED_ASSETS_ROOT; ?>/js/flashmediaelement.swf" />
                        <?php if(!empty($video_mp4)) { ?>
                            <param name="flashvars" value="controls=true&amp;file=<?php esc_url($video_mp4); ?>" />
                        <?php } ?>
                        <?php if($v_image) { ?>
                            <img <?php echo kreate_elated_get_inline_attr($v_image, 'src'); ?> width="1920" height="800" title="No video playback capabilities" alt="<?php esc_html_e('video thumb','kreate'); ?>" />';
                        <?php } ?>
                    </object>
                </video>
            </div>
        <?php  } ?>

        <div class="eltd-title-holder" <?php kreate_elated_inline_style($title_holder_height); ?>>
            <div class="eltd-container clearfix">
                <div class="eltd-container-inner">
                    <div class="eltd-title-subtitle-holder" style="<?php echo esc_attr($title_subtitle_holder_padding); ?>">
                        <div class="eltd-title-subtitle-holder-inner">
                        <?php switch ($type){
                            case 'standard': ?>
                                <h1 <?php kreate_elated_inline_style($title_color); ?>><span><?php kreate_elated_title_text(); ?></span></h1>
                                <?php if($has_subtitle){ ?>
                                    <span class="eltd-subtitle" <?php kreate_elated_inline_style($subtitle_color); ?>><span><?php kreate_elated_subtitle_text(); ?></span></span>
                                <?php } ?>
                                <?php kreate_elated_title_show_separator();?>
                                <?php if($enable_breadcrumbs){ ?>
                                    <div class="eltd-breadcrumbs-holder"> <?php kreate_elated_custom_breadcrumbs(); ?></div>
                                <?php } ?>
                            <?php break;
                            case 'breadcrumb': ?>
                                <div class="eltd-breadcrumbs-holder"> <?php kreate_elated_custom_breadcrumbs(); ?></div>
                                <?php kreate_elated_title_show_separator();?>
                            <?php break;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php } ?>
<?php do_action('kreate_elated_after_page_title'); ?>