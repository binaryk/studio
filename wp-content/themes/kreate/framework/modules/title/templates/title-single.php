<?php do_action('kreate_elated_before_page_title'); ?>
<?php if($show_title_area) { ?>

    <div class="eltd-title title-single <?php echo kreate_elated_title_classes(); ?>" style="<?php echo esc_attr($title_height); echo esc_attr($title_background_color); echo esc_attr($title_background_image); ?>" data-height="<?php echo esc_attr(intval(preg_replace('/[^0-9]+/', '', $title_height), 10));?>" <?php echo esc_attr($title_background_image_width); ?>>
        <div class="eltd-title-image"><?php if($title_background_image_src != ""){ ?><img src="<?php echo esc_url($title_background_image_src); ?>" alt="&nbsp;" /> <?php } ?></div>
        <div class="eltd-title-holder" <?php kreate_elated_inline_style($title_holder_height); ?>>
            <div class="eltd-container clearfix">
                <div class="eltd-container-inner">
                    <div class="eltd-title-subtitle-holder" style="<?php echo esc_attr($title_subtitle_holder_padding); ?>">
                        <div class="eltd-title-subtitle-holder-inner">
                            <h1 <?php kreate_elated_inline_style($title_color); ?>><span><?php kreate_elated_title_text(); ?></span></h1>
                            <?php kreate_elated_title_show_separator();?>
                        </div>
                        <div class="eltd-title-info-holder">
                        	<?php kreate_elated_post_info(array(
                        		'author' => 'yes',
	                        	'category' => 'yes'
                        	),'single') ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php } ?>
<?php do_action('kreate_elated_after_page_title'); ?>