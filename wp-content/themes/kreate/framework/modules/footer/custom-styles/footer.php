<?php

if(!function_exists('kreate_elated_footer_style')) {
	/**
	 * Generates custom styles for footer
	 */
	function kreate_elated_footer_style() {
		$selector = array();
		$styles = array();

		$styles['border-top'] = '1px solid #434343';
		$styles['padding-top'] = '34px';

		$footer_in_grid = kreate_elated_options()->getOptionValue('footer_in_grid');
		if ($footer_in_grid == "yes"){
			$selector[] = '.eltd-footer-bottom-holder .eltd-container-inner';
		}
		else{
			$selector[] = '.eltd-footer-bottom-holder .eltd-footer-bottom-holder-inner';
		}

		echo kreate_elated_dynamic_css($selector, $styles);
	}

	add_action('kreate_elated_style_dynamic', 'kreate_elated_footer_style');
}