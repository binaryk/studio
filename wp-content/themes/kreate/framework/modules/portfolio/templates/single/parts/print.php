<div class="eltd-print-holder">
    <a href="#" onClick="window.print();return false;" class="eltd-print-page">
        <span class="icon-basic-printer eltd-icon-printer"></span>
        <span class="eltd-printer-title"><?php esc_html_e("Print page", "kreate") ?></span>
    </a>
</div>