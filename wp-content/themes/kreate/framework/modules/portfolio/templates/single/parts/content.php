<div class="eltd-portfolio-info-item">
    <h3><?php the_title(); ?></h3>
    <div class="eltd-portfolio-content">
        <?php the_content(); ?>
    </div>
</div>