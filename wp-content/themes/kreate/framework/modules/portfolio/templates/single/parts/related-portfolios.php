<?php
if(kreate_elated_options()->getOptionValue('portfolio_single_hide_related') !== 'yes') :
$query = kreate_elated_get_related_post_type(get_the_ID(), array('posts_per_page' => '4'));
$categories = wp_get_post_terms(get_the_ID(), 'portfolio-category');
$category_html = '<div class="eltd-ptf-category-holder">';
$k = 1;
foreach ($categories as $cat) {
    $category_html .= '<span>'.$cat->name.'</span>';
    if (count($categories) != $k) {
        $category_html .= ' | ';
    }
    $k++;
}
$category_html .= '</div>';

if(is_object($query)) { ?>
<div class="eltd-related-projects">
    <h2><?php esc_html_e('Similar Projects','kreate'); ?></h2>
    <?php
    echo kreate_elated_execute_shortcode('eltd_separator', array(
        'type'         => 'normal',
        'position'     => 'left',
        'color_dot'    => 'black',
        'border_style' => 'dotted_multiple',
        'width'        => '140',
        'thickness'    => '3',
        'top_margin'   => '20',
        'bottom_margin'   => '40'
        ));
    ?>

    <div class="eltd-portfolio-list-holder-outer eltd-ptf-standard eltd-ptf-four-columns eltd-with-space">
        <div class="eltd-portfolio-list-holder clearfix">
            <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>

                <article class="eltd-portfolio-item mix">
                    <div class = "eltd-item-image-holder">
                        <a href="<?php echo esc_url(get_permalink()); ?>">
                            <?php
                            echo get_the_post_thumbnail(get_the_ID(),'kreate_elated_square');
                            ?>
                        </a>
                    </div>
                    <div class="eltd-item-text-holder">
                        <?php echo wp_kses_post($category_html); ?>
                        <h3 class="eltd-item-title">
                            <a href="<?php echo esc_url(get_permalink()) ?>">
                            <?php echo esc_attr(get_the_title()); ?>
                            </a>
                        </h3>
                        <?php echo kreate_elated_execute_shortcode('eltd_button',array(
                            'type' => 'arrow',
                            'hover_type' => 'rotate',
                            'text' => esc_html__('View', 'kreate'),
                            'link' => esc_url(get_permalink())
                        ));
                        ?>
                    </div>

                </article>

            <?php
            endwhile;
            endif;
            wp_reset_postdata();
            ?>
            <div class="eltd-gap"></div>
            <div class="eltd-gap"></div>
            <div class="eltd-gap"></div>
            <div class="eltd-gap"></div>
        </div>
    </div>
</div>
<?php }
endif;
?>