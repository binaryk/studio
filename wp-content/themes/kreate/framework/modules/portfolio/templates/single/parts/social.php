<?php if(kreate_elated_options()->getOptionValue('enable_social_share') == 'yes'
    && kreate_elated_options()->getOptionValue('enable_social_share_on_portfolio-item') == 'yes') : ?>
    <div class="eltd-portfolio-social">
        <?php echo kreate_elated_get_social_share_html(array(
            'type' => 'dropdown'
        )); ?>
    </div>
<?php endif; ?>