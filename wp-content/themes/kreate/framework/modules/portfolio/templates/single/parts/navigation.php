<?php if(kreate_elated_options()->getOptionValue('portfolio_single_hide_pagination') !== 'yes') : ?>

    <?php
    $back_to_link = get_post_meta(get_the_ID(), 'portfolio_single_back_to_link', true);
    $nav_same_category = kreate_elated_options()->getOptionValue('portfolio_single_nav_same_category') == 'yes';
    ?>

    <div class="eltd-portfolio-single-nav">
        <?php if(get_previous_post() !== '') : ?>
            <div class="eltd-portfolio-prev">
                <?php if($nav_same_category) {
                    previous_post_link('%link', '<span class="eltd-btn-text">'.esc_html__('Previous','kreate').'</span><span class="eltd-btn-icon-arrow"><span class="eltd-line-1"></span><span class="eltd-line-2"></span><span class="eltd-line-3"></span></span>', true, '', 'portfolio_category');
                } else {
                    previous_post_link('%link', '<span class="eltd-btn-text">'.esc_html__('Previous','kreate').'</span><span class="eltd-btn-icon-arrow"><span class="eltd-line-1"></span><span class="eltd-line-2"></span><span class="eltd-line-3"></span></span>');
                } ?>
            </div>
        <?php endif; ?>

        <?php if($back_to_link !== '') : ?>
            <div class="eltd-portfolio-back-btn">
                <a href="<?php echo esc_url(get_permalink($back_to_link)); ?>">
                    <span class="icon-arrows-squares"></span>
                </a>
            </div>
        <?php endif; ?>

        <?php if(get_next_post() !== '') : ?>
            <div class="eltd-portfolio-next">
                <?php if($nav_same_category) {
                    next_post_link('%link', '<span class="eltd-btn-text">'.esc_html__('Next','kreate').'</span><span class="eltd-btn-icon-arrow"><span class="eltd-line-1"></span><span class="eltd-line-2"></span><span class="eltd-line-3"></span></span>', true, '', 'portfolio_category');
                } else {
                    next_post_link('%link', '<span class="eltd-btn-text">'.esc_html__('Next','kreate').'</span><span class="eltd-btn-icon-arrow"><span class="eltd-line-1"></span><span class="eltd-line-2"></span><span class="eltd-line-3"></span></span>');
                } ?>


                </a>

            </div>
        <?php endif; ?>
    </div>

<?php endif; ?>