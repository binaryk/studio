<div class="eltd-two-columns-75-25 clearfix">
    <div class="eltd-column1">
        <div class="eltd-column-inner">
            <?php
            $media = kreate_elated_get_portfolio_single_media();

            if(is_array($media) && count($media)) : ?>
                <div class="eltd-portfolio-media">
                    <?php foreach($media as $single_media) : ?>
                        <div class="eltd-portfolio-single-media">
                            <?php kreate_elated_portfolio_get_media_html($single_media); ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="eltd-column2">
        <div class="eltd-column-inner">
            <div class="eltd-portfolio-info-holder">

                <div class="eltd-toolbar-holder">
                    <?php kreate_elated_portfolio_get_info_part('social'); ?>
                    <?php kreate_elated_portfolio_get_info_part('print'); ?>
                    <?php kreate_elated_portfolio_get_info_part('like'); ?>
                </div>

                <?php
                //get portfolio content section
                kreate_elated_portfolio_get_info_part('content');

                //get portfolio date section
                kreate_elated_portfolio_get_info_part('date');

                //get portfolio categories section
                kreate_elated_portfolio_get_info_part('categories');

                //get portfolio tags section
                kreate_elated_portfolio_get_info_part('tags');

                //get portfolio custom fields section
                kreate_elated_portfolio_get_info_part('custom-fields');
                ?>
            </div>
        </div>
    </div>
</div>