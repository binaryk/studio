<?php if(kreate_elated_options()->getOptionValue('portfolio_single_hide_date') !== 'yes') : ?>

    <div class="eltd-portfolio-info-item eltd-portfolio-date">
        <h6 class="eltd-info-title"><?php esc_html_e('Date', 'kreate'); ?></h6>

        <p><?php the_time(get_option('date_format')); ?></p>
    </div>

<?php endif; ?>