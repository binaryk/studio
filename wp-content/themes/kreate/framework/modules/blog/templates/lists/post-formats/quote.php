<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="eltd-post-content">
		<?php if (!$hide_image){
			kreate_elated_get_module_template_part('templates/lists/parts/image', 'blog');
		} ?>
		<div class="eltd-post-text">
			<div class="eltd-post-text-inner">
				<div class="eltd-post-mark">
					<span class="icon-basic-lightbulb"></span>
				</div>
				<div class="eltd-post-info">
					<?php kreate_elated_post_info(array('category' => 'yes', 'date' => 'no', 'comments' => 'no', 'share' => 'no', 'like' => 'no')) ?>
				</div>
				<div class="eltd-post-title">
					<h3>
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php echo esc_html(get_post_meta(get_the_ID(), "eltd_post_quote_text_meta", true)); ?></a>
					</h3>
					<span class="eltd-quote-author">&mdash; <?php the_title(); ?></span>
				</div>
			</div>
			<?php
				kreate_elated_get_module_template_part('templates/lists/parts/author-read-more', 'blog');
			?>
		</div>
	</div>
</article>