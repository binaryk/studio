<div class="eltd-item-bottom-holder">
	<div class="eltd-item-bottom-holder-inner">
		<div class="eltd-item-btm-left-holder">
			<div class="eltd-item-author-image">
				<?php echo kreate_elated_kses_img(get_avatar(get_the_author_meta( 'ID' ), 30)); ?>
			</div>
			<?php kreate_elated_post_info(array(
				'author' => 'yes',
			)) ?>
		</div>
		<div class="eltd-item-btn-right-holder">
			<?php kreate_elated_read_more_button();?>
		</div>
	</div>
</div>