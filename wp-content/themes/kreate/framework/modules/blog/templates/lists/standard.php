<div class="eltd-blog-holder eltd-blog-type-standard <?php echo kreate_elated_get_cols_number(); ?>">
	<?php
		if($blog_query->have_posts()) : while ( $blog_query->have_posts() ) : $blog_query->the_post();
			kreate_elated_get_post_format_html($blog_type,$hide_image);
		endwhile;
		else:
			kreate_elated_get_module_template_part('templates/parts/no-posts', 'blog');
		endif;
	?>
	<?php
		if(kreate_elated_options()->getOptionValue('pagination') == 'yes') {
			kreate_elated_pagination($blog_query->max_num_pages, $blog_page_range, $paged);
		}
	?>
</div>
