<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="eltd-post-content">
		<?php if (!$hide_image){
			kreate_elated_get_module_template_part('templates/lists/parts/image', 'blog');
		} ?>
		<div class="eltd-post-text">
			<div class="eltd-post-text-inner">
				<div class="eltd-post-mark">
					<span class="icon-basic-link"></span>
				</div>
				<div class="eltd-post-info">
					<?php kreate_elated_post_info(array('category' => 'yes', 'date' => 'no', 'comments' => 'no', 'share' => 'no', 'like' => 'no')) ?>
				</div>
				<?php kreate_elated_get_module_template_part('templates/lists/parts/title', 'blog'); ?>
				<?php
					kreate_elated_excerpt();
                    $args_pages = array(
                        'before'           => '<div class="eltd-single-links-pages"><div class="eltd-single-links-pages-inner">',
                        'after'            => '</div></div>',
                        'link_before'      => '<span>',
                        'link_after'       => '</span>',
                        'pagelink'         => '%'
                    );

                    wp_link_pages($args_pages);
				?>
			</div>
			<?php
				kreate_elated_get_module_template_part('templates/lists/parts/author-read-more', 'blog');
			?>
		</div>
	</div>
</article>