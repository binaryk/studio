<?php if ( has_post_thumbnail()) { ?>
	<div class="eltd-post-image">
		<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
			<?php the_post_thumbnail('kreate_elated_landscape'); ?>
		</a>
	</div>
<?php } ?>