<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="eltd-post-content">
		<div class="eltd-post-text">
			<div class="eltd-post-text-inner clearfix">
				<div class="eltd-post-title-holder">
					<div class="eltd-post-mark-holder">
						<div class="eltd-post-mark">
							<span class="icon-basic-lightbulb"></span>
						</div>
					</div>
					<div class="eltd-post-title">
						<h2>
							<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php echo esc_html(get_post_meta(get_the_ID(), "eltd_post_quote_text_meta", true)); ?></a>
						</h2>
						<h3 class="eltd-quote-author">&mdash; <?php the_title(); ?></h3>
					</div>
				</div>
				<?php the_content(); ?>
			</div>
		</div>
	</div>
	<?php do_action('kreate_elated_before_blog_article_closed_tag'); ?>
</article>