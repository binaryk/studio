<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="eltd-post-content">
		<div class="eltd-post-text">
			<div class="eltd-post-text-inner clearfix">
				<div class="eltd-post-title-holder">
					<div class="eltd-post-mark-holder">
						<div class="eltd-post-mark">
							<span class="icon-basic-link"></span>
						</div>
					</div>
					<h2 class="eltd-post-title">
						<a href="<?php echo esc_html(get_post_meta(get_the_ID(), "eltd_post_link_link_meta", true)); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
					</h2>
				</div>
				<?php the_content(); ?>
			</div>
		</div>
	</div>
	<?php do_action('kreate_elated_before_blog_article_closed_tag'); ?>
</article>