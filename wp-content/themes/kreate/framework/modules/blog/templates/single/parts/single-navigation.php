<?php if(kreate_elated_options()->getOptionValue('blog_single_navigation') == 'yes'){ ?>
	<?php $navigation_blog_through_category = kreate_elated_options()->getOptionValue('blog_navigation_through_same_category') ?>
	<div class="eltd-blog-single-navigation">
		<div class="eltd-blog-single-navigation-inner">
			<?php if(get_previous_post() != ""){ ?>
				<div class="eltd-blog-single-prev">
					<?php
					if($navigation_blog_through_category == 'yes'){
						previous_post_link('%link','<span class="eltd-btn-text">'.esc_html__('Previous post','kreate').'</span><span class="eltd-btn-icon-arrow"><span class="eltd-line-1"></span><span class="eltd-line-2"></span><span class="eltd-line-3"></span></span>', true,'','category');
					} else {
						previous_post_link('%link','<span class="eltd-btn-text">'.esc_html__('Previous post','kreate').'</span><span class="eltd-btn-icon-arrow"><span class="eltd-line-1"></span><span class="eltd-line-2"></span><span class="eltd-line-3"></span></span>');
					}
					?>
				</div> <!-- close div.blog_prev -->
			<?php } ?>
			<div class="eltd-blog-single-all">
				<a href="<?php echo kreate_elated_get_blog_list_url();?>" target="_blank"><?php esc_html_e('All posts','kreate');?></a>
			</div>
			<?php if(get_next_post() != ""){ ?>
				<div class="eltd-blog-single-next">
					<?php
					if($navigation_blog_through_category == 'yes'){
						next_post_link('%link','<span class="eltd-btn-text">'.esc_html__('Next post','kreate').'</span><span class="eltd-btn-icon-arrow"><span class="eltd-line-1"></span><span class="eltd-line-2"></span><span class="eltd-line-3"></span></span>', true,'','category');
					} else {
						next_post_link('%link','<span class="eltd-btn-text">'.esc_html__('Next post','kreate').'</span><span class="eltd-btn-icon-arrow"><span class="eltd-line-1"></span><span class="eltd-line-2"></span><span class="eltd-line-3"></span></span>');
					}
					?>
				</div>
			<?php } ?>
		</div>
	</div>
<?php } ?>