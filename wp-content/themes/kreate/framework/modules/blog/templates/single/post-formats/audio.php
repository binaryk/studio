<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="eltd-post-content">
		<?php kreate_elated_get_module_template_part('templates/parts/audio', 'blog'); ?>
		<div class="eltd-post-text">
			<div class="eltd-post-text-inner clearfix">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
	<?php do_action('kreate_elated_before_blog_article_closed_tag'); ?>
</article>