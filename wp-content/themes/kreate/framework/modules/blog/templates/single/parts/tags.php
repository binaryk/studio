<div class="eltd-single-tags-holder">
	<h4 class="eltd-single-tags-title"><?php esc_html_e('Post Tags:', 'kreate'); ?></h4>
	<div class="eltd-tags">
		<?php the_tags('', '', ''); ?>
	</div>
</div>