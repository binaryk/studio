<?php
if( !function_exists('kreate_elated_get_blog') ) {
	/**
	 * Function which return holder for all blog lists
	 *
	 * @return holder.php template
	 */
	function kreate_elated_get_blog($type) {

		$sidebar = kreate_elated_sidebar_layout();

		$params = array(
			"blog_type" => $type,
			"sidebar" => $sidebar
		);
		kreate_elated_get_module_template_part('templates/lists/holder', 'blog', '', $params);
	}

}

if( !function_exists('kreate_elated_get_blog_type') ) {

	/**
	 * Function which create query for blog lists
	 *
	 * @return blog list template
	 */

	function kreate_elated_get_blog_type($type) {
		global $wp_query;

		$id = kreate_elated_get_page_id();
		$category = get_post_meta($id, "eltd_blog_category_meta", true);
		$post_number = esc_attr(get_post_meta($id, "eltd_show_posts_per_page_meta", true));

		$paged = kreate_elated_paged();

		if(!is_archive()) {
			$blog_query = new WP_Query('post_type=post&paged=' . $paged . '&cat=' . $category . '&posts_per_page=' . $post_number);
		}else{
			$blog_query = $wp_query;
		}

		if(kreate_elated_options()->getOptionValue('blog_page_range') != ""){
			$blog_page_range = esc_attr(kreate_elated_options()->getOptionValue('blog_page_range'));
		} else{
			$blog_page_range = $blog_query->max_num_pages;
		}
		$params = array(
			'blog_query' => $blog_query,
			'paged' => $paged,
			'blog_page_range' => $blog_page_range,
			'blog_type' => $type,
			'hide_image' => kreate_elated_hide_image()
		);

		kreate_elated_get_module_template_part('templates/lists/' .  $type, 'blog', '', $params);
	}

}



if( !function_exists('kreate_elated_get_post_format_html') ) {

	/**
	 * Function which return html for post formats
	 * @param $type
	 * @return post hormat template
	 */

	function kreate_elated_get_post_format_html($type = "",$hide_image = false) {

		$post_format = get_post_format();

		$supported_post_formats = array('audio', 'video', 'link', 'quote', 'gallery');
		if(in_array($post_format,$supported_post_formats)) {
			$post_format = $post_format;
		} else {
			$post_format = 'standard';
		}

		$slug = '';
		if($type !== ""){
			$slug = $type;
		}

		$params = array();
		$params['hide_image'] = $hide_image;


		kreate_elated_get_module_template_part('templates/lists/post-formats/' . $post_format, 'blog', $slug, $params);

	}

}

if( !function_exists('kreate_elated_get_default_blog_list') ) {
	/**
	 * Function which return default blog list for archive post types
	 *
	 * @return post format template
	 */

	function kreate_elated_get_default_blog_list() {

		$blog_list = 'standard';
		return $blog_list;

	}

}


if (!function_exists('kreate_elated_pagination')) {

	/**
	 * Function which return pagination
	 *
	 * @return blog list pagination html
	 */

	function kreate_elated_pagination($pages = '', $range = 4, $paged = 1){

		$showitems = $range+1;

		if($pages == ''){
			global $wp_query;
			$pages = $wp_query->max_num_pages;
			if(!$pages){
				$pages = 1;
			}
		}
		if(1 != $pages){

			echo '<div class="eltd-pagination">';
				echo '<ul>';
					if($paged > 2 && $paged > $range+1 && $showitems < $pages){
						echo '<li class="eltd-pagination-first-page"><a href="'.esc_url(get_pagenum_link(1)).'"><<</a></li>';
					}
					echo "<li class='eltd-pagination-prev";
					if($paged > 2 && $paged > $range+1 && $showitems < $pages) {
						echo " eltd-pagination prev-first";
					}
					echo "'><a href='".esc_url(get_pagenum_link($paged - 1))."'>&larr;</a></li>";

					for ($i=1; $i <= $pages; $i++){
						if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){
							echo ($paged == $i)? "<li class='active'><span>".$i."</span></li>":"<li><a href='".get_pagenum_link($i)."' class='inactive'>".$i."</a></li>";
						}
					}

					echo '<li class="eltd-pagination-next';
					if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages){
						echo ' eltd-pagination-next-last';
					}
					echo '"><a href="';
					if($pages > $paged){
						echo esc_url(get_pagenum_link($paged + 1));
					} else {
						echo esc_url(get_pagenum_link($paged));
					}
					echo '">&rarr;</a></li>';
					if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages){
						echo '<li class="eltd-pagination-last-page"><a href="'.esc_url(get_pagenum_link($pages)).'">&#8608;</a></li>';
					}
				echo '</ul>';
			echo "</div>";
		}
	}
}

if(!function_exists('kreate_elated_post_info')){
	/**
	 * Function that loads parts of blog post info section
	 * Possible options are:
	 * 1. date
	 * 2. category
	 * 3. author
	 * 4. comments
	 * 5. like
	 * 6. share
	 *
	 * @param $config array of sections to load
	 */
	function kreate_elated_post_info($config, $slug = ''){
		$default_config = array(
			'date' => '',
			'category' => '',
			'author' => '',
			'comments' => '',
			'like' => '',
			'share' => ''
		);

		extract(shortcode_atts($default_config, $config));

		if($date == 'yes'){
			kreate_elated_get_module_template_part('templates/parts/post-info-date', 'blog', $slug);
		}
		if($author == 'yes'){
			kreate_elated_get_module_template_part('templates/parts/post-info-author', 'blog', $slug);
		}
		if($category == 'yes'){
			kreate_elated_get_module_template_part('templates/parts/post-info-category', 'blog', $slug);
		}
		if($comments == 'yes'){
			kreate_elated_get_module_template_part('templates/parts/post-info-comments', 'blog', $slug);
		}
		if($like == 'yes'){
			kreate_elated_get_module_template_part('templates/parts/post-info-like', 'blog', $slug);
		}
		if($share == 'yes'){
			kreate_elated_get_module_template_part('templates/parts/post-info-share', 'blog', $slug);
		}
	}
}

if(!function_exists('kreate_elated_excerpt')) {
	/**
	 * Function that cuts post excerpt to the number of word based on previosly set global
	 * variable $word_count, which is defined in eltd_set_blog_word_count function.
	 *
	 * It current post has read more tag set it will return content of the post, else it will return post excerpt
	 *
	 */
	function kreate_elated_excerpt() {
		global $post;

		if(post_password_required()) {
			echo get_the_password_form();
		}

		//does current post has read more tag set?
		elseif(kreate_elated_post_has_read_more()) {
			global $more;

			//override global $more variable so this can be used in blog templates
			$more = 0;
			the_content(true);
		}

		else {
			//take word count

			$word_count = kreate_elated_excerpt_length();
			//if post excerpt field is filled take that as post excerpt, else that content of the post
			$post_excerpt = $post->post_excerpt != "" ? $post->post_excerpt : strip_tags($post->post_content);

			//remove leading dots if those exists
			$clean_excerpt = strlen($post_excerpt) && strpos($post_excerpt, '...') ? strstr($post_excerpt, '...', true) : $post_excerpt;

			//if clean excerpt has text left
			if($clean_excerpt !== '') {
				//explode current excerpt to words
				$excerpt_word_array = explode (' ', $clean_excerpt);

				//cut down that array based on the number of the words option
				$excerpt_word_array = array_slice ($excerpt_word_array, 0, $word_count);

				//add exerpt postfix
				$excert_postfix		= apply_filters('kreate_elated_excerpt_postfix', '...');

				//and finally implode words together
				$excerpt 			= implode (' ', $excerpt_word_array).$excert_postfix;

				//is excerpt different than empty string?
				if($excerpt !== '') {
					echo '<p class="eltd-post-excerpt">'.wp_kses_post($excerpt).'</p>';
				}
			}
		}
	}
}

if(!function_exists('kreate_elated_get_blog_single')) {

	/**
	 * Function which return holder for single posts
	 *
	 * @return single holder.php template
	 */

	function kreate_elated_get_blog_single() {
		$sidebar = kreate_elated_sidebar_layout();

		$params = array(
			"sidebar" => $sidebar
		);

		kreate_elated_get_module_template_part('templates/single/holder', 'blog', '', $params);
	}
}

if( !function_exists('kreate_elated_get_single_html') ) {

	/**
	 * Function return all parts on single.php page
	 *
	 *
	 * @return single.php html
	 */

	function kreate_elated_get_single_html() {
		$blog_data_classes = '';

		$post_format = get_post_format();
		$supported_post_formats = array('audio', 'video', 'link', 'quote', 'gallery');
		if(in_array($post_format,$supported_post_formats)) {
			$post_format = $post_format;
		} else {
			$post_format = 'standard';
		}

		//Related posts
		$related_posts_params = array();
		$show_related = (kreate_elated_options()->getOptionValue('blog_single_related_posts') == 'yes') ? true : false;
		if ($show_related) {
			$hasSidebar = kreate_elated_sidebar_layout();
			$post_id = get_the_ID();
			$related_post_number = ($hasSidebar == '' || $hasSidebar == 'default' || $hasSidebar == 'no-sidebar') ? 4 : 3;
			$related_posts_options = array(
				'posts_per_page' => $related_post_number
			);
			$related_posts_params = array(
				'related_posts' => kreate_elated_get_related_post_type($post_id, $related_posts_options)
			);
		}

		if (kreate_elated_options()->getOptionValue('blog_additional_full_width') == "yes"){
			$blog_data_classes .= 'eltd-blog-additional-data-full';
		}

		kreate_elated_get_module_template_part('templates/single/post-formats/' . $post_format, 'blog');

		?>
		<div class="eltd-blog-single-data <?php echo esc_attr($blog_data_classes);?>">
		<?php
		kreate_elated_get_module_template_part('templates/single/parts/author-info', 'blog');
		kreate_elated_get_module_template_part('templates/parts/post-info-share','blog');
		if ($show_related) {
			kreate_elated_get_module_template_part('templates/single/parts/related-posts', 'blog', '', $related_posts_params);
		}
		if(kreate_elated_show_comments()){
			comments_template('', true);
		}
		?>
		</div>
		<?php
	}

}
if( !function_exists('kreate_elated_additional_post_items') ) {

	/**
	 * Function which return parts on single.php which are just below content
	 *
	 * @return single.php html
	 */

	function kreate_elated_additional_post_items() {

		if(has_tag() && kreate_elated_options()->getOptionValue('blog_single_tags') == 'yes'){
			kreate_elated_get_module_template_part('templates/single/parts/tags', 'blog');
		}


		$args_pages = array(
			'before'           => '<div class="eltd-single-links-pages"><div class="eltd-single-links-pages-inner">',
			'after'            => '</div></div>',
			'link_before'      => '<span>',
			'link_after'       => '</span>',
			'pagelink'         => '%'
		);

		wp_link_pages($args_pages);

	}
	add_action('kreate_elated_before_blog_article_closed_tag', 'kreate_elated_additional_post_items');
}


if (!function_exists('kreate_elated_comment')) {

	/**
	 * Function which modify default wordpress comments
	 *
	 * @return comments html
	 */

	function kreate_elated_comment($comment, $args, $depth) {

		$GLOBALS['comment'] = $comment;

		global $post;

		$is_pingback_comment = $comment->comment_type == 'pingback';
		$is_author_comment  = $post->post_author == $comment->user_id;

		$comment_class = 'eltd-comment clearfix';

		if($is_author_comment) {
			$comment_class .= ' eltd-post-author-comment';
		}

		if($is_pingback_comment) {
			$comment_class .= ' eltd-pingback-comment';
		}

		?>

		<li>
		<div class="<?php echo esc_attr($comment_class); ?>">
			<?php if(!$is_pingback_comment) { ?>
				<div class="eltd-comment-image"> <?php echo kreate_elated_kses_img(get_avatar($comment, 81)); ?> </div>
			<?php } ?>
			<div class="eltd-comment-text">
				<div class="eltd-comment-info">
					<span class="eltd-comment-name">
						<?php if($is_pingback_comment) { esc_html_e('Pingback:', 'kreate'); } ?>
						<?php echo wp_kses_post(get_comment_author_link()); ?>
						<?php if($is_author_comment) { ?>
							<i class="fa fa-user post-author-comment-icon"></i>
						<?php } ?>
					</span>
					<span class="eltd-comment-date"><?php comment_time(get_option('date_format')); ?>, <?php comment_time(get_option('time_format')); ?></span>
				<?php
					comment_reply_link( array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']) ) );
					edit_comment_link();
				?>
					<span class="eltd-comment-arrow icon-arrows-slim-right"></span>
			</div>
			<?php if(!$is_pingback_comment) { ?>
				<div class="eltd-text-holder" id="comment-<?php echo comment_ID(); ?>">
					<?php comment_text(); ?>
				</div>
			<?php } ?>
		</div>
		</div>
		<?php //li tag will be closed by WordPress after looping through child elements ?>

		<?php
	}
}

if( !function_exists('kreate_elated_blog_archive_pages_classes') ) {

	/**
	 * Function which create classes for container in archive pages
	 *
	 * @return array
	 */

	function kreate_elated_blog_archive_pages_classes($blog_type) {

		$classes = array();
		if(in_array($blog_type, kreate_elated_blog_full_width_types())){
			$classes['holder'] = 'eltd-full-width';
			$classes['inner'] = 'eltd-full-width-inner';
		} elseif(in_array($blog_type, kreate_elated_blog_grid_types())){
			$classes['holder'] = 'eltd-container';
			$classes['inner'] = 'eltd-container-inner clearfix';
		}

		return $classes;

	}

}

if( !function_exists('kreate_elated_blog_full_width_types') ) {

	/**
	 * Function which return all full width blog types
	 *
	 * @return array
	 */

	function kreate_elated_blog_full_width_types() {

		$types = array();

		return $types;

	}

}

if( !function_exists('kreate_elated_blog_grid_types') ) {

	/**
	 * Function which return in grid blog types
	 *
	 * @return array
	 */

	function kreate_elated_blog_grid_types() {

		$types = array('standard');

		return $types;

	}

}

if( !function_exists('kreate_elated_blog_types') ) {

	/**
	 * Function which return all blog types
	 *
	 * @return array
	 */

	function kreate_elated_blog_types() {

		$types = array_merge(kreate_elated_blog_grid_types(), kreate_elated_blog_full_width_types());

		return $types;

	}

}

if( !function_exists('kreate_elated_blog_templates') ) {

	/**
	 * Function which return all blog templates names
	 *
	 * @return array
	 */

	function kreate_elated_blog_templates() {

		$templates = array();
		$grid_templates = kreate_elated_blog_grid_types();
		$full_templates = kreate_elated_blog_full_width_types();
		foreach($grid_templates as $grid_template){
			array_push($templates, 'blog-'.$grid_template);
		}
		foreach($full_templates as $full_template){
			array_push($templates, 'blog-'.$full_template);
		}

		return $templates;

	}

}

if (!function_exists('kreate_elated_excerpt_length')) {
	/**
	 * Function that changes excerpt length based on theme options
	 * @param $length int original value
	 * @return int changed value
	 */
	function kreate_elated_excerpt_length() {

		if(kreate_elated_options()->getOptionValue('number_of_chars') !== ''){
			return esc_attr(kreate_elated_options()->getOptionValue('number_of_chars'));
		} else {
			return 45;
		}
	}

	add_filter( 'excerpt_length', 'kreate_elated_excerpt_length', 999 );
}

if (!function_exists('kreate_elated_excerpt_more')) {
	/**
	 * Function that adds three dotes on the end excerpt
	 * @param $more
	 * @return string
	 */
	function kreate_elated_excerpt_more( $more ) {
		return '...';
	}
	add_filter('excerpt_more', 'kreate_elated_excerpt_more');
}

if(!function_exists('kreate_elated_post_has_read_more')) {
	/**
	 * Function that checks if current post has read more tag set
	 * @return int position of read more tag text. It will return false if read more tag isn't set
	 */
	function kreate_elated_post_has_read_more() {
		global $post;

		return strpos($post->post_content, '<!--more-->');
	}
}

if(!function_exists('kreate_elated_post_has_title')) {
	/**
	 * Function that checks if current post has title or not
	 * @return bool
	 */
	function kreate_elated_post_has_title() {
		return get_the_title() !== '';
	}
}

if (!function_exists('kreate_elated_modify_read_more_link')) {
	/**
	 * Function that modifies read more link output.
	 * Hooks to the_content_more_link
	 * @return string modified output
	 */
	function kreate_elated_modify_read_more_link() {
		$link = '<div class="eltd-more-link-container">';
		$link .= kreate_elated_get_button_html(array(
			'link' => get_permalink().'#more-'.get_the_ID(),
			'size'         => 'small',
			'type'         => 'arrow',
			'arrow_alignment' => 'left',
			'link'         => get_the_permalink(),
			'text'         => esc_html__('More', 'kreate')
		));

		$link .= '</div>';

		return $link;
	}

	add_filter( 'the_content_more_link', 'kreate_elated_modify_read_more_link');
}


if(!function_exists('kreate_elated_has_blog_widget')) {
	/**
	 * Function that checks if latest posts widget is added to widget area
	 * @return bool
	 */
	function kreate_elated_has_blog_widget() {
		$widgets_array = array(
			'eltd_latest_posts_widget'
		);

		foreach ($widgets_array as $widget) {
			$active_widget = is_active_widget(false, false, $widget);

			if($active_widget) {
				return true;
			}
		}

		return false;
	}
}

if(!function_exists('kreate_elated_has_blog_shortcode')) {
	/**
	 * Function that checks if any of blog shortcodes exists on a page
	 * @return bool
	 */
	function kreate_elated_has_blog_shortcode() {
		$blog_shortcodes = array(
			'eltd_blog_list',
			'eltd_blog_slider',
			'eltd_blog_carousel'
		);

		$slider_field = get_post_meta(kreate_elated_get_page_id(), 'eltd_page_slider_meta', true); //TODO change

		foreach ($blog_shortcodes as $blog_shortcode) {
			$has_shortcode = kreate_elated_has_shortcode($blog_shortcode) || kreate_elated_has_shortcode($blog_shortcode, $slider_field);

			if($has_shortcode) {
				return true;
			}
		}

		return false;
	}
}


if(!function_exists('kreate_elated_load_blog_assets')) {
	/**
	 * Function that checks if blog assets should be loaded
	 *
	 * @see eltd_is_blog_template()
	 * @see is_home()
	 * @see is_single()
	 * @see eltd_has_blog_shortcode()
	 * @see is_archive()
	 * @see is_search()
	 * @see eltd_has_blog_widget()
	 * @return bool
	 */
	function kreate_elated_load_blog_assets() {
		return kreate_elated_is_blog_template() || is_home() || is_single() || kreate_elated_has_blog_shortcode() || is_archive() || is_search() || kreate_elated_has_blog_widget();
	}
}

if(!function_exists('kreate_elated_is_blog_template')) {
	/**
	 * Checks if current template page is blog template page.
	 *
	 *@param string current page. Optional parameter.
	 *
	 *@return bool
	 *
	 * @see kreate_elated_get_page_template_name()
	 */
	function kreate_elated_is_blog_template($current_page = '') {

		if($current_page == '') {
			$current_page = kreate_elated_get_page_template_name();
		}

		$blog_templates = kreate_elated_blog_templates();

		return in_array($current_page, $blog_templates);
	}
}

if(!function_exists('kreate_elated_read_more_button')) {
	/**
	 * Function that outputs read more button html if necessary.
	 * It checks if read more button should be outputted only if option for given template is enabled and post does'nt have read more tag
	 * and if post isn't password protected
	 *
	 * @param string $option name of option to check
	 * @param string $class additional class to add to button
	 *
	 */
	function kreate_elated_read_more_button($option = '', $class = '') {
		if($option != '') {
			$show_read_more_button = kreate_elated_options()->getOptionValue($option) == 'yes';
		}else {
			$show_read_more_button = 'yes';
		}
		if($show_read_more_button && !kreate_elated_post_has_read_more() && !post_password_required()) {
			echo kreate_elated_get_button_html(array(
				'size'         => 'small',
				'type'         => 'arrow',
				'arrow_alignment' => 'right',
				'link'         => get_the_permalink(),
				'text'         => esc_html__('More', 'kreate'),
				'custom_class' => $class
			));
		}
	}
}


if( !function_exists('kreate_elated_get_post_format_icon_class') ) {

	/**
	 * Function which return icon class for post formats
	 * @param $type
	 * @return icon class
	 */

	function kreate_elated_get_post_format_icon_class($type = "") {
		$icon_class = '';

		$post_format = get_post_format();

		$supported_post_formats = array('audio', 'video', 'link', 'quote', 'gallery');
		if(in_array($post_format,$supported_post_formats)) {
			$post_format = $post_format;
		} else {
			$post_format = 'standard';
		}

		switch ($post_format) {
			case 'audio':
				$icon_class = 'icon-music-record';
				break;
			case 'video':
				$icon_class = 'icon-music-play-button';
				break;
			case 'link':
				$icon_class = 'icon-basic-link';
				break;
			case 'quote':
				$icon_class = 'icon-basic-lightbulb';
				break;
			default:
				$icon_class = '';
				break;
		}

		return $icon_class;

	}

}

if( !function_exists('kreate_elated_get_post_format_class') ) {

	/**
	 * Function which return class for post formats
	 * @param $type
	 * @return class
	 */

	function kreate_elated_get_post_format_class($type = "") {
		$post_format = get_post_format();

		$supported_post_formats = array('audio', 'video', 'link', 'quote', 'gallery');
		if(in_array($post_format,$supported_post_formats)) {
			$post_format = $post_format;
		} else {
			$post_format = 'standard';
		}

		return $post_format;

	}

}


if( !function_exists('kreate_elated_get_cols_number') ) {

	/**
	 * Function which return class for columns number
	 * @param $type
	 * @return string
	 */

	function kreate_elated_get_cols_number() {
		$cols_class = "eltd-three-columns";
		$cols = kreate_elated_get_meta_field_intersect('number_of_cols');

		switch ($cols) {
			case '1':
				$cols_class = "eltd-one-column";
				break;
			
			case '2':
				$cols_class = "eltd-two-columns";
				break;
			
			case '3':
				$cols_class = "eltd-three-columns";
				break;
			
			case '4':
				$cols_class = "eltd-four-columns";
				break;
			
			default:
				$cols_class = "eltd-three-columns";
				break;
		}

		return $cols_class;

	}

}

if( !function_exists('kreate_elated_get_blog_list_url') ) {

	/**
	 * Function which returns url for blog list
	 * @param $type
	 * @return string
	 */

	function kreate_elated_get_blog_list_url() {
		$blog_link = esc_url(home_url('/'));
		if (get_option('show_on_front') == 'page'){
			$blog_link = get_permalink(get_option('page_for_posts'));
		}

		return $blog_link;
	}

}

if (!function_exists('kreate_elated_get_blog_before_content')){
	/**
	 * Function which shows the_content on blog lists (not archive pages)
	 * @param $type
	 */
	function kreate_elated_get_blog_before_content(){
		$blog_index = is_front_page() && get_option('show_on_front') == 'posts';
		
		if (!is_archive() && !$blog_index){ ?>
			<div class="eltd-blog-list-before">
			<?php the_content(); ?>
			</div>
		<?php }
	}
}


if( !function_exists('kreate_elated_hide_image') ) {

	/**
	 * Function which returns hide image for blog list
	 * @param $type
	 * @return boolean
	 */

	function kreate_elated_hide_image() {
		$hide = kreate_elated_get_meta_field_intersect('blog_hide_image');

		if ($hide == 'yes'){
			return true;
		}
		else{
			return false;
		}

	}

}

?>