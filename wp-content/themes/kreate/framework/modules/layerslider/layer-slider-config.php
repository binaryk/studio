<?php
	if(!function_exists('kreate_elated_layerslider_overrides')) {
		/**
		 * Disables Layer Slider auto update box
		 */
		function kreate_elated_layerslider_overrides() {
			$GLOBALS['lsAutoUpdateBox'] = false;
		}

		add_action('layerslider_ready', 'kreate_elated_layerslider_overrides');
	}
?>