<?php

if ( ! function_exists('kreate_elated_like') ) {
	/**
	 * Returns KreateLike instance
	 *
	 * @return KreateLike
	 */
	function kreate_elated_like() {
		return KreateLike::get_instance();
	}

}

function kreate_elated_get_like() {

	echo wp_kses(kreate_elated_like()->add_like(), array(
		'span' => array(
			'class' => true,
			'aria-hidden' => true,
			'style' => true,
			'id' => true
		),
		'i' => array(
			'class' => true,
			'style' => true,
			'id' => true
		),
		'a' => array(
			'href' => true,
			'class' => true,
			'id' => true,
			'title' => true,
			'style' => true
		)
	));
}

if ( ! function_exists('kreate_elated_like_latest_posts') ) {
	/**
	 * Add like to latest post
	 *
	 * @return string
	 */
	function kreate_elated_like_latest_posts() {
		return kreate_elated_like()->add_like();
	}

}

if ( ! function_exists('kreate_elated_like_portfolio_list') ) {
	/**
	 * Add like to portfolio project
	 *
	 * @param $portfolio_project_id
	 * @return string
	 */
	function kreate_elated_like_portfolio_list($portfolio_project_id) {
		return kreate_elated_like()->add_like_portfolio_list($portfolio_project_id);
	}

}

if ( ! function_exists('kreate_elated_like_portfolio_post') ) {
    /**
     * Add like to portfolio project
     *
     * @param $portfolio_project_id
     * @return string
     */
    function kreate_elated_like_portfolio_post($portfolio_project_id) {
        return kreate_elated_like()->add_like_portfolio_post($portfolio_project_id);
    }

}