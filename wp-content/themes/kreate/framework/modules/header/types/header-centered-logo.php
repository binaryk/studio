<?php
namespace Kreate\Modules\Header\Types;

use Kreate\Modules\Header\Lib\HeaderType;

/**
 * Class that represents Header Centered Logo layout and option
 *
 * Class HeaderCenteredLogo
 */
class HeaderCenteredLogo extends HeaderType {
    protected $heightOfTransparency;
    protected $heightOfCompleteTransparency;
    protected $headerHeight;
    protected $mobileHeaderHeight;

    /**
     * Sets slug property which is the same as value of option in DB
     */
    public function __construct() {
        $this->slug = 'header-centered-logo';

        if(!is_admin()) {

            $menuAreaHeight       = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('menu_area_height_header_centered_logo'));

            $this->menuAreaHeight = $menuAreaHeight !== '' ? (float) $menuAreaHeight : 90;

            $mobileHeaderHeight       = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('mobile_header_height'));
            $this->mobileHeaderHeight = $mobileHeaderHeight !== '' ? (float) $mobileHeaderHeight : 100;

            add_action('wp', array($this, 'setHeaderHeightProps'));

            add_filter('kreate_elated_js_global_variables', array($this, 'getGlobalJSVariables'));
            add_filter('kreate_elated_per_page_js_vars', array($this, 'getPerPageJSVariables'));

        }
    }

    /**
     * Loads template file for this header type
     *
     * @param array $parameters associative array of variables that needs to passed to template
     */
    public function loadTemplate($parameters = array()) {

        $parameters['menu_area_in_grid'] = kreate_elated_options()->getOptionValue('menu_area_in_grid_header_centered_logo') == 'yes' ? true : false;

        $parameters = apply_filters('kreate_elated_header_centered_logo_parameters', $parameters);

        kreate_elated_get_module_template_part('templates/types/'.$this->slug, $this->moduleName, '', $parameters);
    }

    /**
     * Sets header height properties after WP object is set up
     */
    public function setHeaderHeightProps(){
        $this->heightOfTransparency         = $this->calculateHeightOfTransparency();
        $this->heightOfCompleteTransparency = $this->calculateHeightOfCompleteTransparency();
        $this->headerHeight                 = $this->calculateHeaderHeight();
        $this->mobileHeaderHeight           = $this->calculateMobileHeaderHeight();
    }

    /**
     * Returns total height of transparent parts of header
     *
     * @return int
     */
    public function calculateHeightOfTransparency() {
        $id = kreate_elated_get_page_id();
        $transparencyHeight = 90;
        $menuAreaTransparent = true;

        if(get_post_meta($id, 'eltd_menu_area_background_color_header_centered_logo_meta', true) !== '') {
            $menuAreaTransparent = get_post_meta($id, 'eltd_menu_area_background_color_header_centered_logo_meta', true) !== '' &&
                                    get_post_meta($id, 'eltd_menu_area_background_transparency_header_centered_logo_meta', true) !== '1';
        }elseif(kreate_elated_options()->getOptionValue('menu_area_background_color_header_centered_logo') != ''){
            $menuAreaTransparent = kreate_elated_options()->getOptionValue('menu_area_background_color_header_centered_logo') !== '' &&
                                   kreate_elated_options()->getOptionValue('menu_area_background_transparency_header_centered_logo') !== '1';
        }


        $sliderExists = get_post_meta($id, 'eltd_page_slider_meta', true) !== '';

        if($sliderExists){
            $menuAreaTransparent = true;
        }

        if($menuAreaTransparent) {
            $transparencyHeight = $this->menuAreaHeight;
        }

        return $transparencyHeight;
    }

    /**
     * Returns height of completely transparent header parts
     *
     * @return int
     */
    public function calculateHeightOfCompleteTransparency() {
        $id = kreate_elated_get_page_id();
        $transparencyHeight = 90;
        $menuAreaTransparent = true;

        if(get_post_meta($id, 'eltd_menu_area_background_color_header_centered_logo_meta', true) !== ''){
            $menuAreaTransparent = get_post_meta($id, 'eltd_menu_area_background_color_header_centered_logo_meta', true) !== '' &&
                                   get_post_meta($id, 'eltd_menu_area_background_transparency_header_centered_logo_meta', true) === '0';
        } elseif(kreate_elated_options()->getOptionValue('menu_area_background_color_header_centered_logo') != ''){

            $menuAreaTransparent = kreate_elated_options()->getOptionValue('menu_area_background_color_header_centered_logo') !== '' &&
                                   kreate_elated_options()->getOptionValue('menu_area_background_transparency_header_centered_logo') === '0';
        }

        if($menuAreaTransparent) {
            $transparencyHeight = $this->menuAreaHeight;
        }

        return $transparencyHeight;
    }


    /**
     * Returns total height of header
     *
     * @return int|string
     */
    public function calculateHeaderHeight() {
        $headerHeight = $this->menuAreaHeight;

        return $headerHeight;
    }

    /**
     * Returns total height of mobile header
     *
     * @return int|string
     */
    public function calculateMobileHeaderHeight() {
        $mobileHeaderHeight = $this->mobileHeaderHeight;

        return $mobileHeaderHeight;
    }

    /**
     * Returns global js variables of header
     *
     * @param $globalVariables
     * @return int|string
     */
    public function getGlobalJSVariables($globalVariables) {
        $globalVariables['eltdLogoAreaHeight'] = 0;
        $globalVariables['eltdMenuAreaHeight'] = $this->headerHeight;
        $globalVariables['eltdMobileHeaderHeight'] = $this->mobileHeaderHeight;

        return $globalVariables;
    }

    /**
     * Returns per page js variables of header
     *
     * @param $perPageVars
     * @return int|string
     */
    public function getPerPageJSVariables($perPageVars) {
        //calculate transparency height only if header has no sticky behaviour
        if(!in_array(kreate_elated_options()->getOptionValue('header_behaviour'), array('sticky-header-on-scroll-up','sticky-header-on-scroll-down-up'))) {
            $perPageVars['eltdHeaderTransparencyHeight'] = $this->headerHeight - $this->heightOfCompleteTransparency;
        }else{
            $perPageVars['eltdHeaderTransparencyHeight'] = 0;
        }

        return $perPageVars;
    }
}