<?php do_action('kreate_elated_before_mobile_navigation'); ?>

<nav class="eltd-mobile-nav">
    <?php wp_nav_menu(array(
        'theme_location' => 'mobile-navigation' ,
        'container'  => '',
        'container_class' => '',
        'menu_class' => '',
        'menu_id' => '',
        'fallback_cb' => 'top_navigation_fallback',
        'link_before' => '<span>',
        'link_after' => '</span>',
        'walker' => new KreateMobileNavigationWalker()
    )); ?>
</nav>

<?php do_action('kreate_elated_after_mobile_navigation'); ?>