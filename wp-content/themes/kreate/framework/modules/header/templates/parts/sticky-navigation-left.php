<div>
<nav class="eltd-main-menu eltd-drop-down eltd-items-number-<?php echo kreate_elated_get_nav_menu_items_number('left-main-navigation'); ?> <?php echo esc_attr($additional_class); ?>">
    <?php
    wp_nav_menu( array(
        'theme_location' => 'left-main-navigation' ,
        'container'  => '',
        'container_class' => '',
        'menu_class' => 'clearfix',
        'menu_id' => '',
        'fallback_cb' => 'top_navigation_fallback',
        'link_before' => '<span>',
        'link_after' => '</span>',
        'walker' => new KreateStickyNavigationWalker()
    ));
    ?>
</nav>
</div>