<?php do_action('kreate_elated_before_page_header'); ?>

<header class="eltd-page-header">

    <div class="eltd-menu-area" <?php kreate_elated_inline_style($menu_area_background_color); ?>>
        <?php do_action( 'kreate_elated_after_header_menu_area_html_open' )?>
        <div class="eltd-vertical-align-containers">
            <div class="eltd-position-left">
                <div class="eltd-position-left-inner">

                    <?php if(is_active_sidebar('eltd-left-from-logo')) : ?>
                        <div class="widget">
                            <?php dynamic_sidebar('eltd-left-from-logo'); ?>
                        </div>
                    <?php endif; ?>
                    <?php kreate_elated_get_left_main_menu(); ?>
                </div>
            </div>
            <div class="eltd-position-center">
                <div class="eltd-position-center-inner">
                    <?php if(!$hide_logo) {
                        kreate_elated_get_logo();
                    } ?>
                </div>
            </div>
            <div class="eltd-position-right">
                <div class="eltd-position-right-inner">
                    <?php kreate_elated_get_right_main_menu(); ?>
                    <?php if(is_active_sidebar('eltd-right-from-logo')) : ?>
                        <div class="widget">
                            <?php dynamic_sidebar('eltd-right-from-logo'); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <?php if($show_sticky) {
        kreate_elated_get_sticky_header('centered');
    } ?>
</header>

<?php do_action('kreate_elated_after_page_header'); ?>

