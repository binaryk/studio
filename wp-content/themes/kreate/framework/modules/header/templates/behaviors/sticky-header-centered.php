<?php do_action('kreate_elated_before_sticky_header'); ?>

<div class="eltd-sticky-header">
    <?php do_action( 'kreate_elated_after_sticky_menu_html_open' ); ?>
    <div class="eltd-sticky-holder">
        <div class="eltd-vertical-align-containers">
            <div class="eltd-position-left">
                <div class="eltd-position-left-inner">

                    <?php if(is_active_sidebar('eltd-sticky-left')) : ?>
                        <div class="widget">
                        <?php dynamic_sidebar('eltd-sticky-left'); ?>
                        </div>
                    <?php endif; ?>
                    <?php kreate_elated_get_sticky_left_main_menu('eltd-sticky-nav'); ?>
                </div>
            </div>
            <div class="eltd-position-center">
                <div class="eltd-position-center-inner">
                    <?php if(!$hide_logo) {
                        kreate_elated_get_logo('sticky');
                    } ?>
                </div>
            </div>
            <div class="eltd-position-right">
                <div class="eltd-position-right-inner">
                    <?php kreate_elated_get_sticky_right_main_menu('eltd-sticky-nav'); ?>
                    <?php if(is_active_sidebar('eltd-sticky-right')) : ?>
                        <div class="widget">
                        <?php dynamic_sidebar('eltd-sticky-right'); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php do_action('kreate_elated_after_sticky_header'); ?>