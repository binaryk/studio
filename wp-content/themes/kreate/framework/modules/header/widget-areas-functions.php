<?php

if(!function_exists('kreate_elated_header_standard_widget_areas')) {
    /**
     * Registers widget areas for standard header type
     */
    function kreate_elated_header_standard_widget_areas() {
        if(kreate_elated_options()->getOptionValue('header_type') == 'header-standard') {
            register_sidebar(array(
                'name'          => esc_html__('Header Right', 'kreate'),
                'id'            => 'eltd-right-from-main-menu',
                'before_widget' => '<div id="%1$s" class="widget %2$s eltd-right-from-main-menu-widget">',
                'after_widget'  => '</div>',
                'description'   => esc_html__('Widgets added here will appear on the right hand side from the main menu', 'kreate')
            ));
        }
    }

    add_action('widgets_init', 'kreate_elated_header_standard_widget_areas');
}

if(!function_exists('kreate_elated_header_centered_logo_widget_areas')) {
    /**
     * Registers widget areas for centered logo header type
     */
    function kreate_elated_header_centered_logo_widget_areas() {
        if(kreate_elated_options()->getOptionValue('header_type') == 'header-centered-logo') {
            register_sidebar(array(
                'name'          => esc_html__('Header Left', 'kreate'),
                'id'            => 'eltd-left-from-logo',
                'before_widget' => '<div id="%1$s" class="%2$s eltd-left-from-logo-widget">',
                'after_widget'  => '</div>',
                'description'   => esc_html__('Widgets added here will appear on the left hand side from logo', 'kreate')
            ));

            register_sidebar(array(
                'name'          => esc_html__('Header Right', 'kreate'),
                'id'            => 'eltd-right-from-logo',
                'before_widget' => '<div id="%1$s" class="%2$s eltd-right-from-logo-widget">',
                'after_widget'  => '</div>',
                'description'   => esc_html__('Widgets added here will appear on the right hand side from logo', 'kreate')
            ));
        }
    }

    add_action('widgets_init', 'kreate_elated_header_centered_logo_widget_areas');
}

if(!function_exists('kreate_elated_register_sticky_header_areas')) {
    /**
     * Registers widget area for sticky header
     */
    function kreate_elated_register_sticky_header_areas() {
        if(in_array(kreate_elated_options()->getOptionValue('header_behaviour'), array('sticky-header-on-scroll-up','sticky-header-on-scroll-down-up'))) {
            if(kreate_elated_options()->getOptionValue('header_type') == 'header-standard') {
                register_sidebar(array(
                    'name' => esc_html__('Sticky Header Right', 'kreate'),
                    'id' => 'eltd-sticky-right-from-main-menu',
                    'before_widget' => '<div id="%1$s" class="%2$s eltd-sticky-right-from-main-menu-widget">',
                    'after_widget' => '</div>',
                    'description' => esc_html__('Widgets added here will appear on the right hand side in sticky menu', 'kreate')
                ));
            }
            if(kreate_elated_options()->getOptionValue('header_type') == 'header-centered-logo') {
                register_sidebar(array(
                    'name' => esc_html__('Sticky Header Left', 'kreate'),
                    'id' => 'eltd-sticky-left',
                    'before_widget' => '<div id="%1$s" class="%2$s eltd-sticky-left">',
                    'after_widget' => '</div>',
                    'description' => esc_html__('Widgets added here will appear on the left hand side in sticky menu', 'kreate')
                ));
                register_sidebar(array(
                    'name' => esc_html__('Sticky Header Right', 'kreate'),
                    'id' => 'eltd-sticky-right',
                    'before_widget' => '<div id="%1$s" class="%2$s eltd-sticky-right">',
                    'after_widget' => '</div>',
                    'description' => esc_html__('Widgets added here will appear on the right hand side in sticky menu', 'kreate')
                ));
            }
        }
    }

    add_action('widgets_init', 'kreate_elated_register_sticky_header_areas');
}

if(!function_exists('kreate_elated_register_mobile_header_areas')) {
    /**
     * Registers widget areas for mobile header
     */
    function kreate_elated_register_mobile_header_areas() {
        if(kreate_elated_is_responsive_on()) {
            register_sidebar(array(
                'name'          => esc_html__('Mobile Header Right', 'kreate'),
                'id'            => 'eltd-right-from-mobile-logo',
                'before_widget' => '<div id="%1$s" class="%2$s eltd-right-from-mobile-logo">',
                'after_widget'  => '</div>',
                'description'   => esc_html__('Widgets added here will appear on the right hand side from the mobile logo', 'kreate')
            ));
        }
    }

    add_action('widgets_init', 'kreate_elated_register_mobile_header_areas');
}