<?php

if(!function_exists('kreate_elated_header_standard_menu_area_styles')) {
    /**
     * Generates styles for header standard menu
     */
    function kreate_elated_header_standard_menu_area_styles() {
        global $kreate_elated_options;

        $menu_area_header_standard_styles = array();

        if($kreate_elated_options['menu_area_background_color_header_standard'] !== '') {
            $menu_area_background_color        = $kreate_elated_options['menu_area_background_color_header_standard'];
            $menu_area_background_transparency = 1;

            if($kreate_elated_options['menu_area_background_transparency_header_standard'] !== '') {
                $menu_area_background_transparency = $kreate_elated_options['menu_area_background_transparency_header_standard'];
            }

            $menu_area_header_standard_styles['background-color'] = kreate_elated_rgba_color($menu_area_background_color, $menu_area_background_transparency);
        }

        if($kreate_elated_options['menu_area_height_header_standard'] !== '') {
        	$header_standard_height = kreate_elated_filter_px($kreate_elated_options['menu_area_height_header_standard']);
            $max_height = intval($header_standard_height * 0.9).'px';
            echo kreate_elated_dynamic_css('.eltd-header-standard .eltd-page-header .eltd-logo-wrapper a', array('max-height' => $max_height));
            echo kreate_elated_dynamic_css('.eltd-header-standard .eltd-menu-area #lang_sel>ul>li', array('height' => $header_standard_height.'px'));
            echo kreate_elated_dynamic_css('.eltd-header-standard .eltd-menu-area #lang_sel>ul>li', array('line-height' => ($header_standard_height-2).'px'));

            $menu_area_header_standard_styles['height'] = $header_standard_height.'px';

        }

        if($kreate_elated_options['menu_area_border_color_header_standard'] !== ''){
            $menu_area_header_standard_styles['border-color'] = kreate_elated_rgba_color($kreate_elated_options['menu_area_border_color_header_standard'], 0.2);
        }

        echo kreate_elated_dynamic_css('.eltd-header-standard .eltd-page-header .eltd-menu-area', $menu_area_header_standard_styles);

    }

    add_action('kreate_elated_style_dynamic', 'kreate_elated_header_standard_menu_area_styles');
}

if(!function_exists('kreate_elated_header_centered_logo_menu_area_styles')) {
    /**
     * Generates styles for header centered logo menu
     */
    function kreate_elated_header_centered_logo_menu_area_styles() {
        global $kreate_elated_options;

        $menu_area_header_centered_logo_styles = array();

        if($kreate_elated_options['menu_area_background_color_header_centered_logo'] !== '') {
            $menu_area_background_color        = $kreate_elated_options['menu_area_background_color_header_centered_logo'];
            $menu_area_background_transparency = 1;

            if($kreate_elated_options['menu_area_background_transparency_header_centered_logo'] !== '') {
                $menu_area_background_transparency = $kreate_elated_options['menu_area_background_transparency_header_centered_logo'];
            }

            $menu_area_header_centered_logo_styles['background-color'] = kreate_elated_rgba_color($menu_area_background_color, $menu_area_background_transparency);
        }

        if($kreate_elated_options['menu_area_height_header_centered_logo'] !== '') {
        	$header_centered_height = kreate_elated_filter_px($kreate_elated_options['menu_area_height_header_centered_logo']);
            $max_height = intval($header_centered_height * 0.9).'px';
            echo kreate_elated_dynamic_css('.eltd-header-centered-logo .eltd-page-header .eltd-logo-wrapper a', array('max-height' => $max_height));
            echo kreate_elated_dynamic_css('.eltd-header-centered-logo .eltd-menu-area #lang_sel>ul>li', array('height' => $header_centered_height.'px'));
            echo kreate_elated_dynamic_css('.eltd-header-centered-logo .eltd-menu-area #lang_sel>ul>li', array('line-height' => ($header_centered_height-2).'px'));

            $menu_area_header_centered_logo_styles['height'] = $header_centered_height.'px';

        }

        if($kreate_elated_options['menu_area_border_color_header_centered_logo'] !== ''){
            $menu_area_header_centered_logo_styles['border-color'] = kreate_elated_rgba_color($kreate_elated_options['menu_area_border_color_header_centered_logo'], 0.2);
        }

        echo kreate_elated_dynamic_css('.eltd-header-centered-logo .eltd-page-header .eltd-menu-area', $menu_area_header_centered_logo_styles);

    }

    add_action('kreate_elated_style_dynamic', 'kreate_elated_header_centered_logo_menu_area_styles');
}



if(!function_exists('kreate_elated_sticky_header_styles')) {
    /**
     * Generates styles for sticky haeder
     */
    function kreate_elated_sticky_header_styles() {
        global $kreate_elated_options;

        if($kreate_elated_options['sticky_header_background_color'] !== '') {

            $sticky_header_background_color              = $kreate_elated_options['sticky_header_background_color'];
            $sticky_header_background_color_transparency = 1;

            if($kreate_elated_options['sticky_header_transparency'] !== '') {
                $sticky_header_background_color_transparency = $kreate_elated_options['sticky_header_transparency'];
            }

            echo kreate_elated_dynamic_css('.eltd-page-header .eltd-sticky-header .eltd-sticky-holder', array('background-color' => kreate_elated_rgba_color($sticky_header_background_color, $sticky_header_background_color_transparency)));
        }

        if($kreate_elated_options['sticky_header_height'] !== '') {
        	$sticky_header_height = kreate_elated_filter_px($kreate_elated_options['sticky_header_height']);
            $max_height = intval($sticky_header_height * 0.9).'px';

            echo kreate_elated_dynamic_css('.eltd-page-header .eltd-sticky-header', array('height' => $sticky_header_height.'px'));
            echo kreate_elated_dynamic_css('.eltd-page-header .eltd-sticky-header .eltd-logo-wrapper a', array('max-height' => $max_height));
            echo kreate_elated_dynamic_css('.eltd-sticky-header #lang_sel>ul>li', array('height' => $sticky_header_height.'px'));
            echo kreate_elated_dynamic_css('.eltd-sticky-header #lang_sel>ul>li', array('line-height' => ($sticky_header_height-2).'px'));
        }

        $sticky_menu_item_styles = array();
        if($kreate_elated_options['sticky_color'] !== '') {
            $sticky_menu_item_styles['color'] = $kreate_elated_options['sticky_color'];
        }
        if($kreate_elated_options['sticky_google_fonts'] !== '-1') {
            $sticky_menu_item_styles['font-family'] = kreate_elated_get_formatted_font_family($kreate_elated_options['sticky_google_fonts']);
        }
        if($kreate_elated_options['sticky_fontsize'] !== '') {
            $sticky_menu_item_styles['font-size'] = $kreate_elated_options['sticky_fontsize'].'px';
        }
        if($kreate_elated_options['sticky_lineheight'] !== '') {
            $sticky_menu_item_styles['line-height'] = $kreate_elated_options['sticky_lineheight'].'px';
        }
        if($kreate_elated_options['sticky_texttransform'] !== '') {
            $sticky_menu_item_styles['text-transform'] = $kreate_elated_options['sticky_texttransform'];
        }
        if($kreate_elated_options['sticky_fontstyle'] !== '') {
            $sticky_menu_item_styles['font-style'] = $kreate_elated_options['sticky_fontstyle'];
        }
        if($kreate_elated_options['sticky_fontweight'] !== '') {
            $sticky_menu_item_styles['font-weight'] = $kreate_elated_options['sticky_fontweight'];
        }
        if($kreate_elated_options['sticky_letterspacing'] !== '') {
            $sticky_menu_item_styles['letter-spacing'] = $kreate_elated_options['sticky_letterspacing'].'px';
        }

        $sticky_menu_item_selector = array(
            '.eltd-main-menu.eltd-sticky-nav > ul > li > a'
        );

        echo kreate_elated_dynamic_css($sticky_menu_item_selector, $sticky_menu_item_styles);


        if($kreate_elated_options['sticky_color']) {
            echo kreate_elated_dynamic_css('.eltd-main-menu.eltd-sticky-nav > ul > li > a span.bottom_line', array('background-color'=>$kreate_elated_options['sticky_color']));
        }

        if($kreate_elated_options['sticky_fontsize']) {
            echo kreate_elated_dynamic_css('.eltd-main-menu.eltd-sticky-nav > ul > li > a span.item_top_title', array('font-size'=>$kreate_elated_options['sticky_fontsize']));
        }


        $sticky_menu_item_hover_styles = array();
        if($kreate_elated_options['sticky_hovercolor'] !== '') {
            $sticky_menu_item_hover_styles['color'] = $kreate_elated_options['sticky_hovercolor'];

            echo kreate_elated_dynamic_css('.eltd-main-menu.eltd-sticky-nav > ul > li > a:hover span.bottom_line, .eltd-main-menu.eltd-sticky-nav > ul > li.eltd-active-item > a span.bottom_line', array('background-color'=>$kreate_elated_options['sticky_hovercolor']));

        }

        $sticky_menu_item_hover_selector = array(
            '.eltd-main-menu.eltd-sticky-nav > ul > li:hover > a',
            '.eltd-main-menu.eltd-sticky-nav > ul > li.eltd-active-item:hover > a',
            'body:not(.eltd-menu-item-first-level-bg-color) .eltd-main-menu.eltd-sticky-nav > ul > li:hover > a',
            'body:not(.eltd-menu-item-first-level-bg-color) .eltd-main-menu.eltd-sticky-nav > ul > li.eltd-active-item:hover > a'
        );

        echo kreate_elated_dynamic_css($sticky_menu_item_hover_selector, $sticky_menu_item_hover_styles);
    }

    add_action('kreate_elated_style_dynamic', 'kreate_elated_sticky_header_styles');
}

if(!function_exists('kreate_elated_main_menu_styles')) {
    /**
     * Generates styles for main menu
     */
    function kreate_elated_main_menu_styles() {
        global $kreate_elated_options;

        if($kreate_elated_options['menu_color'] !== '' || $kreate_elated_options['menu_fontsize'] != '' || $kreate_elated_options['menu_fontstyle'] !== '' || $kreate_elated_options['menu_fontweight'] !== '' || $kreate_elated_options['menu_texttransform'] !== '' || $kreate_elated_options['menu_letterspacing'] !== '' || $kreate_elated_options['menu_google_fonts'] != "-1") { ?>
            .eltd-main-menu.eltd-default-nav > ul > li > a,
            .eltd-page-header #lang_sel > ul > li > a,
            .eltd-page-header #lang_sel_click > ul > li > a,
            .eltd-page-header #lang_sel ul > li:hover > a{
            <?php if($kreate_elated_options['menu_color']) { ?> color: <?php echo esc_attr($kreate_elated_options['menu_color']); ?>; <?php } ?>
            <?php if($kreate_elated_options['menu_google_fonts'] != "-1") { ?>
                font-family: '<?php echo esc_attr(str_replace('+', ' ', $kreate_elated_options['menu_google_fonts'])); ?>', sans-serif;
            <?php } ?>
            <?php if($kreate_elated_options['menu_fontsize'] !== '') { ?> font-size: <?php echo esc_attr($kreate_elated_options['menu_fontsize']); ?>px; <?php } ?>
            <?php if($kreate_elated_options['menu_fontstyle'] !== '') { ?> font-style: <?php echo esc_attr($kreate_elated_options['menu_fontstyle']); ?>; <?php } ?>
            <?php if($kreate_elated_options['menu_fontweight'] !== '') { ?> font-weight: <?php echo esc_attr($kreate_elated_options['menu_fontweight']); ?>; <?php } ?>
            <?php if($kreate_elated_options['menu_texttransform'] !== '') { ?> text-transform: <?php echo esc_attr($kreate_elated_options['menu_texttransform']); ?>;  <?php } ?>
            <?php if($kreate_elated_options['menu_letterspacing'] !== '') { ?> letter-spacing: <?php echo esc_attr($kreate_elated_options['menu_letterspacing']); ?>px; <?php } ?>
            }

            <?php if($kreate_elated_options['menu_color']) { ?>
                .eltd-main-menu.eltd-default-nav > ul > li > a span.bottom_line{
                    background-color: <?php echo esc_attr($kreate_elated_options['menu_color']); ?>;
                }
            <?php } ?>

            <?php if($kreate_elated_options['menu_fontsize'] !== '') { ?>
                .eltd-main-menu.eltd-default-nav > ul > li > a span.item_top_title{
                    font-size: <?php echo esc_attr($kreate_elated_options['menu_fontsize']-2); ?>px;
                }
            <?php } ?>
        <?php } ?>

        <?php if($kreate_elated_options['menu_google_fonts'] != "-1") { ?>
            .eltd-page-header #lang_sel_list{
            font-family: '<?php echo esc_attr(str_replace('+', ' ', $kreate_elated_options['menu_google_fonts'])); ?>', sans-serif !important;
            }
        <?php } ?>

        <?php if($kreate_elated_options['menu_hovercolor'] !== '') { ?>
            .eltd-main-menu.eltd-default-nav > ul > li:hover > a,
            .eltd-main-menu.eltd-default-nav > ul > li.eltd-active-item:hover > a,
            body:not(.eltd-menu-item-first-level-bg-color) .eltd-main-menu.eltd-default-nav > ul > li:hover > a,
            body:not(.eltd-menu-item-first-level-bg-color) .eltd-main-menu.eltd-default-nav > ul > li.eltd-active-item:hover > a,
            .eltd-page-header #lang_sel ul li a:hover,
            .eltd-page-header #lang_sel_click > ul > li a:hover{
            color: <?php echo esc_attr($kreate_elated_options['menu_hovercolor']); ?> !important;
            }

            .eltd-main-menu.eltd-default-nav > ul > li > a:hover span.bottom_line{
            background-color: <?php echo esc_attr($kreate_elated_options['menu_hovercolor']); ?>;
            }
        <?php } ?>

        <?php if($kreate_elated_options['menu_activecolor'] !== '') { ?>
            .eltd-main-menu.eltd-default-nav > ul > li.eltd-active-item > a,
            body:not(.eltd-menu-item-first-level-bg-color) .eltd-main-menu.eltd-default-nav > ul > li.eltd-active-item > a{
            color: <?php echo esc_attr($kreate_elated_options['menu_activecolor']); ?>;
            }

            .eltd-main-menu.eltd-default-nav > ul > li.eltd-active-item > a span.bottom_line{
            background-color: <?php echo esc_attr($kreate_elated_options['menu_activecolor']); ?>;
            }
        <?php } ?>

        <?php if($kreate_elated_options['menu_lineheight'] != "" || $kreate_elated_options['menu_padding_left_right'] !== '') { ?>
            .eltd-main-menu.eltd-default-nav > ul > li > a span.item_inner{
            <?php if($kreate_elated_options['menu_lineheight'] !== '') { ?> line-height: <?php echo esc_attr($kreate_elated_options['menu_lineheight']); ?>px; <?php } ?>
            <?php if($kreate_elated_options['menu_padding_left_right']) { ?> padding: 0  <?php echo esc_attr($kreate_elated_options['menu_padding_left_right']); ?>px; <?php } ?>
            }
        <?php } ?>

        <?php if($kreate_elated_options['menu_margin_left_right'] !== '') { ?>
            .eltd-main-menu.eltd-default-nav > ul > li{
            margin: 0  <?php echo esc_attr($kreate_elated_options['menu_margin_left_right']); ?>px;
            }
        <?php } ?>

        <?php
        if($kreate_elated_options['dropdown_background_color'] != "" || $kreate_elated_options['dropdown_background_transparency'] != "") {

            //dropdown background and transparency styles
            $dropdown_bg_color_initial        = '#ffffff';
            $dropdown_bg_transparency_initial = 1;

            $dropdown_bg_color        = $kreate_elated_options['dropdown_background_color'] !== "" ? $kreate_elated_options['dropdown_background_color'] : $dropdown_bg_color_initial;
            $dropdown_bg_transparency = $kreate_elated_options['dropdown_background_transparency'] !== "" ? $kreate_elated_options['dropdown_background_transparency'] : $dropdown_bg_transparency_initial;

            $dropdown_bg_color_rgb = kreate_elated_hex2rgb($dropdown_bg_color);

            ?>

            .eltd-drop-down .second .inner ul,
            .eltd-drop-down .second .inner ul li ul,
            .shopping_cart_dropdown,
            li.narrow .second .inner ul,
            .eltd-main-menu.eltd-default-nav #lang_sel ul ul,
            .eltd-main-menu.eltd-default-nav #lang_sel_click  ul ul,
            .header-widget.widget_nav_menu ul ul,
            .eltd-drop-down .wide.wide_background .second{
            background-color: <?php echo esc_attr($dropdown_bg_color); ?>;
            background-color: rgba(<?php echo esc_attr($dropdown_bg_color_rgb[0]); ?>,<?php echo esc_attr($dropdown_bg_color_rgb[1]); ?>,<?php echo esc_attr($dropdown_bg_color_rgb[2]); ?>,<?php echo esc_attr($dropdown_bg_transparency); ?>);
            }

        <?php } //end dropdown background and transparency styles ?>

        <?php if($kreate_elated_options['dropdown_top_separator_color'] !== '') { ?>
            .eltd-drop-down .second{
            border-top-color: <?php echo esc_attr($kreate_elated_options['dropdown_top_separator_color']); ?>;
            }
        <?php } ?>

        <?php
        if($kreate_elated_options['dropdown_top_padding'] !== '' || $kreate_elated_options['dropdown_border_around'] == "no") {

            $menu_inner_ul_top = 10; //default value without border
            if($kreate_elated_options['dropdown_top_padding'] !== '') {
                ?>
                li.narrow .second .inner ul,
                .eltd-drop-down .wide .second .inner > ul{
                padding-top: <?php echo esc_attr($kreate_elated_options['dropdown_top_padding']); ?>px;
                }
                <?php
                $menu_inner_ul_top = $kreate_elated_options['dropdown_top_padding']; //overwrite default value
            } ?>
            <?php if($kreate_elated_options['dropdown_border_around'] == "yes") {
                $menu_inner_ul_top += 1; //top border is 1px
            }
            ?>
            .eltd-drop-down .narrow .second .inner ul li ul,
            body.eltd-slide-from-bottom .eltd-drop-down .narrow .second .inner ul li:hover ul,
            body.eltd-slide-from-top .narrow .second .inner ul li:hover ul{
            top:-<?php echo esc_attr($menu_inner_ul_top); ?>px;
            }

        <?php } ?>

        <?php if($kreate_elated_options['dropdown_bottom_padding'] !== '') { ?>
            li.narrow .second .inner ul,
            .eltd-drop-down .wide .second .inner > ul{
            padding-bottom: <?php echo esc_attr($kreate_elated_options['dropdown_bottom_padding']); ?>px;
            }
        <?php } ?>

        <?php if($kreate_elated_options['enable_dropdown_top_separator'] == "no") { ?>
            .eltd-drop-down .second{
            border-top: 0 !important;
            }
        <?php } ?>

        <?php
        $dropdown_separator_full_width = 'no';
        if($kreate_elated_options['enable_dropdown_separator_full_width'] == "yes") {
            $dropdown_separator_full_width = $kreate_elated_options['enable_dropdown_separator_full_width']; ?>
            .eltd-drop-down .second .inner > ul > li:last-child > a,
            .eltd-drop-down .second .inner > ul > li > ul > li:last-child > a,
            .eltd-drop-down .second .inner > ul > li > ul > li > ul > li:last-child > a{
            border-bottom:1px solid transparent;
            }

        <?php }
        if($dropdown_separator_full_width !== 'yes') {
            ?>
            .eltd-drop-down .second .inner ul li a,
            .header-widget.widget_nav_menu ul.menu li ul li a {
            border-color: <?php echo esc_attr($kreate_elated_options['dropdown_separator_color']); ?>;
            }
        <?php } ?>
        <?php
        if($dropdown_separator_full_width == 'yes') {
            ?>
            .eltd-drop-down .second .inner ul li,
            .header-widget.widget_nav_menu ul.menu li ul li{
            border-bottom: 1px solid <?php echo esc_attr($kreate_elated_options['dropdown_separator_color']); ?>;
            }
        <?php } ?>

        <?php if(!empty($kreate_elated_options['dropdown_vertical_separator_color'])) { ?>
            .eltd-drop-down .wide .second ul li{
            border-left-color: <?php echo esc_attr($kreate_elated_options['dropdown_vertical_separator_color']); ?>;
            }
        <?php } ?>


        <?php if($kreate_elated_options['dropdown_top_position'] !== '') { ?>
            header .eltd-drop-down .second {
            top: <?php echo esc_attr($kreate_elated_options['dropdown_top_position']).'%;'; ?>
            }
        <?php } ?>


        <?php if($kreate_elated_options['dropdown_border_around'] == "yes" && $kreate_elated_options['dropdown_border_around_color'] !== '') { ?>
            .eltd-drop-down .second .inner > ul,
            li.narrow .second .inner ul,
            .eltd-drop-down .narrow .second .inner ul li ul,
            .shopping_cart_dropdown,
            .shopping_cart_dropdown ul li{
            border-color:  <?php echo esc_attr($kreate_elated_options['dropdown_border_around_color']); ?>;
            }

        <?php } ?>

        <?php if($kreate_elated_options['dropdown_border_around'] == "no") { ?>
            .eltd-drop-down .second .inner>ul,
            li.narrow .second .inner ul,
            .eltd-drop-down .narrow .second .inner ul li ul{
            border: none;
            }

            .eltd-drop-down .second .inner ul.right li ul{
            margin-left: 0;
            }

        <?php } ?>

        <?php if($kreate_elated_options['dropdown_color'] !== '' || $kreate_elated_options['dropdown_fontsize'] !== '' || $kreate_elated_options['dropdown_lineheight'] !== '' || $kreate_elated_options['dropdown_fontstyle'] !== '' || $kreate_elated_options['dropdown_fontweight'] !== '' || $kreate_elated_options['dropdown_google_fonts'] != "-1" || $kreate_elated_options['dropdown_texttransform'] !== '' || $kreate_elated_options['dropdown_letterspacing'] !== '') { ?>
            .eltd-drop-down .second .inner > ul > li > a,
            .eltd-drop-down .second .inner > ul > li > h4,
            .eltd-drop-down .wide .second .inner > ul > li > h4,
            .eltd-drop-down .wide .second .inner > ul > li > a,
            .eltd-drop-down .wide .second ul li ul li.menu-item-has-children > a,
            .eltd-drop-down .wide .second .inner ul li.sub ul li.menu-item-has-children > a,
            .eltd-drop-down .wide .second .inner > ul li.sub .flexslider ul li  h4 a,
            .eltd-drop-down .wide .second .inner > ul li .flexslider ul li  h4 a,
            .eltd-drop-down .wide .second .inner > ul li.sub .flexslider ul li  h4,
            .eltd-drop-down .wide .second .inner > ul li .flexslider ul li  h4,
            .eltd-main-menu.eltd-default-nav #lang_sel ul li li a,
            .eltd-main-menu.eltd-default-nav #lang_sel_click ul li ul li a,
            .eltd-main-menu.eltd-default-nav #lang_sel ul ul a,
            .eltd-main-menu.eltd-default-nav #lang_sel_click ul ul a{
            <?php if(!empty($kreate_elated_options['dropdown_color'])) { ?> color: <?php echo esc_attr($kreate_elated_options['dropdown_color']); ?>; <?php } ?>
            <?php if($kreate_elated_options['dropdown_google_fonts'] != "-1") { ?>
                font-family: '<?php echo esc_attr(str_replace('+', ' ', $kreate_elated_options['dropdown_google_fonts'])); ?>', sans-serif !important;
            <?php } ?>
            <?php if($kreate_elated_options['dropdown_fontsize'] !== '') { ?> font-size: <?php echo esc_attr($kreate_elated_options['dropdown_fontsize']); ?>px; <?php } ?>
            <?php if($kreate_elated_options['dropdown_lineheight'] !== '') { ?> line-height: <?php echo esc_attr($kreate_elated_options['dropdown_lineheight']); ?>px; <?php } ?>
            <?php if($kreate_elated_options['dropdown_fontstyle'] !== '') { ?> font-style: <?php echo esc_attr($kreate_elated_options['dropdown_fontstyle']); ?>;  <?php } ?>
            <?php if($kreate_elated_options['dropdown_fontweight'] !== '') { ?>font-weight: <?php echo esc_attr($kreate_elated_options['dropdown_fontweight']); ?>; <?php } ?>
            <?php if($kreate_elated_options['dropdown_texttransform'] !== '') { ?> text-transform: <?php echo esc_attr($kreate_elated_options['dropdown_texttransform']); ?>;  <?php } ?>
            <?php if($kreate_elated_options['dropdown_letterspacing'] !== '') { ?> letter-spacing: <?php echo esc_attr($kreate_elated_options['dropdown_letterspacing']); ?>px;  <?php } ?>
            }
        <?php } ?>

        <?php if($kreate_elated_options['dropdown_color'] !== '') { ?>
            .shopping_cart_dropdown ul li
            .item_info_holder .item_left a,
            .shopping_cart_dropdown ul li .item_info_holder .item_right .amount,
            .shopping_cart_dropdown .cart_bottom .subtotal_holder .total,
            .shopping_cart_dropdown .cart_bottom .subtotal_holder .total_amount{
            color: <?php echo esc_attr($kreate_elated_options['dropdown_color']); ?>;
            }
        <?php } ?>

        <?php if(!empty($kreate_elated_options['dropdown_hovercolor'])) { ?>
            .eltd-drop-down .second .inner > ul > li:hover > a,
            .eltd-drop-down .wide .second ul li ul li.menu-item-has-children:hover > a,
            .eltd-drop-down .wide .second .inner ul li.sub ul li.menu-item-has-children:hover > a,
            .eltd-main-menu.eltd-default-nav #lang_sel ul li li:hover a,
            .eltd-main-menu.eltd-default-nav #lang_sel_click ul li ul li:hover a,
            .eltd-main-menu.eltd-default-nav #lang_sel ul li:hover > a,
            .eltd-main-menu.eltd-default-nav #lang_sel_click ul li:hover > a{
            color: <?php echo esc_attr($kreate_elated_options['dropdown_hovercolor']); ?> !important;
            }
        <?php } ?>

        <?php if(!empty($kreate_elated_options['dropdown_background_hovercolor'])) { ?>
            .eltd-drop-down li:not(.wide) .second .inner > ul > li:hover{
            background-color: <?php echo esc_attr($kreate_elated_options['dropdown_background_hovercolor']); ?>;
            }
        <?php } ?>

        <?php if(!empty($kreate_elated_options['dropdown_padding_top_bottom'])) { ?>
            .eltd-drop-down .wide .second>.inner>ul>li.sub>ul>li>a,
            .eltd-drop-down .second .inner ul li a,
            .eltd-drop-down .wide .second ul li a,
            .eltd-drop-down .second .inner ul.right li a{
            padding-top: <?php echo esc_attr($kreate_elated_options['dropdown_padding_top_bottom']); ?>px;
            padding-bottom: <?php echo esc_attr($kreate_elated_options['dropdown_padding_top_bottom']); ?>px;
            }
        <?php } ?>

        <?php if($kreate_elated_options['dropdown_wide_color'] !== '' || $kreate_elated_options['dropdown_wide_fontsize'] !== '' || $kreate_elated_options['dropdown_wide_lineheight'] !== '' || $kreate_elated_options['dropdown_wide_fontstyle'] !== '' || $kreate_elated_options['dropdown_wide_fontweight'] !== '' || $kreate_elated_options['dropdown_wide_google_fonts'] !== "-1" || $kreate_elated_options['dropdown_wide_texttransform'] !== '' || $kreate_elated_options['dropdown_wide_letterspacing'] !== '') { ?>
            .eltd-drop-down .wide .second .inner > ul > li > a{
            <?php if($kreate_elated_options['dropdown_wide_color'] !== '') { ?> color: <?php echo esc_attr($kreate_elated_options['dropdown_wide_color']); ?>; <?php } ?>
            <?php if($kreate_elated_options['dropdown_wide_google_fonts'] != "-1") { ?>
                font-family: '<?php echo esc_attr(str_replace('+', ' ', $kreate_elated_options['dropdown_wide_google_fonts'])); ?>', sans-serif !important;
            <?php } ?>
            <?php if($kreate_elated_options['dropdown_wide_fontsize'] !== '') { ?> font-size: <?php echo esc_attr($kreate_elated_options['dropdown_wide_fontsize']); ?>px; <?php } ?>
            <?php if($kreate_elated_options['dropdown_wide_lineheight'] !== '') { ?> line-height: <?php echo esc_attr($kreate_elated_options['dropdown_wide_lineheight']); ?>px; <?php } ?>
            <?php if($kreate_elated_options['dropdown_wide_fontstyle'] !== '') { ?> font-style: <?php echo esc_attr($kreate_elated_options['dropdown_wide_fontstyle']); ?>;  <?php } ?>
            <?php if($kreate_elated_options['dropdown_wide_fontweight'] !== '') { ?>font-weight: <?php echo esc_attr($kreate_elated_options['dropdown_wide_fontweight']); ?>; <?php } ?>
            <?php if($kreate_elated_options['dropdown_wide_texttransform'] !== '') { ?> text-transform: <?php echo esc_attr($kreate_elated_options['dropdown_wide_texttransform']); ?>;  <?php } ?>
            <?php if($kreate_elated_options['dropdown_wide_letterspacing'] !== '') { ?> letter-spacing: <?php echo esc_attr($kreate_elated_options['dropdown_wide_letterspacing']); ?>px;  <?php } ?>
            }
        <?php } ?>

        <?php if($kreate_elated_options['dropdown_wide_hovercolor'] !== '') { ?>
            .eltd-drop-down .wide .second .inner > ul > li:hover > a {
            color: <?php echo esc_attr($kreate_elated_options['dropdown_wide_hovercolor']); ?> !important;
            }
        <?php } ?>

        <?php if(!empty($kreate_elated_options['dropdown_wide_background_hovercolor'])) { ?>
            .eltd-drop-down .wide .second .inner > ul > li:hover > a{
            background-color: <?php echo esc_attr($kreate_elated_options['dropdown_wide_background_hovercolor']); ?>
            }
        <?php } ?>

        <?php if($kreate_elated_options['dropdown_wide_padding_top_bottom'] !== '') { ?>
            .eltd-drop-down .wide .second>.inner > ul > li.sub > ul > li > a,
            .eltd-drop-down .wide .second .inner ul li a,
            .eltd-drop-down .wide .second ul li a,
            .eltd-drop-down .wide .second .inner ul.right li a{
            padding-top: <?php echo esc_attr($kreate_elated_options['dropdown_wide_padding_top_bottom']); ?>px;
            padding-bottom: <?php echo esc_attr($kreate_elated_options['dropdown_wide_padding_top_bottom']); ?>px;
            }
        <?php } ?>

        <?php if($kreate_elated_options['dropdown_color_thirdlvl'] !== '' || $kreate_elated_options['dropdown_fontsize_thirdlvl'] !== '' || $kreate_elated_options['dropdown_lineheight_thirdlvl'] !== '' || $kreate_elated_options['dropdown_fontstyle_thirdlvl'] !== '' || $kreate_elated_options['dropdown_fontweight_thirdlvl'] !== '' || $kreate_elated_options['dropdown_google_fonts_thirdlvl'] != "-1" || $kreate_elated_options['dropdown_texttransform_thirdlvl'] !== '' || $kreate_elated_options['dropdown_letterspacing_thirdlvl'] !== '') { ?>
            .eltd-drop-down .second .inner ul li.sub ul li a{
            <?php if($kreate_elated_options['dropdown_color_thirdlvl'] !== '') { ?> color: <?php echo esc_attr($kreate_elated_options['dropdown_color_thirdlvl']); ?>;  <?php } ?>
            <?php if($kreate_elated_options['dropdown_google_fonts_thirdlvl'] != "-1") { ?>
                font-family: '<?php echo esc_attr(str_replace('+', ' ', $kreate_elated_options['dropdown_google_fonts_thirdlvl'])); ?>', sans-serif;
            <?php } ?>
            <?php if($kreate_elated_options['dropdown_fontsize_thirdlvl'] !== '') { ?> font-size: <?php echo esc_attr($kreate_elated_options['dropdown_fontsize_thirdlvl']); ?>px;  <?php } ?>
            <?php if($kreate_elated_options['dropdown_lineheight_thirdlvl'] !== '') { ?> line-height: <?php echo esc_attr($kreate_elated_options['dropdown_lineheight_thirdlvl']); ?>px;  <?php } ?>
            <?php if($kreate_elated_options['dropdown_fontstyle_thirdlvl'] !== '') { ?> font-style: <?php echo esc_attr($kreate_elated_options['dropdown_fontstyle_thirdlvl']); ?>;   <?php } ?>
            <?php if($kreate_elated_options['dropdown_fontweight_thirdlvl'] !== '') { ?> font-weight: <?php echo esc_attr($kreate_elated_options['dropdown_fontweight_thirdlvl']); ?>;  <?php } ?>
            <?php if($kreate_elated_options['dropdown_texttransform_thirdlvl'] !== '') { ?> text-transform: <?php echo esc_attr($kreate_elated_options['dropdown_texttransform_thirdlvl']); ?>;  <?php } ?>
            <?php if($kreate_elated_options['dropdown_letterspacing_thirdlvl'] !== '') { ?> letter-spacing: <?php echo esc_attr($kreate_elated_options['dropdown_letterspacing_thirdlvl']); ?>px;  <?php } ?>
            }
        <?php } ?>
        <?php if($kreate_elated_options['dropdown_hovercolor_thirdlvl'] !== '') { ?>
            .eltd-drop-down .second .inner ul li.sub ul li:not(.flex-active-slide):hover > a:not(.flex-prev):not(.flex-next),
            .eltd-drop-down .second .inner ul li ul li:not(.flex-active-slide):hover > a:not(.flex-prev):not(.flex-next){
            color: <?php echo esc_attr($kreate_elated_options['dropdown_hovercolor_thirdlvl']); ?> !important;
            }
        <?php } ?>

        <?php if($kreate_elated_options['dropdown_background_hovercolor_thirdlvl'] !== '') { ?>
            .eltd-drop-down .second .inner ul li.sub ul li:hover,
            .eltd-drop-down .second .inner ul li ul li:hover{
            background-color: <?php echo esc_attr($kreate_elated_options['dropdown_background_hovercolor_thirdlvl']); ?>;
            }
        <?php } ?>

        <?php if($kreate_elated_options['dropdown_wide_color_thirdlvl'] !== '' || $kreate_elated_options['dropdown_wide_fontsize_thirdlvl'] !== '' || $kreate_elated_options['dropdown_wide_lineheight_thirdlvl'] !== '' || $kreate_elated_options['dropdown_wide_fontstyle_thirdlvl'] !== '' || $kreate_elated_options['dropdown_wide_fontweight_thirdlvl'] !== '' || $kreate_elated_options['dropdown_wide_google_fonts_thirdlvl'] != "-1" || $kreate_elated_options['dropdown_wide_texttransform_thirdlvl'] !== '' || $kreate_elated_options['dropdown_wide_letterspacing_thirdlvl'] !== '') { ?>
            .eltd-drop-down .wide .second .inner ul li.sub ul li a,
            .eltd-drop-down .wide .second ul li ul li a{
            <?php if($kreate_elated_options['dropdown_wide_color_thirdlvl'] !== '') { ?> color: <?php echo esc_attr($kreate_elated_options['dropdown_wide_color_thirdlvl']); ?>;  <?php } ?>
            <?php if($kreate_elated_options['dropdown_wide_google_fonts_thirdlvl'] != "-1") { ?>
                font-family: '<?php echo esc_attr(str_replace('+', ' ', $kreate_elated_options['dropdown_wide_google_fonts_thirdlvl'])); ?>', sans-serif;
            <?php } ?>
            <?php if($kreate_elated_options['dropdown_wide_fontsize_thirdlvl'] !== '') { ?> font-size: <?php echo esc_attr($kreate_elated_options['dropdown_wide_fontsize_thirdlvl']); ?>px;  <?php } ?>
            <?php if($kreate_elated_options['dropdown_wide_lineheight_thirdlvl'] !== '') { ?> line-height: <?php echo esc_attr($kreate_elated_options['dropdown_wide_lineheight_thirdlvl']); ?>px;  <?php } ?>
            <?php if($kreate_elated_options['dropdown_wide_fontstyle_thirdlvl'] !== '') { ?> font-style: <?php echo esc_attr($kreate_elated_options['dropdown_wide_fontstyle_thirdlvl']); ?>;   <?php } ?>
            <?php if($kreate_elated_options['dropdown_wide_fontweight_thirdlvl'] !== '') { ?> font-weight: <?php echo esc_attr($kreate_elated_options['dropdown_wide_fontweight_thirdlvl']); ?>;  <?php } ?>
            <?php if($kreate_elated_options['dropdown_wide_texttransform_thirdlvl'] !== '') { ?> text-transform: <?php echo esc_attr($kreate_elated_options['dropdown_wide_texttransform_thirdlvl']); ?>;  <?php } ?>
            <?php if($kreate_elated_options['dropdown_wide_letterspacing_thirdlvl'] !== '') { ?> letter-spacing: <?php echo esc_attr($kreate_elated_options['dropdown_wide_letterspacing_thirdlvl']); ?>px;  <?php } ?>
            }
        <?php } ?>
        <?php if($kreate_elated_options['dropdown_wide_hovercolor_thirdlvl'] !== '') { ?>
            .eltd-drop-down .wide .second .inner ul li.sub ul li:not(.flex-active-slide) > a:not(.flex-prev):not(.flex-next):hover,
            .eltd-drop-down .wide .second .inner ul li ul li:not(.flex-active-slide) > a:not(.flex-prev):not(.flex-next):hover{
            color: <?php echo esc_attr($kreate_elated_options['dropdown_wide_hovercolor_thirdlvl']); ?> !important;
            }
        <?php } ?>

        <?php if($kreate_elated_options['dropdown_wide_background_hovercolor_thirdlvl'] !== '') { ?>
            .eltd-drop-down .wide .second .inner ul li.sub ul li:hover,
            .eltd-drop-down .wide .second .inner ul li ul li:hover{
            background-color: <?php echo esc_attr($kreate_elated_options['dropdown_wide_background_hovercolor_thirdlvl']); ?>;
            }
        <?php }
    }

    add_action('kreate_elated_style_dynamic', 'kreate_elated_main_menu_styles');
}