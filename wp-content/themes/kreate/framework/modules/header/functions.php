<?php

if(!function_exists('kreate_elated_header_register_main_navigation')) {
    /**
     * Registers main navigation
     */
    function kreate_elated_header_register_main_navigation() {

        if(kreate_elated_options()->getOptionValue('header_type') == 'header-standard') {
            register_nav_menus(
                array(
                    'main-navigation' => esc_html__('Main Navigation', 'kreate')
                )
            );
        }
        if(kreate_elated_options()->getOptionValue('header_type') == 'header-centered-logo') {
            register_nav_menus(
                array(
                    'left-main-navigation' => esc_html__('Left Main Navigation', 'kreate')
                )
            );
            register_nav_menus(
                array(
                    'right-main-navigation' => esc_html__('Right Main Navigation', 'kreate')
                )
            );
        }

        register_nav_menus(
            array(
                'mobile-navigation' => esc_html__('Mobile Navigation', 'kreate')
            )
        );

    }

    add_action('after_setup_theme', 'kreate_elated_header_register_main_navigation');
}

if(!function_exists('kreate_elated_get_nav_menu_items_number')) {
    /**
     * Return number of menu items
     * @param $location
     * @return bool
     */
    function kreate_elated_get_nav_menu_items_number($location) {
        if(has_nav_menu($location)) {
            $theme_location  = $location;
            $theme_locations = get_nav_menu_locations();
            $menu_obj        = get_term($theme_locations[$theme_location], 'nav_menu');

            return $menu_obj->count;
        }else{
            return false;
        }
    }

    add_action('after_setup_theme', 'kreate_elated_header_register_main_navigation');
}

if(!function_exists('kreate_elated_get_sticky_header_height')) {
    /**
     * Returns top sticky header height
     *
     * @return bool|int|void
     */
    function kreate_elated_get_sticky_header_height() {
        //sticky menu height, needed only for sticky header on scroll up
        if(kreate_elated_options()->getOptionValue('header_type') !== 'header-vertical' &&
           in_array(kreate_elated_options()->getOptionValue('header_behaviour'), array('sticky-header-on-scroll-up'))) {

            $sticky_header_height = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('sticky_header_height'));

            return $sticky_header_height !== '' ? intval($sticky_header_height) : 60;
        }

        return 0;

    }
}

if(!function_exists('kreate_elated_get_sticky_header_height_of_complete_transparency')) {
    /**
     * Returns top sticky header height it is fully transparent. used in anchor logic
     *
     * @return bool|int|void
     */
    function kreate_elated_get_sticky_header_height_of_complete_transparency() {

        if(kreate_elated_options()->getOptionValue('header_type') !== 'header-vertical') {
            if(kreate_elated_options()->getOptionValue('sticky_header_transparency') === '0') {
                $stickyHeaderTransparent = kreate_elated_options()->getOptionValue('sticky_header_grid_background_color') !== '' &&
                                           kreate_elated_options()->getOptionValue('sticky_header_grid_transparency') === '0';
            } else {
                $stickyHeaderTransparent = kreate_elated_options()->getOptionValue('sticky_header_background_color') !== '' &&
                                           kreate_elated_options()->getOptionValue('sticky_header_transparency') === '0';
            }

            if($stickyHeaderTransparent) {
                return 0;
            } else {
                $sticky_header_height = kreate_elated_filter_px(kreate_elated_options()->getOptionValue('sticky_header_height'));

                return $sticky_header_height !== '' ? intval($sticky_header_height) : 60;
            }
        }
        return 0;
    }
}

if(!function_exists('kreate_elated_get_sticky_scroll_amount')) {
    /**
     * Returns top sticky scroll amount
     *

     * @return bool|int|void
     */
    function kreate_elated_get_sticky_scroll_amount() {

        //sticky menu scroll amount
        if(kreate_elated_options()->getOptionValue('header_type') !== 'header-vertical' &&
           in_array(kreate_elated_options()->getOptionValue('header_behaviour'), array('sticky-header-on-scroll-up','sticky-header-on-scroll-down-up'))) {

            $sticky_scroll_amount = kreate_elated_filter_px(kreate_elated_get_meta_field_intersect('scroll_amount_for_sticky'));

            return $sticky_scroll_amount !== '' ? intval($sticky_scroll_amount) : 0;
        }

        return 0;

    }
}