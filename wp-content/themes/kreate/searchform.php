<form method="get" id="searchform" action="<?php echo esc_url(home_url( '/' )); ?>">
	<div role="search">
		<input type="text" value="" placeholder="<?php esc_html_e('Enter search term', 'kreate'); ?>" name="s" id="s" />
		<input type="submit" class="eltd-search-widget-icon" id="searchsubmit" value="#">
	</div>
</form>