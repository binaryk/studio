<?php $sidebar = kreate_elated_sidebar_layout(); ?>
<?php get_header(); ?>
<?php 
global $wp_query;

if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
else { $paged = 1; }

if(kreate_elated_options()->getOptionValue('blog_page_range') != ""){
	$blog_page_range = esc_attr(kreate_elated_options()->getOptionValue('blog_page_range'));
} else{
	$blog_page_range = $wp_query->max_num_pages;
}
?>
<?php kreate_elated_get_title(); ?>
	<div class="eltd-container">
		<?php do_action('kreate_elated_after_container_open'); ?>
		<div class="eltd-container-inner clearfix">
			<div class="eltd-container">
				<?php do_action('kreate_elated_after_container_open'); ?>
				<div class="eltd-container-inner" >
					<h2 class="eltd-search-for"><?php echo esc_html__('Search results for: ', 'kreate').get_search_query();?></h2>
					<div class="eltd-blog-holder eltd-blog-type-standard">
                        <?php if(have_posts()) : while ( have_posts() ) : the_post();
								kreate_elated_get_post_format_html('standard');
                        endwhile; ?>
                            <?php
                            if(kreate_elated_options()->getOptionValue('pagination') == 'yes') {
                                kreate_elated_pagination($wp_query->max_num_pages, $blog_page_range, $paged);
                            }
                            ?>
                        <?php else: ?>
                            <div class="entry eltd-search-none">
                                <p><?php esc_html_e('No posts were found.', 'kreate'); ?></p>
                            </div>
                        <?php endif; ?>
				</div>
				<?php do_action('kreate_elated_before_container_close'); ?>
			</div>
			</div>
		</div>
		<?php do_action('kreate_elated_before_container_close'); ?>
	</div>
<?php get_footer(); ?>