<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <?php kreate_elated_wp_title(); ?>
    <?php
    /**
     * @see kreate_elated_header_meta() - hooked with 10
     * @see eltd_user_scalable - hooked with 10
     */
    ?>
	<?php do_action('kreate_elated_header_meta'); ?>

	<?php wp_head(); ?>
	<!-- Google Tag Manager -->
	<noscript>
		<iframe src="//www.googletagmanager.com/ns.html?id=GTM-WZK882"
		height="0" width="0" style="display:none;visibility:hidden">
		</iframe>
	</noscript>
	<script>
		(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-WZK882');
	</script>
	<!-- End Google Tag Manager -->
</head>

<body <?php body_class(); ?> <?php echo kreate_elated_get_fixed_image_background(); ?>>

<div class="eltd-wrapper">
    <div class="eltd-wrapper-inner">
        <?php kreate_elated_get_header(); ?>

        <?php if(kreate_elated_options()->getOptionValue('show_back_button') == "yes") { ?>
            <a id='eltd-back-to-top'  href='#'>
                <span class="eltd-back-to-text">
                     <?php
                        echo esc_html_e('Top','kreate');
                    ?>
                </span>
                <span class="eltd-icon-stack">
                     <?php
                        kreate_elated_icon_collections()->getBackToTopIcon('linea_icons');
                    ?>
                </span>
            </a>
        <?php } ?>
        <?php kreate_elated_get_full_screen_menu(); ?>

        <div class="eltd-content" <?php kreate_elated_content_elem_style_attr(); ?>>
            <div class="eltd-content-inner">