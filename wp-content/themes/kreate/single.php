<?php get_header(); ?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<?php kreate_elated_get_title(); ?>
<?php get_template_part('slider'); ?>
	<div class="eltd-container">
		<?php do_action('kreate_elated_after_container_open'); ?>
		<div class="eltd-container-inner">
			<?php kreate_elated_get_blog_single(); ?>
		</div>
		<?php do_action('kreate_elated_before_container_close'); ?>
	</div>
	<?php kreate_elated_get_module_template_part('templates/single/parts/single-navigation', 'blog'); ?>
<?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>