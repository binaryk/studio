<?php global $product; ?>
<li>
	<a href="<?php echo esc_url( get_permalink( $product->id ) ); ?>" title="<?php echo esc_attr( $product->get_title() ); ?>">
		<?php echo wp_kses_post($product->get_image()); ?>
	</a>
	<div class="eltd-product-list-widget-info">
		<h5 class="eltd-product-categories">
			<?php echo wp_kses($product->get_categories(', '), array(
				'a' => array(
					'href' => true,
					'rel' => true,
					'class' => true,
					'title' => true,
					'id' => true
				)
			)); ?>
		</h5>
		<h4 class="product-title">
			<a href="<?php echo esc_url( get_permalink( $product->id ) ); ?>" title="<?php echo esc_attr( $product->get_title() ); ?>">
				<?php echo esc_html($product->get_title()); ?>
			</a>
		</h4>
		<?php if ( ! empty( $show_rating ) ) echo esc_attr($product->get_rating_html()); ?>
		<div class="eltd-product-price">
			<?php echo wp_kses_post($product->get_price_html()); ?>
		</div>
	</div>
</li>