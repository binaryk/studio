<?php
/**
 * Single Product tabs
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$tabs = apply_filters( 'woocommerce_product_tabs', array() );

if ( ! empty( $tabs ) ) : ?>

	<div class="eltd-accordion-holder clearfix eltd-accordion eltd-initial  ui-accordion ui-widget ui-helper-reset">
		<?php foreach ( $tabs as $key => $tab ) : ?>
			<h4 class="clearfix eltd-title-holder">
				<span class="eltd-tab-title">
					<span class="eltd-tab-title-inner">
						<?php echo esc_html( $tab['title'] ); ?>
					</span>
				</span>
				<span class="eltd-accordion-mark">
					<span class="eltd-accordion-mark-icon">
						<span class="icon-arrows-plus"></span>
						<span class="icon-arrows-minus"></span>
					</span>
				</span>
			</h4>
			<div class="eltd-accordion-content">
				<div class="eltd-accordion-content-inner">
					<?php call_user_func( $tab['callback'], $key, $tab ); ?>
				</div>
			</div>
		<?php endforeach; ?>
	</div>

<?php endif; ?>
