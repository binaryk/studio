<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) ) {
	$woocommerce_loop['loop'] = 0;
}

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) ) {
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );
}

// Ensure visibility
if ( ! $product || ! $product->is_visible() ) {
	return;
}

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array();
if ( 0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'] ) {
	$classes[] = 'first';
}
if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] ) {
	$classes[] = 'last';
}

//Get product gallery images and take first for displaying on single product hover
$product_gallery_ids = $product->get_gallery_attachment_ids();
if (!empty($product_gallery_ids)) {
	//get product image url, shop catalog size
	$product_hover_image = wp_get_attachment_image_src( $product_gallery_ids[0], 'shop_catalog' );
}
?>
<li <?php post_class( $classes ); ?>>

	<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>
		<div class="eltd-top-product-section">
			<a href="<?php the_permalink(); ?>" class="eltd-product-link" target="_blank">
				<span class="eltd-image-wrapper">
				<?php
					/**
					 * woocommerce_before_shop_loop_item_title hook
					 *
					 * @hooked woocommerce_show_product_loop_sale_flash - 10
					 * @hooked woocommerce_template_loop_product_thumbnail - 10
					 */
					do_action( 'woocommerce_before_shop_loop_item_title' );
					?>
				</span>
				<?php
				if (isset($product_hover_image)) { ?>

					<span class="eltd-single-product-hover-image" style="background-image: url(<?php echo esc_url( $product_hover_image[0] );?>)">
					</span>

				<?php
				}
				?>
			</a>
			<?php do_action('kreate_elated_woocommerce_hover_image_cart'); ?>
		</div>
		<div class="eltd-product-content clearfix">
			<div class="eltd-info-box">
				<h5 class="eltd-product-categories">
					<?php echo wp_kses($product->get_categories(', '), array(
						'a' => array(
							'href' => true,
							'rel' => true,
							'class' => true,
							'title' => true,
							'id' => true
						)
					)); ?>
				</h5>
				<a href="<?php the_permalink(); ?>" class="eltd-product-title">
					<?php kreate_elated_woocommerce_template_loop_product_title();?>
				</a>
				<?php
				/**
				 * woocommerce_after_shop_loop_item_title hook
				 *
				 * @hooked woocommerce_template_loop_rating - 5
				 */
				remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5); ?>

				<div class="eltd-product-price-holder">
					<?php
					do_action( 'woocommerce_after_shop_loop_item_title' );
					?>
				</div>
			</div>
		</div>

	<?php

		/**
		 * woocommerce_after_shop_loop_item hook
		 *
		 * @hooked woocommerce_template_loop_add_to_cart - 10
		 */
		do_action( 'woocommerce_after_shop_loop_item' );

	?>

</li>
