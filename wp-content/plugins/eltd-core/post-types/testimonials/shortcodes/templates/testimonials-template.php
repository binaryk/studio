<div id="eltd-testimonials<?php echo esc_attr($current_id) ?>" class="eltd-testimonial-content <?php echo esc_attr($has_thumb);?>">
	<div class="eltd-testimonial-content-inner">
		<div class="eltd-testimonial-text-holder">
			<div class="eltd-testimonial-text-inner">
				<h4 class="eltd-testimonial-text"><?php echo trim($text) ?></h4>
				<?php if (has_post_thumbnail($current_id)) { ?>
					<div class="eltd-testimonial-image-holder">
						<?php esc_html(the_post_thumbnail($current_id)) ?>
					</div>
				<?php } ?>
				<?php if ($show_author == "yes") { ?>
					<div class = "eltd-testimonial-author">
						<h5 class="eltd-testimonial-author-text"><?php echo esc_attr($author)?>
							<?php if($show_position == "yes" && $job !== ''){ ?>
								<span class="eltd-testimonials-job"> - <?php echo esc_attr($job)?></span>
							<?php }?>
						</h5>	
					</div>
				<?php } ?>				
			</div>
		</div>
	</div>	
</div>
