<article class="eltd-portfolio-item <?php echo esc_attr($categories)?>" >
	<a class ="eltd-portfolio-link" href="<?php echo esc_url($item_link); ?>"></a>
	<div class = "eltd-item-image-holder">
	<?php
		echo get_the_post_thumbnail(get_the_ID(),$thumb_size);
	?>				
	</div>
	<div class="eltd-item-text-overlay">
		<div class="eltd-item-text-overlay-inner">
			<div class="eltd-item-text-holder">
				<<?php echo esc_attr($title_tag)?> class="eltd-item-title">
					<?php echo esc_attr(get_the_title()); ?>
				</<?php echo esc_attr($title_tag)?>>	
				<?php 
				echo $icon_html; 
				echo $category_html;
				?>
			</div>
		</div>	
	</div>
</article>
