<article class="eltd-portfolio-item <?php echo esc_attr($article_masonry_size)?> <?php echo esc_attr($categories)?>">
	<a class ="eltd-portfolio-link" href="<?php echo esc_url($item_link); ?>"></a>
	<div class = "eltd-item-image-holder">
	<?php
		echo get_the_post_thumbnail(get_the_ID(),$thumb_size);
	?>				
	</div>
	<div class="eltd-item-text-overlay">
		<div class="eltd-item-text-overlay-inner">
			<div class="eltd-item-text-holder">
                <?php
                echo $category_html;
                ?>
                <<?php echo esc_attr($title_tag)?> class="eltd-item-title">
                <?php echo esc_attr(get_the_title()); ?>
                </<?php echo esc_attr($title_tag)?>>
                <?php echo kreate_elated_execute_shortcode('eltd_button',array(
                    'type' => 'arrow',
                    'hover_type' => 'rotate',
                    'text' => esc_html__('View', 'kreate'),
                    'link' => esc_url($item_link)
                ));
                ?>
			</div>
		</div>	
	</div>
</article>
