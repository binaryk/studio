<?php if($query_results->max_num_pages>1){ ?>
	<div class="eltd-ptf-list-paging">
		<span class="eltd-ptf-list-load-more">
			<?php
                echo kreate_elated_execute_shortcode('eltd_icon', array(
                    'icon_pack' => 'linea_icons',
                    'linea_icon' => 'icon-arrows-slim-down',
                    'size' =>'eltd-icon-medium',
                    'shape_size' => '67',
                    'custom_size' => '25',
                    'type' => 'circle',
                    'link' => 'javascript: void(0)'
                ));
			?>
            <p><?php esc_html_e('Load More', 'kreate'); ?></p>
		</span>
	</div>
<?php }