<?php // This line is needed for mixItUp gutter ?>

<article class="eltd-portfolio-item mix <?php echo esc_attr($categories)?>">
	<div class = "eltd-item-image-holder">
		<a href="<?php echo esc_url($item_link); ?>">
			<?php
				echo get_the_post_thumbnail(get_the_ID(),$thumb_size);
			?>				
		</a>
	</div>
	<div class="eltd-item-text-holder">
        <?php
        echo $category_html;
        ?>
        <<?php echo esc_attr($title_tag)?> class="eltd-item-title">
			<a href="<?php echo esc_url($item_link); ?>">
				<?php echo esc_attr(get_the_title()); ?>
			</a>	
		</<?php echo esc_attr($title_tag)?>>
        <?php echo kreate_elated_execute_shortcode('eltd_button',array(
            'type' => 'arrow',
            'hover_type' => 'rotate',
            'arrow_alignment' => 'left',
            'text' => esc_html__('View', 'kreate'),
            'link' => esc_url($item_link)
        ));
        ?>
	</div>
</article>

<?php // This line is needed for mixItUp gutter ?>