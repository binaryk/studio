<?php
namespace ElatedCore\CPT\Portfolio\Shortcodes;

use ElatedCore\Lib;

/**
 * Class PortfolioList
 * @package ElatedCore\CPT\Portfolio\Shortcodes
 */
class PortfolioList implements Lib\ShortcodeInterface {
    /**
     * @var string
     */
    private $base;

    public function __construct() {
        $this->base = 'eltd_portfolio_list';

        add_action('vc_before_init', array($this, 'vcMap'));
    }

    /**
     * Returns base for shortcode
     * @return string
     */
    public function getBase() {
        return $this->base;
    }

    /**
     * Maps shortcode to Visual Composer
     *
     * @see vc_map
     */
    public function vcMap() {
        if(function_exists('vc_map')) {

            $icons_array= array();
            if(eltd_core_theme_installed()) {
                $icons_array = \KreateIconCollections::get_instance()->getVCParamsArray();
            }

            vc_map( array(
                'name' => 'Portfolio List',
                'base' => $this->getBase(),
                'category' => 'by ELATED',
                'icon' => 'icon-wpb-portfolio extended-custom-icon',
                'allowed_container_element' => 'vc_row',
                'params' => array(
						array(
							'type' => 'dropdown',								
							'heading' => 'Portfolio List Template',
							'param_name' => 'type',
							'value' => array(
								'Standard' => 'standard',
								'Gallery' => 'gallery',
								'Masonry' => 'masonry'
							),
							'admin_label' => true,
							'description' => ''
						),
//						array(
//							'type' => 'dropdown',
//							'heading' => 'Title Tag',
//							'param_name' => 'title_tag',
//							'value' => array(
//								''   => '',
//								'h2' => 'h2',
//								'h3' => 'h3',
//								'h4' => 'h4',
//								'h5' => 'h5',
//								'h6' => 'h6',
//							),
//							'admin_label' => true,
//							'description' => ''
//						),
                        array(
                            'type' => 'dropdown',
                            'heading' => 'Show Additional Info',
                            'param_name' => 'show_additional_info',
                            'value' => array(
                                'No' => 'no',
                                'Yes' => 'yes'
                            ),
                            'save_always' => true,
                            'admin_label' => true
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => 'Title',
                            'param_name' => 'info_title',
                            'value' => '',
                            'admin_label' => true,
                            'description' => '',
                            'dependency' => array('element' => 'show_additional_info', 'value' => array('yes'))
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => 'Text',
                            'param_name' => 'info_text',
                            'value' => '',
                            'admin_label' => true,
                            'description' => '',
                            'dependency' => array('element' => 'show_additional_info', 'value' => array('yes'))
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => 'Link',
                            'param_name' => 'info_link',
                            'value' => '',
                            'admin_label' => true,
                            'description' => '',
                            'dependency' => array('element' => 'show_additional_info', 'value' => array('yes'))
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => 'Link Label',
                            'param_name' => 'info_link_label',
                            'value' => '',
                            'admin_label' => true,
                            'description' => '',
                            'dependency' => array('element' => 'show_additional_info', 'value' => array('yes'))
                        ),
						array(
							'type' => 'dropdown',
							'heading' => 'Image Proportions',
							'param_name' => 'image_size',
							'value' => array(
								'Original' => 'full',
								'Square' => 'square',
								'Landscape' => 'landscape',
								'Portrait' => 'portrait'
							),
							'save_always' => true,
							'admin_label' => true,
							'description' => '',
							'dependency' => array('element' => 'type', 'value' => array('standard', 'gallery'))
						),
						array(
							'type' => 'dropdown',
							'heading' => 'Show Load More',
							'param_name' => 'show_load_more',
							'value' => array(
								'Yes' => 'yes',
								'No' => 'no'
							),
							'save_always' => true,
							'admin_label' => true,
							'description' => 'Default value is Yes'
						),
						array(
							'type' => 'dropdown',
							'heading' => 'Order By',
							'param_name' => 'order_by',
							'value' => array(
								'Menu Order' => 'menu_order',
								'Title' => 'title',
								'Date' => 'date'
							),
							'admin_label' => true,
							'save_always' => true,
							'description' => '',
							'group' => 'Query and Layout Options'
						),
						array(
							'type' => 'dropdown',
							'heading' => 'Order',
							'param_name' => 'order',
							'value' => array(
								'ASC' => 'ASC',
								'DESC' => 'DESC',
							),
							'admin_label' => true,
							'save_always' => true,
							'description' => '',
							'group' => 'Query and Layout Options'
						),
						array(
							'type' => 'textfield',
							'heading' => 'One-Category Portfolio List',
							'param_name' => 'category',
							'value' => '',
							'admin_label' => true,
							'description' => 'Enter one category slug (leave empty for showing all categories)',
							'group' => 'Query and Layout Options'
						),
						 array(
							'type' => 'textfield',
							'heading' => 'Number of Portfolios Per Page',
							'param_name' => 'number',
							'value' => '-1',
							'admin_label' => true,
							'description' => '(enter -1 to show all)',
							'group' => 'Query and Layout Options'
						),
						array(
							'type' => 'dropdown',
							'heading' => 'Number of Columns',
							'param_name' => 'columns',
							'value' => array(
								'' => '',
								'One' => 'one',
								'Two' => 'two',
								'Three' => 'three',
								'Four' => 'four',
								'Five' => 'five',
								'Six' => 'six'
							),
							'admin_label' => true,
							'description' => 'Default value is Three',
							'dependency' => array('element' => 'type', 'value' => array('standard','gallery')),
							'group' => 'Query and Layout Options'
						),
                        array(
                            'type' => 'dropdown',
                            'heading' => 'Enable space between portfolio items',
                            'param_name' => 'space_between',
                            'value' => array(
                                'No' => 'no',
                                'Yes' => 'yes'
                            ),
                            'admin_label' => true,
                            'save_always' => true,
                            'description' => 'Enable this option to display a predefined spacing between portfolio items in the list',
                            'dependency' => array('element' => 'type', 'value' => array('standard','gallery')),
                            'group' => 'Query and Layout Options'
                        ),
						array(
							'type' => 'dropdown',
							'heading' => 'Grid Size',
							'param_name' => 'grid_size',								
							'value' => array(
								'Default' => '',
								'3 Columns Grid' => 'three',
								'4 Columns Grid' => 'four',
								'5 Columns Grid' => 'five'
							),
							'admin_label' => true,
							'description' => 'This option is only for Full Width Page Template',
							'dependency' => array('element' => 'type', 'value' => array('pinterest')),
							'group' => 'Query and Layout Options'
						),
						array(
							'type' => 'textfield',
							'heading' => 'Show Only Projects with Listed IDs',
							'param_name' => 'selected_projects',
							'value' => '',
							'admin_label' => true,
							'description' => 'Delimit ID numbers by comma (leave empty for all)',
							'group' => 'Query and Layout Options'
						),
						array(
							'type' => 'dropdown',
							'heading' => 'Enable Category Filter',
							'param_name' => 'filter',
							'value' => array(
								'No' => 'no',
								'Yes' => 'yes'                               
							),
							'admin_label' => true,
							'save_always' => true,
							'description' => 'Default value is No',
							'group' => 'Query and Layout Options',
                            'dependency' => array('element' => 'show_additional_info', 'value' => array('no'))
						),
                        array(
                            'type' => 'textfield',
                            'heading' => 'Filter Bottom Margin (px)',
                            'param_name' => 'filter_bottom_margin',
                            'value' => '',
                            'admin_label' => true,
                            'description' => '',
                            'dependency' => array('element' => 'filter', 'value' => array('yes')),
                            'group' => 'Query and Layout Options'
                        ),
                        array(
                            'type' => 'dropdown',
                            'heading' => 'Filter Skin',
                            'param_name' => 'filter_skin',
                            'value' => array(
                                'Light'  => 'light',
                                'Dark' => 'dark'
                            ),
                            'admin_label' => true,
                            'save_always' => true,
                            'dependency' => array('element' => 'filter', 'value' => array('yes')),
                            'group' => 'Query and Layout Options'
                        ),
						array(
							'type' => 'dropdown',
							'heading' => 'Filter Order By',
							'param_name' => 'filter_order_by',
							'value' => array(
								'Name'  => 'name',
								'Count' => 'count',
								'Id'    => 'id',
								'Slug'  => 'slug'
							),
							'admin_label' => true,
							'save_always' => true,
							'description' => 'Default value is Name',
							'dependency' => array('element' => 'filter', 'value' => array('yes')),
							'group' => 'Query and Layout Options'
						),
                        array(
                            'type' => 'dropdown',
                            'heading' => 'Filter Content Alignment',
                            'param_name' => 'filter_content_alignment',
                            'value' => array(
                                'Left'  => 'left',
                                'Center' => 'center'
                            ),
                            'admin_label' => true,
                            'save_always' => true,
                            'dependency' => array('element' => 'filter', 'value' => array('yes')),
                            'group' => 'Query and Layout Options'
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => 'Link Label',
                            'param_name' => 'filter_link_label',
                            'value' => '',
                            'admin_label' => true,
                            'description' => '',
                            'dependency' => array('element' => 'filter_content_alignment', 'value' => array('left')),
                            'group' => 'Query and Layout Options'
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => 'Link',
                            'param_name' => 'filter_link',
                            'value' => '',
                            'admin_label' => true,
                            'description' => '',
                            'dependency' => array('element' => 'filter_content_alignment', 'value' => array('left')),
                            'group' => 'Query and Layout Options'
                        ),

                )
				)
			);
        }
    }

    /**
     * Renders shortcodes HTML
     *
     * @param $atts array of shortcode params
     * @param $content string shortcode content
     * @return string
     */
    public function render($atts, $content = null) {
        
        $args = array(
            'type' => 'standard',
            'columns' => 'three',
            'space_between' => 'no',
            'grid_size' => 'three',
            'image_size' => 'full',
            'order_by' => 'date',
            'order' => 'ASC',
            'number' => '-1',
            'filter' => 'no',
            'filter_bottom_margin' => '',
            'filter_order_by' => 'name',
            'filter_content_alignment' => 'left',
            'filter_skin' => 'light',
            'filter_link_label' => '',
            'filter_link' => '',
            'category' => '',
            'selected_projects' => '',
            'show_load_more' => 'yes',
            'title_tag' => 'h3',
			'next_page' => '',
			'next_page' => '',
            'show_additional_info' => 'no',
            'info_title' => '',
            'info_text' => '',
            'info_link' => '',
            'info_link_label' => ''
        );

		$params = shortcode_atts($args, $atts);
		extract($params);
		
		$query_array = $this->getQueryArray($params);
		$query_results = new \WP_Query($query_array);		
		$params['query_results'] = $query_results;
		
		$classes = $this->getPortfolioClasses($params);
		$data_atts = $this->getDataAtts($params);
		$data_atts .= 'data-max-num-pages = '.$query_results->max_num_pages;
		$params['masonry_filter'] = '';
		$params['filter_style'] = '';

		$html = '';

        if($filter_bottom_margin !== ''){
            $params['filter_style'] .= 'margin-bottom:'.$filter_bottom_margin.'px';
        }

		if($filter == 'yes' && ($type == 'masonry' || $type =='pinterest')){
			$params['filter_categories'] = $this->getFilterCategories($params);	
			$params['masonry_filter'] = 'eltd-masonry-filter';
			$html .= eltd_core_get_shortcode_module_template_part('portfolio','portfolio-filter', '', $params);
		}
		
		$html .= '<div class = "eltd-portfolio-list-holder-outer '.$classes.'" '.$data_atts. '>';
		
		if($filter == 'yes' && ($type == 'standard' || $type =='gallery') && $show_additional_info == 'no'){
			$params['filter_categories'] = $this->getFilterCategories($params);
            $params['filter_additional_link'] = $this->getFilterAdditionalLink($params);
			$html .= eltd_core_get_shortcode_module_template_part('portfolio','portfolio-filter', '', $params);
		}
		
		$html .= '<div class = "eltd-portfolio-list-holder clearfix" >';
		if($type == 'masonry' || $type == 'pinterest'){
			$html .= '<div class="eltd-portfolio-list-masonry-grid-sizer"></div>';
			$html .= '<div class="eltd-portfolio-list-masonry-grid-gutter"></div>';
		}

        if($show_additional_info == 'yes'){
            $html .= '<article class="eltd-portfolio-item mix eltd-additional-info">';
                if($info_title !== ""){
                    $html .= '<h2>'.$info_title.'</h2>';
                    $html .= kreate_elated_execute_shortcode('eltd_separator', array(
                        'type'         => 'normal',
                        'position'     => 'left',
                        'color_dot'    => 'black',
                        'border_style' => 'dotted_multiple',
                        'width'        => '140',
                        'thickness'    => '3',
                        'top_margin'   => '25',
                        'bottom_margin'   => '25'
                    ));
                }
                if($info_text !== ""){
                    $html .= '<p>'.$info_text.'</p>';
                }
                if($info_link !== "" && $info_link_label !== ""){
                    $html .= kreate_elated_execute_shortcode('eltd_button',array(
                        'type' => 'arrow',
                        'hover_type' => 'rotate',
                        'text' => esc_html($info_link_label),
                        'link' => esc_url($info_link)
                    ));

                }
            $html .= '</article>';
        }

		if($query_results->have_posts()):			
			while ( $query_results->have_posts() ) : $query_results->the_post(); 
			
				$params['current_id'] = get_the_ID();
				$params['thumb_size'] = $this->getImageSize($params);
				$params['category_html'] = $this->getItemCategoriesHtml($params);
				$params['categories'] = $this->getItemCategories($params);
				$params['article_masonry_size'] = $this->getMasonrySize($params);
                $params['item_link'] = $this->getItemLink($params);
				
				$html .= eltd_core_get_shortcode_module_template_part('portfolio',$type, '', $params);
				
			endwhile;
		else: 
			
			$html .= '<p>'. _e( 'Sorry, no posts matched your criteria.' ) .'</p>';
		
		endif;

		if($params['space_between'] == 'yes'){
            $html .='<div class="eltd-gap"></div>';
            $html .='<div class="eltd-gap"></div>';
            $html .='<div class="eltd-gap"></div>';
            $html .='<div class="eltd-gap"></div>';
        }

		$html .= '</div>'; //close eltd-portfolio-list-holder
		if($show_load_more == 'yes'){
			$html .= eltd_core_get_shortcode_module_template_part('portfolio','load-more-template', '', $params);
		}
		wp_reset_postdata();
		$html .= '</div>'; // close eltd-portfolio-list-holder-outer
        return $html;
	}
	
	/**
    * Generates portfolio list query attribute array
    *
    * @param $params
    *
    * @return array
    */
	public function getQueryArray($params){
		
		$query_array = array();
		
		$query_array = array(
			'post_type' => 'portfolio-item',
			'orderby' =>$params['order_by'],
			'order' => $params['order'],
			'posts_per_page' => $params['number']
		);
		
		if(!empty($params['category'])){
			$query_array['portfolio-category'] = $params['category'];
		}
		
		$project_ids = null;
		if (!empty($params['selected_projects'])) {
			$project_ids = explode(',', $params['selected_projects']);
			$query_array['post__in'] = $project_ids;
		}
		
		$paged = '';
		if(empty($params['next_page'])) {
            if(get_query_var('paged')) {
                $paged = get_query_var('paged');
            } elseif(get_query_var('page')) {
                $paged = get_query_var('page');
            }
        }
		
		if(!empty($params['next_page'])){
			$query_array['paged'] = $params['next_page'];
			
		}else{
			$query_array['paged'] = 1;
		}
		
		return $query_array;
	}
	
	/**
    * Generates portfolio icons html
    *
    * @param $params
    *
    * @return html
    */
	public function getPortfolioIconsHtml($params){
		
		$html ='';
		$id = $params['current_id'];
		$slug_list_ = 'pretty_photo_gallery';
		
		$featured_image_array = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'full'); //original size				
		$large_image = $featured_image_array[0];
				
		$html .= '<div class="eltd-item-icons-holder">';
		
		$html .= '<a class="eltd-portfolio-lightbox" title="' . get_the_title($id) . '" href="' . $large_image . '" data-rel="prettyPhoto[' . $slug_list_ . ']"></a>';
		
		
		if (function_exists('kreate_elated_like_portfolio_list')) {
			$html .= kreate_elated_like_portfolio_list($id);
		}
		
		$html .= '<a class="eltd-preview" title="Go to Project" href="' . $this->getItemLink($params) . '" data-type="portfolio_list"></a>';
		
		$html .= '</div>';
		
		return $html;
        
	}
	/**
    * Generates portfolio classes
    *
    * @param $params
    *
    * @return string
    */
	public function getPortfolioClasses($params){
		$classes = array();
		$type = $params['type'];
		$columns = $params['columns'];
		$grid_size = $params['grid_size'];

        switch($type):
			case 'standard':
				$classes[] = 'eltd-ptf-standard';
			break;
			case 'gallery':
				$classes[] = 'eltd-ptf-gallery';
			break;
			case 'masonry':
				$classes[] = 'eltd-ptf-masonry';
			break;
			case 'pinterest':
				$classes[] = 'eltd-ptf-pinterest';
			break;
		endswitch;
		
		if($type == 'standard' || $type == 'gallery' ){
			switch ($columns):
				case 'one': 
					$classes[] = 'eltd-ptf-one-column';
				break;
				case 'two': 
					$classes[] = 'eltd-ptf-two-columns';
				break;
				case 'three': 
					$classes[] = 'eltd-ptf-three-columns';
				break;
				case 'four': 
					$classes[] = 'eltd-ptf-four-columns';
				break;
				case 'five': 
					$classes[] = 'eltd-ptf-five-columns';
				break;
				case 'six': 
					$classes[] = 'eltd-ptf-six-columns';
				break;
			endswitch;
		}
		if($params['show_load_more']== 'yes'){ 
			$classes[] = 'eltd-ptf-load-more';
		}
		
		if($type == "pinterest"){
			switch ($grid_size):
				case 'three': 
					$classes[] = 'eltd-ptf-pinterest-three-columns';
				break;
				case 'four': 
					$classes[] = 'eltd-ptf-pinterest-four-columns';
				break;
				case 'five': 
					$classes[] = 'eltd-ptf-pinterest-five-columns';
				break;
			endswitch;
		}
		if($params['filter'] == 'yes'){
			$classes[] = 'eltd-ptf-has-filter';
			if($params['type'] == 'masonry' || $params['type'] == 'pinterest'){
				if($params['filter'] == 'yes'){
					$classes[] = 'eltd-ptf-masonry-filter';
				}
			}
		}

        if($params['space_between'] == 'yes'){
            $classes[] = 'eltd-with-space';
        }
		
		return implode(' ',$classes);
        
	}
	/**
    * Generates portfolio image size
    *
    * @param $params
    *
    * @return string
    */
	public function getImageSize($params){
		
		$thumb_size = 'full';
		$type = $params['type'];
		
		if($type == 'standard' || $type == 'gallery'){
			if(!empty($params['image_size'])){
				$image_size = $params['image_size'];

				switch ($image_size) {
					case 'landscape':
						$thumb_size = 'kreate_elated_landscape';
						break;
					case 'portrait':
						$thumb_size = 'kreate_elated_portrait';
						break;
					case 'square':
						$thumb_size = 'kreate_elated_square';
						break;
					case 'full':
						$thumb_size = 'full';
						break;
				}
			}
		}
		elseif($type == 'masonry'){
			
			$id = $params['current_id'];
			$masonry_size = get_post_meta($id, 'portfolio_masonry_dimenisions',true);
			
			switch($masonry_size):
				case 'default' : 
					$thumb_size = 'kreate_elated_portfolio_square';
				break;
				case 'large_width' : 
					$thumb_size = 'kreate_elated_large_width';
				break;
				case 'large_height' : 
					$thumb_size = 'kreate_elated_large_height';
				break;
				case 'large_width_height' : 
					$thumb_size = 'kreate_elated_large_width_height';
				break;
			endswitch;
		}
		
		
		return $thumb_size;
	}
	/**
    * Generates portfolio item categories ids.This function is used for filtering
    *
    * @param $params
    *
    * @return array
    */
	public function getItemCategories($params){
		$id = $params['current_id'];
		$category_return_array = array();
		
		$categories = wp_get_post_terms($id, 'portfolio-category');
		
		foreach($categories as $cat){
			$category_return_array[] = 'portfolio_category_'.$cat->term_id;
		}
		return implode(' ', $category_return_array);
	}
	/**
    * Generates portfolio item categories html based on id
    *
    * @param $params
    *
    * @return html
    */
	public function getItemCategoriesHtml($params){
		$id = $params['current_id'];
		
		$categories = wp_get_post_terms($id, 'portfolio-category');
		$category_html = '<div class="eltd-ptf-category-holder">';
		$k = 1;
		foreach ($categories as $cat) {
			$category_html .= '<span>'.$cat->name.'</span>';
			if (count($categories) != $k) {
				$category_html .= ' / ';
			}
			$k++;
		}
		$category_html .= '</div>'; 
		return $category_html;
	}
	/**
    * Generates masonry size class for each article( based on id)
    *
    * @param $params
    *
    * @return string
    */
	public function getMasonrySize($params){
		$masonry_size_class = '';
		
		if($params['type'] == 'masonry'){
			
			$id = $params['current_id'];
			$masonry_size = get_post_meta($id, 'portfolio_masonry_dimenisions',true);
			switch($masonry_size):
				case 'default' : 
					$masonry_size_class = 'eltd-default-masonry-item';
				break;
				case 'large_width' : 
					$masonry_size_class = 'eltd-large-width-masonry-item';
				break;
				case 'large_height' : 
					$masonry_size_class = 'eltd-large-height-masonry-item';
				break;
				case 'large_width_height' : 
					$masonry_size_class = 'eltd-large-width-height-masonry-item';
				break;
			endswitch;
		}
		
		return $masonry_size_class;
	}
	/**
    * Generates filter categories array
    *
    * @param $params
    * @return array
    */
	public function getFilterCategories($params){
		
		$cat_id = 0;
		$top_category = '';
		
		if(!empty($params['category'])){	
			
			$top_category = get_term_by('slug', $params['category'], 'portfolio-category');
			if(isset($top_category->term_id)){
				$cat_id = $top_category->term_id;
			}
			
		}
		
		$args = array(
			'child_of' => $cat_id,
			'order_by' => $params['filter_order_by']
		);
		
		$filter_categories = get_terms('portfolio-category',$args);
		
		return $filter_categories;
		
	}

    /**
     * Generates filter additional link
     *
     * @param $params
     * @return string
     */
    public function getFilterAdditionalLink($params){

        $html = '';

        if($params['filter_link'] !== "" && $params['filter_link_label'] !== "" && $params['filter_content_alignment'] == 'left'){

            $html .= '<a class="eltd-filter-link" href="'.$params['filter_link'].'">'.$params['filter_link_label'].'<i class="eltd-icon-linea-icon icon-arrows-squares eltd-icon-element" style=""></i></a>';

        }

        return $html;

    }

	/**
    * Generates datta attributes array
    *
    * @param $params
    *
    * @return array
    */
	public function getDataAtts($params){
		
		$data_attr = array();
		$data_return_string = '';
		
		if(get_query_var('paged')) {
            $paged = get_query_var('paged');
        } elseif(get_query_var('page')) {
            $paged = get_query_var('page');
        } else {
            $paged = 1;
        }
		
		if(!empty($paged)) {
            $data_attr['data-next-page'] = $paged+1;
        }		
		if(!empty($params['type'])){
			$data_attr['data-type'] = $params['type'];
		}
		if(!empty($params['columns'])){
			$data_attr['data-columns'] = $params['columns'];
		}
		if(!empty($params['grid_size'])){
			$data_attr['data-grid-size'] = $params['grid_size'];
		}
		if(!empty($params['order_by'])){
			$data_attr['data-order-by'] = $params['order_by'];
		}
		if(!empty($params['order'])){
			$data_attr['data-order'] = $params['order'];
		}
		if(!empty($params['number'])){
			$data_attr['data-number'] = $params['number'];
		}
		if(!empty($params['image_size'])){
			$data_attr['data-image-size'] = $params['image_size'];
		}
		if(!empty($params['filter'])){
			$data_attr['data-filter'] = $params['filter'];
		}
		if(!empty($params['filter_order_by'])){
			$data_attr['data-filter-order-by'] = $params['filter_order_by'];
		}
		if(!empty($params['category'])){
			$data_attr['data-category'] = $params['category'];
		}
		if(!empty($params['selected_projectes'])){
			$data_attr['data-selected-projects'] = $params['selected_projectes'];
		}
		if(!empty($params['show_load_more'])){
			$data_attr['data-show-load-more'] = $params['show_load_more'];
		}
		if(!empty($params['title_tag'])){
			$data_attr['data-title-tag'] = $params['title_tag'];
		}
		
		foreach($data_attr as $key => $value) {
			if($key !== '') {
				$data_return_string .= $key.'= '.esc_attr($value).' ';
			}
		}
		return $data_return_string;
	}

    public function getItemLink($params){

        $id = $params['current_id'];
        $portfolio_link = get_permalink($id);
        if (get_post_meta($id, 'portfolio_external_link',true) !== ''){
            $portfolio_link = get_post_meta($id, 'portfolio_external_link',true);
        }

        return $portfolio_link;

    }
}